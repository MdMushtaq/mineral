<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/clear-cache', function() {
    $exitCode = Artisan::call('config:clear');
    $exitCode = Artisan::call('cache:clear');
    $exitCode = Artisan::call('config:cache');
    $exitCode = Artisan::call('view:cache');
    return 'DONE'; //Return anything
});
Route::get('localization/{locale}','LocalizationController@lang');

Route::get('/', 'WelcomeController@index')->name('welcome.page');
// Route::post('/price/filter','WelcomeController@filterprice')->name('price.filter');

// Route::get('/layouts3', 'WelcomeController@layout')->name('layouts.new');
// show View all , Special , New , Popular , Top Selling Prodcts


Route::get('/new/arrival','WelcomeController@newarrival')->name('new.arrival');
Route::get('/hot/product','WelcomeController@hotproduct')->name('hot.product');
Route::get('/feature/product','WelcomeController@featureproduct')->name('feature.product');
Route::get('/category/list/{id}','WelcomeController@Categorylist')->name('category.list');
Route::get('/Subcategory/list/{id}','WelcomeController@SubCategorylist')->name('subcategory.list');
Route::get('/deals','WelcomeController@deals');


Route::get('/products/list/{id}','WelcomeController@productslist')->name('products.list');

Route::get('setpassword/{token}','EmailController@setpassword');

Route::get('sendemail','EmailController@sendemail');

Route::get('/careers','WelcomeController@careers');
Route::post('/careers','WelcomeController@careersPost');

Route::get('/faq','WelcomeController@faq');
Route::POST('/faq','WelcomeController@faqPost');

Route::get('/contact','WelcomeController@contact');

Route::get('/termsandconditions','WelcomeController@termsandconditions');

Route::get('/privacyandpolicy','WelcomeController@privacyandpolicy');

Route::get('/about','WelcomeController@about');

Route::get('/help','WelcomeController@help');

Route::post('sendproductemail','WelcomeController@sendproductemail');

Route::post('subscribe','WelcomeController@subscribe');

// cart added
Route::post('/cart/add/{id}','WelcomeController@cartadd')->name('cart.added');
Route::get('/checkout/cart','WelcomeController@checkoutcart')->name('checkout.cart');
Route::post('/quantity/update/{rowId}','WelcomeController@Qtyupdate')->name('quantity.update');
Route::get('remove/cart/{rowId}','WelcomeController@removeCart')->name('remove.cart');

Auth::routes();

    Route::post('password/reset','EmailController@passwordreset');
    Route::post('set_passwordPost','EmailController@set_passwordPost');

    Route::get('/checkout-form' , 'HomeController@checkoutForm')->name('checkout.form');
    Route::post('/city/change' , 'HomeController@CityChange')->name('city.change');
    Route::post('/checout-form/post' , 'HomeController@Checkoutpost')->name('checkout-form.post');
    Route::get('/checkout/pay/{order}', 'HomeController@payView')->name('checkout.pay');
    Route::get('/transaction/pay/{id}' ,'HomeController@pay')->name('transaction');
    // Route::get('/api-test' ,'HomeController@pay')->name('pay.success');
    Route::get('/payredirect' ,'HomeController@payredirect')->name('redirect.success');
    Route::post('/tap/post' ,'HomeController@tapPost')->name('tap.post');
    Route::post('/tap/charge' ,'HomeController@tapCharge')->name('tap.charge');

    // Route::post('/cart/add/{id}','HomeController@cartadd')->name('cart.added');
    Route::get('/wishlist/add/{id}','HomeController@wishlist')->name('wishlist.add');
    // Route::get('/wishlist/update/{id}','HomeController@wishlistUpdate')->name('wishlist.update');
    Route::get('/home', 'HomeController@index')->name('home');
    Route::get('/customer/dashboard', 'HomeController@CustomerDashboard')->name('customer.dashboard');
    Route::get('/cart/items/{id}', 'HomeController@show')->name('cartitem.show');

// admin Middleware Section
Route::get('/admin/login', 'Auth\AdminLoginController@showLoginForm')->name('admin.login');
Route::post('/admin/login', 'Auth\AdminLoginController@login')->name('admin.login.submit');

Route::get('/delivery/agents/login', 'Auth\AdminLoginController@showDeliveryLoginForm')->name('delivery.login');
Route::post('/delivery/agents/login', 'Auth\AdminLoginController@showDeliveryLoginFormPost')->name('delivery.login.submit');
Route::get('/delivery/agents/register', 'Auth\AdminLoginController@showDeliveryRegisterForm')->name('delivery.register');
Route::post('/delivery/agents/register', 'Auth\AdminLoginController@showDeliveryRegisterFormPost')->name('delivery.register.submit');



Route::prefix('delivery/agents')->middleware('checkdelivery')->group(function(){
    Route::get('/', 'DeliveryController@index')->name('delivery.dashboard');
    Route::get('/orders', 'deliveryagents\SalesController@index');

    Route::get('/orders/items/{id}', 'deliveryagents\SalesController@orderagentapproved')->name('order.agentapprove');
});

Route::prefix('admin')->middleware('auth:admin')->group(function() {
    Route::get('/', 'AdminController@index')->name('admin.dashboard');
    Route::get('/logout','Auth\AdminLoginController@logout')->name('admin.logout');

    Route::get('/main-categories' , 'CategoriesController@mainCategory')->name('main-categories');
    Route::get('/main-categories/form' , 'CategoriesController@mainCategoryForm')->name('main-categories.form');
    Route::post('/main-categories/create' , 'CategoriesController@mainCategoryCreate')->name('main-categories.create');

    //  Product dropdown route
    Route::get('/categories','CategoriesController@index');
    Route::get('/categories/form','CategoriesController@create');
    Route::post('/categories/form','CategoriesController@store')->name('add.categories');
    Route::get('/categories/form/{id}','CategoriesController@edit')->name('edit.categories');
    Route::post('/categories/form/{id}','CategoriesController@update')->name('update.categories');
    Route::get('/categories/delete{id}','CategoriesController@delete')->name('delete.categories');
    Route::get('/categories/trash','CategoriesController@categoriesTrash')->name('trash.categories');
    Route::get('/subcategories','CategoriesController@subcategories');
    Route::get('/subcategories/form','CategoriesController@subcategoriesform');
    Route::post('/subcategories/form','CategoriesController@subcategoriesformpost');

    Route::get('/subcategories/edit/{id}','CategoriesController@subcategoriesformedit');
    Route::post('/subcategories/formeditpost','CategoriesController@subcategoriesformeditpost');
    
    Route::post('/getcategories', 'CategoriesController@getcategories');
    Route::post('/getsubcategories', 'CategoriesController@getsubcategories');

    // subcategories delet
    Route::get('/subcategories/delete/{id}','CategoriesController@subcategories_delet')->name('subcategories.delete');
    Route::get('/subcategories/trash','CategoriesController@trashsubcategories')->name('trash.subcategories');


//    Route::get('/banners','ContentContrroller@index');

    Route::get('/brands','CategoriesController@brands');
    Route::get('/barnds/add','CategoriesController@addbrands')->name('add.brands');
    Route::post('/barnds/store','CategoriesController@storebrands')->name('brands.create');
    Route::get('/brands/edit/{id}','CategoriesController@editbrand')->name('brands.edit');
    Route::post('/brands/update/{id}','CategoriesController@updatebrand')->name('brnads.update');
    Route::get('/brands/delete/{id}','CategoriesController@deletebrand')->name('brnads.delete');
    Route::get('/brands/trash','CategoriesController@trashbrand')->name('brands.trash');

    Route::get('/products','CategoriesController@products');
    Route::get('/products/add','CategoriesController@addproducts')->name('add.products');
    Route::post('/products/addformpost','CategoriesController@addproductspost');
    Route::get('/products/edit/{id}','CategoriesController@editproduct');
    Route::post('/products/editpost/{id}','CategoriesController@editproductpost');
    Route::get('/products/delete/{id}','CategoriesController@productdelete')->name('product.delete');
    Route::get('/products/trash','CategoriesController@producttrash')->name('trash.products');
    Route::post('/Img/remove/{id}','CategoriesController@imgDelete')->name('img.delete');


    Route::post('/products/deleteproductattribute','CategoriesController@deleteproductattribute');


    Route::post('/products/filter/categories','CategoriesController@Filterbycategoru')->name('categories.filter');

    Route::get('/pages','CategoriesController@information');
    Route::get('/pages/add','CategoriesController@pagesadd');
    Route::post('/pages/addpost','CategoriesController@pagesaddpost');

    Route::get('/pages/addlink','CategoriesController@pagesaddlink');
    Route::post('/pages/addlinkpost','CategoriesController@pagesaddlinkpost');

    // Promotion Dropdown route
    
    Route::get('/product_offers', 'PromotionController@product_offers');
    Route::get('/product_offers/form','PromotionController@add_product_wise_offer')->name('products.offers');
    Route::get('/product_offers/edit/{id}', 'PromotionController@product_offersedit');
    Route::post('/product_offers/formpostedit','PromotionController@product_formpostedit');
    Route::get('/product_offers/delete/{id}','PromotionController@product_offers_delete')->name('product_offer.delete');
    Route::post('/product_offers/formpost','PromotionController@product_offerspost');

    Route::post('/getsubcategoryproducts','PromotionController@getsubcategoryproducts');
    Route::post('/getsubcategoryproducts1','PromotionController@getsubcategoryproducts1');

    Route::post('/productpromotionremoveproduct','PromotionController@productpromotionremoveproduct');
    Route::post('/subcategorypromotionremovecategory','PromotionController@subcategorypromotionremovecategory');


  
    // categories offer
    Route::get('/category_offers','PromotionController@category_wise_offers');

    // subcategories offer
    Route::get('/sub_category_wise_offers/form','PromotionController@sub_category_wise_offers');
    Route::post('/sub_category_wise_offers/formpost','PromotionController@sub_category_wise_offersformpost');
    Route::get('/sub_category_offers', 'PromotionController@sub_category_offers');
    Route::get('/sub_category_offers/edit/{id}', 'PromotionController@sub_category_offersedit');
    Route::post('/sub_category_wise_offers/formpostedit', 'PromotionController@sub_category_offerseditpost');
    Route::get('/sub_category_wise_offers/delete/{id}','PromotionController@sub_categorire_offers_delete')->name('sub_categories_offer.delete');


    // brands offers
    Route::get('/brand_offers', 'PromotionController@brandoffers');
    Route::get('/brand_offers/edit/{id}', 'PromotionController@brandoffersedit');
    Route::post('/brand_offers/formpostedit', 'PromotionController@brandofferseditpost');
    Route::get('/brand_offers/form','PromotionController@brandoffersform');
    Route::post('/brand_offers/formpost','PromotionController@brandoffersformpost');
     Route::get('/brand_offers/delete/{id}','PromotionController@brandoffers_delete')->name('brands_offers_delete');
    
    
    // sales dropdown route

    Route::get('/orders', 'SalesController@index');
    Route::get('/orders/items/{id}', 'SalesController@show')->name('orderitem.show');
    Route::post('/orders/update/{id}', 'SalesController@update')->name('order.update');

    Route::get('/orders/confirmbyAdmin', 'SalesController@confirmbyAdmin');
    Route::get('/orders/Delivered', 'SalesController@Orderdelivered')->name('order.delivered');
    Route::get('/orders/delivery', 'SalesController@delivery')->name('order.delivery');
    Route::get('/orders/deliveryform' , 'SalesController@deliveryForm');
    Route::post('/orders/deliveryform' , 'SalesController@deliveryFormPost')->name('delivery.create');
    Route::get('/orders/delivery/edit/{id}','SalesController@deliveryformedit')->name('delivery.edit');
    Route::post('/orders/delivery/edit','SalesController@deliveryformeditpost')->name('delivery.editpost');







    // Route::get('/orders/confirmbyajent', 'SalesController@confirmbyajent');

  

    // content dropdown route
    Route::get('/banners', 'ContentController@index');
    Route::get('/banners/add', 'ContentController@create');
    Route::post('/banners/addpost', 'ContentController@createpost');
    Route::get('/banners/edit/{id}', 'ContentController@banneredit');
    Route::post('/banners/editpost', 'ContentController@bannereditpost');
    Route::get('/banners/delete/{id}', 'ContentController@deletebanner');


    //Images
    Route::get('/images', 'ContentController@images');
    Route::get('/images/add', 'ContentController@imagesadd');
    Route::post('/images/addpost', 'ContentController@imagesaddpost');
    Route::get('/images/edit/{id}', 'ContentController@imagesedit');
    Route::post('/images/editpost', 'ContentController@imageseditpost');
    Route::get('/images/delete/{id}', 'ContentController@deleteimages');


    // content  route
    Route::get('/coupons', function () {
        return view('coupons');
    });

     // Report route
    Route::get('/products/report', 'ReportController@index');
    Route::get('/sales/report', 'ReportController@SalesReport');
    Route::post('/sales/report', 'ReportController@SalesReportPost');

    // Route::post('/report/form', 'ReportController@reportpost');

    // setting dropdown route
    Route::get('/configuration', function () {
        return view('settings.configure');
    });

    Route::get('/shipping', function () { return view('settings.shipping'); });
   

    Route::get('/shipping/list','SettingsController@freeshippinformadd')->name('shipping.list');
    Route::get('/shipping/create','SettingsController@Shippingcreate')->name('add.shipping');
    Route::post('/shipping/form','SettingsController@ShippingStore')->name('store.shipping');

    // Route::post('/getmaxshippingid','SettingsController@getmaxshippingid');

    // Route::post('/free-shipping/form','SettingsController@freeshippinformaddpost');

    // Route::get('/free-shipping/delete/{id}','SettingsController@deletefreeshipping')->name('freeshipping.delete');

    // Route::get('flat-rate/form','SettingsController@flatrateformadd');

    Route::get('/faq','SettingsController@faq');
    Route::post('/faq','SettingsController@faqpost');

    //Faq
    Route::get('/faq/add','SettingsController@faqadd');
    Route::post('/faq/addpost','SettingsController@faqaddpost');
    Route::get('/faq/edit/{id}','SettingsController@faqedit')->name('edit.faq');
    Route::post('/faq/faqeditpost','SettingsController@faqeditpost');
    Route::get('/faq/delete/{id}','SettingsController@faqdelete')->name('delete.faq');


    //Faq Categories
    Route::get('/faq/category','SettingsController@faqcategoies');
    Route::get('/faq/category/add','SettingsController@faqaddcategory');
    Route::post('/faq/category/addfaqcategory','SettingsController@faqaddcategoryPost');
    Route::get('/faq/category/edit/{id}','SettingsController@faqcategoryedit')->name('edit.faqcategory');
    Route::post('/faq/category/editpost','SettingsController@faqcategoryeditpost');


    Route::get('/vat/form','SettingsController@vatformadd');
    Route::post('/vat/form','SettingsController@vatformaddpost');

    Route::post('/getmaxvatid','SettingsController@getmaxvatid');

    Route::get('/vat/delete/{id}','SettingsController@deletevat')->name('vat.delete');
    Route::post('flat-rate/form','SettingsController@flatrateformaddpost');

    Route::get('/flat-rate/delete/{id}','SettingsController@deleteflatrate')->name('flatrate.delete');

    Route::get('/subscriptions','SettingsController@subscriptions');
    Route::get('/generateexcelsheet','SettingsController@generateexcelsheet');
    Route::get('/generatetonotepad','SettingsController@generatetonotepad');


    Route::get('/wishlist','WishlistController@index');


    Route::get('customers','CustomersController@index');

    Route::get('customers/form','CustomersController@addform');
    Route::post('customers/form','CustomersController@addformpost');

    Route::get('customers/form/{id}','CustomersController@editform');
    Route::post('customers/editform/','CustomersController@editformpost');

    Route::get('customers/archive','CustomersController@customersarchive');

    Route::get('customers/suspend','CustomersController@customerssuspend');



    Route::get('/payments', function () {
        return view('settings.payments');
    });
    Route::get('/locations', function () {
        return view('settings.cashondelivery');
    });
    Route::get('/canned_messages', function () {
        return view('settings.canned_messages');
    });
    Route::get('/users',  'SettingsController@index')->name('admin.show');

    Route::get('/create', 'SettingsController@create')->name('create.admins');

    Route::post('/store', 'SettingsController@store')->name('store.admins');



});


