<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCategoryPromotionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('category_promotions', function (Blueprint $table) {
            $table->increments('id');

            $table->string('offer_name')->nullable();
            $table->dateTime('enable_on_utc')->nullable();
            $table->dateTime('disable_on_utc')->nullable();
            $table->string('reduction_type')->nullable();
            $table->string('percentage')->nullable();
            $table->string('status')->nullable();
            $table->string('category_id')->nullable();
            $table->string('sub_category_id')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('category_promotions');
    }
}
