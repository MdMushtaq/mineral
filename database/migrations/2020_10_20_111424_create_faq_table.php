<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFaqTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('faq', function (Blueprint $table)
        {
            $table->increments('id');
            $table->string('question')->nullable();
            $table->string('answer')->nullable();
            $table->string('arabicquestion')->nullable();
            $table->string('arabicanswer')->nullable();
            $table->string('faqcategoryid')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('faq');
    }
}
