<?php

use Illuminate\Database\Seeder;
use App\Permission;
use App\Admin;

class PermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        Permission::truncate();

        $permissions = [
            'Products' => [
                'Categories',
                'Sub Category',
                'Brands',
                'Product',
                'Information'
            ],
            'Promotions' => [
                'Product wise % discount',
                'Sub Category wise % discount'
            ],
            'Sales' =>[
                'Orders',
                'Confirmed By Admin',
                'Delivered'
            ],
            'Content' => [
                'Slider',
                'Images'
            ],
            'Wishlist' => [],
            'Customer' => [],
            'Reports' => [
                'Products Info',
                'Sales Info'
            ],
            'Setting' => [
                'Configuration',
                'Shipping Modules',
                'Payment Modules',
                'Cash on Delivery Charges',
                'Canned Messages',
                'Administrators',
                'Sitemap',
                'Faq',
                'Subscriptions'
            ]
            // 'gub' => []
        ];

        $permissionIds = [];
        foreach ($permissions as $permission => $children) {
            $parent = new Permission();
            $parent->name = $permission;
            $parent->save();
            $permissionIds[] = $parent->id;

            foreach ($children as $child) {
                $newChild = new Permission();
                $newChild->name = $child;
                $newChild->parent_id = $parent->id;
                $newChild->save();
                $permissionIds[] = $newChild->id;
            }
        }

        $admins = Admin::whereIn('job_title', ['master admin', 'admin'])->get();
        foreach ($admins as $admin) {
            $admin->permissions()->sync($permissionIds);
        }
    }
}
