<?php

return [
    // careers.blade.php

    'Job Portal' => 'بوابة الوظائف',
    'Achieving success by respecting and empowering personal potential.' => 'تحقيق النجاح من خلال احترام وتمكين الإمكانات الشخصية.',
    'Our passion and ingenuity fuels inspired design. We are always looking for talented, dedicated and passionate teammates who want to carve their own path exploring technological innovation in engineering. Visit our careers page to find out more!' => 'شغفنا وإبداعنا يغذي التصميم المستوحى. نحن نبحث دائمًا عن زملاء موهوبين ومتفانين وعاطفين يرغبون في شق طريقهم الخاص لاستكشاف الابتكار التكنولوجي في الهندسة. قم بزيارة صفحة الوظائف لدينا لمعرفة المزيد!',
    'There is no job available right now. Kindly upload your CV we will contact you asap.' => 'لا توجد وظيفة متاحة الآن. يرجى تحميل سيرتك الذاتية وسنتواصل معك في اسرع وقت ممكن.',
    'Register' => 'تسجيل',
    'Back' => 'عودة',
    'Your Name' => 'اسمك',
    'Your Email' => 'بريدك الالكتروني',
    'Phone Number' => 'رقم الهاتف',
    'Job Title' => 'عنوان وظيفي',
];

?>