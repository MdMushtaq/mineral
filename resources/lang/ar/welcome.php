<?php
   return [
      // welcome.blade.php
 
      'View All' => 'مشاهدة الكل',
      'New Products' => 'منجاتنا الجديدة',
      'Popular Products' => 'المنجات الرائجة',
      'Top Selling Products' => 'المنتجات الافضل مبيعا',
      'Filter Product' => 'مرشح المنتج',
      'SAR' => 'ريال سعودي',
      'ADD TO CART' => 'أضف إلى السلة',
      'CART' => 'عربة التسوق',


      // layouts front-app.blade.php
      'FREE AND ESAY RETURNS' => 'ترجيع مجانا وبسهولة',
      'BEST DEALS' => 'افضل العروض',
      'TOP BRANDS' => 'افضل الاضاف',
      'SIGN IN' => 'تسجيل الدخول',
      'SIGN UP' => 'انشاء حساب جديد',
      'Select Language' => 'اختار اللغة',

      // Header menu Bar
      'CATEEGORIES' => 'التصنيفات',
      'Electronics' => 'إلكترونيات',
      'PRODUCTS' => 'المنتجات',
      'SPECIAL OFFER' => 'عرض خاص',
      'Offer' => 'عرض',
      'ABOUT US' => 'تعرف علينا',
      'HELP' => 'مساعدة',
      'DEALS' => 'صفقات',
      'CONTACT US' => 'تواصل معنا',
      'Contact Us' => 'تواصل معنا',
      'Complaint' => 'شكوى',
      'SHOP MENU' => 'قائمة التسوق',
      'DELIVER TO' => 'يسلم إلى',
      'SAUDIA ARABIA' => 'المملكة العربية السعودية',
      'FLASH' => 'فلاش',
      'SALE' => 'تخفيض السعر',
      'END IN' => 'ينتهي في',

       //Footer
       'About Us' => 'تعرف علينا',
       'Careers' => 'وظائف',
       'Media' => 'وسائل الإعلام',
       'Initiatives' => 'المبادرات',
       'FAQ' => 'التعليمات',
       'Terms & Conditions' => 'البنود و الظروف',
       'Privacy and Policy' => 'الخصوصية والسياسة',
       'Phone' => 'هاتف',
       'Email' => 'البريد الإلكتروني',
       'Working Days Hours' => 'ساعات أيام العمل',
       'Let Us Help You' => 'دعنا نساعدك',
       'Your Account' => 'الحساب الخاص بك',
       'Your Orders' => 'طلباتك',
       'Shipping Rates & Policies' => 'أسعار الشحن والسياسات',
       'Payment Method' => 'طريقة الدفع او السداد',
       'All Rights Reserved' => 'كل الحقوق محفوظة',
       'Powered by' => 'مشغل بواسطة',
       'Contact Information' => 'معلومات الاتصال',
       'Quick links' => 'روابط سريعة',
       'NEW ARRIVAL' => 'قادم جديد',
       'View More' => 'عرض المزيد',
       'SR' => 'ريال سعودى',
       'HOT PRODUCTS' => 'منتوجات جديدة',
       'FEATURE CATEEGORIES' => 'فئات مميزة',
       'Get The Latest News' => 'احدث اخبارنا',
       'Enter your Email' => 'ادخال الايميل',
       'Subscribe' => 'الإشتراك',
       '© Copyright by Technosouq 2020'=>'الحقوق محفوظه ل تكنو سوق',


   ];
?>