<?php

return [
    // about.blade.php

    'About Us' => 'تعرف علينا',
    'Who we are' => 'من نحن',
    'Technosouq is guided by four principles: customer obsession rather than competitor focus, passion for invention, commitment to operational excellence, and long-term thinking.' => 'تسترشد تكنوسوق بأربعة مبادئ: هوس العملاء بدلاً من التركيز على المنافسين ، والشغف بالاختراع ، والالتزام بالتميز التشغيلي ، والتفكير طويل المدى',
    'Bulding the future' => 'بناء المستقبل',
    'We strive to have a positive impact on customers, employees, small businesses, the economy, and communities. Amazonians are smart, passionate builders with different backgrounds and goals, who share a common desire to always be learning and inventing on behalf of our customers.' => 'نحن نسعى جاهدين لإحداث تأثير إيجابي على العملاء والموظفين والشركات الصغيرة والاقتصاد والمجتمعات. الأمازون هم بناة أذكياء ومتحمسون بخلفيات وأهداف مختلفة ، ولديهم رغبة مشتركة في التعلم والابتكار دائمًا نيابة عن عملائنا',
    'What we do' => 'الذي نفعله',
    'Explore Technosouq’s products and services.' => 'اكتشف منتجات وخدمات تكنوسوق.',
    'Technosouq' => 'تكنوسوق'

];

?>jkk