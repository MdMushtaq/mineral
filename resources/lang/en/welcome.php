<?php
   return [
      // welcome.blade.php
    
      'View All' => 'View All',
      'New Products' => 'New Products',
      'Popular Products' => 'Popular Products',
      'Top Selling Products' => 'Top Selling Products',
      'Filter Product' => 'Filter Product',
      'SAR' => 'SAR',
      'ADD TO CART' => 'ADD TO CART',
      'CART' => 'CART',


      // layouts front-app.blade.php
      'FREE AND ESAY RETURNS' => 'FREE AND ESAY RETURNS',
      'BEST DEALS' => 'BEST DEALS',
      'TOP BRANDS' => 'TOP BRANDS',
      'SIGN IN' => 'SIGN IN',
      'SIGN UP' => 'SIGN UP',
      'Select Language' => 'Select Language',

      // Header menu Bar
      'CATEEGORIES' => 'CATEEGORIES',
      'Electronics' => 'Electronics',
      'PRODUCTS' => 'PRODUCTS',
      'SPECIAL OFFER' => 'SPECIAL OFFER',
      'Offer' => 'Offer',
      'ABOUT US' => 'ABOUT US',
      'HELP' => 'HELP',
      'DEALS' => 'DEALS',
      'CONTACT US' => 'CONTACT US',
      'Contact Us' => 'Contact Us',
      'Complaint' => 'Complaint',
      'SHOP MENU' => 'SHOP MENU',
      'DELIVER TO' => 'DELIVER TO',
      'SAUDIA ARABIA' => 'SAUDIA ARABIA',
      'FLASH' => 'FLASH',
      'SALE' => 'SALE',
      'END IN' => 'END IN',

       'About Us' => 'About Us',
       'Careers' => 'Careers',
       'Media' => 'Media',
       'Initiatives' => 'Initiatives',
       'FAQ' => 'FAQ',
       'Terms & Conditions' => 'Terms & Conditions',
       'Privacy and Policy' => 'Privacy and Policy',
       'Phone' => 'Phone',
       'Email' => 'Email',
       'Working Days Hours' => 'Working Days Hours',
       'Let Us Help You' => 'Let Us Help You',
       'Your Account' => 'Your Account',
       'Your Orders' => 'Your Orders',
       'Shipping Rates & Policies' => 'Shipping Rates & Policies',
       'Payment Method' => 'Payment Method',
       'All Rights Reserved' => 'All Rights Reserved',
       'Powered by' => 'Powered by',
       'Contact Information' => 'Contact Information',
       'Quick links' => 'Contact Information',
       'NEW ARRIVAL' => 'NEW ARRIVAL',
       'View More' => 'View More',
       'SR' => 'SR',
       'HOT PRODUCTS' => 'HOT PRODUCTS',
       'FEATURE CATEEGORIES' => 'FEATURE CATEEGORIES',
       'Get The Latest News' => 'Get The Latest News',
       'Enter your Email' => 'Enter your Email',
       'Subscribe' => 'Subscribe',
       '© Copyright by Technosouq 2020'=>'© Copyright by Technosouq 2020',
       'DELIVERY SIGN IN' => 'DELIVERY SIGN IN',
       'DELIVERY SIGN UP' => 'DELIVERY SIGN UP'

   ];
?>