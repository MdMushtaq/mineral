@extends('layouts.admin-app')
@section('content')
<div class="page-header">
	<h1>Products Info</h1>
</div>
<div class="row">
    <br>
    <div class="col-md-6">
        {{-- <h3>Best Sellers</h3> --}}
    </div>
    <div class="col-md-6">
        <form class="form-inline pull-right">
            <input class="form-control datepicker" type="text" name="best_sellers_start" placeholder="From"/>
            <input class="form-control datepicker" type="text" name="best_sellers_end" placeholder="To"/>

            <input class="btn btn-primary" type="button" value="Get Best Sellers"/>
        </form>
    </div>
</div>
<br>
<div class="btn-group pull-right">
</div>
    <table class="table table-striped" cellspacing="0" cellpadding="0">
        <thead>
        <tr>
            {{-- <th>S.No</th> --}}
            <th>Product Name</th>
            <th>Product Color(s)</th>
            <th>Color Quantity</th>
            <th>Sale</th>
            <th>Balance</th>
        </tr>
        </thead>
        <tbody>
            {{-- @php $serial = 1; @endphp --}}
            @foreach ($best_sell as $report)
            @php
            $names = [];
            $quantities = [];
            $saleQuantities = [];
            $balanceQuantities = [];
            foreach ($report->productQuantity as $attribute) {
                $attribute->items_quantity = $attribute->items_quantity ?? 0;
                $names[] = $attribute->colourname;
                $quantities[] = $attribute->quantity ?? 0;
                $saleQuantities[] = $attribute->items_quantity;
                $balanceQuantities[] = $attribute->quantity - $attribute->items_quantity;
            }
            @endphp
                <tr>
                    <td>{{$report->prod_name}}</td>
                    <td>{{implode(', ', $names)}}</td>
                    <td>{{implode(', ', $quantities)}}</td>
                    <td>{{implode(', ', $saleQuantities)}}</td>
                    <td>{{implode(', ', $balanceQuantities)}}</td>
            @endforeach
        </tbody>
    </table>
 

@endsection