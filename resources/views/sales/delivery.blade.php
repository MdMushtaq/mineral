@extends('layouts.admin-app')
@section('content')

@if (\Session::has('success'))
<div class="alert alert-success">
   <p>{{ \Session::get('success') }}</p>
</div>
<br />

@elseif(\Session::has('danger'))
<div class="alert alert-danger">
   <p>{{ \Session::get('danger') }}</p>
</div>
<br />
@endif 

    <div class="container">
        <div class="page-header"><h1>Delivery</h1></div>
        <div class="row">
            <div class="col-md-12">
                <div class="row">

                    <div class="col-md-4"></div>
                   

                    <div class="col-md-8">
                        <form action="#" class="form-inline form-group" style="float:right" method="post" accept-charset="utf-8">
                            <div class="form-group">

                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control" name="term" value="" placeholder="">
                            </div>
                            <button class="btn btn-default" name="submit" value="search">Search</button>
                            <a class="btn btn-default" href="#">Reset</a>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <div class="text-right form-group">
        <a class="btn btn-primary" style="font-weight:normal;" href="{{ url('/admin/orders/deliveryform') }}">
                <i class="icon-plus"></i> Add New Delivery
            </a>

        </div>
        <table class="table table-striped">
            <thead>
            <tr>
                <th><a href="#">Name <i class="icon-sort-alt-down"></i></a></th>
                <th><a href="#">Mobile Number</a></th>
                <th><a href="#">Verfication Code</a></th>
                <th><a href="#">Action</a></th>
            </tr>
            </thead>

            <tbody>
                @foreach ($deliveries as $delivery)
                    <tr>
                        <td>{{$delivery->name}}</td>
                        <td>{{$delivery->mobile}}</td>
                        <td>{{$delivery->verification_code}}</td>
                        <td>
                            <div class="btn-group">
                                <a class="btn btn-default" href="{{ route('delivery.edit',$delivery->id) }}"><i class="icon-pencil"></i></a>
                            </div>
                        </td>
                    </tr> 
                @endforeach
            </tbody>
        </table>

    </div>

@endsection
