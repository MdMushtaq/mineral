<!doctype html>
<html lang="en">
<head>

    <!--====== Required meta tags ======-->
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!--====== Title ======-->
    <title>Water</title>

    <!--====== Favicon Icon ======-->
    <link rel="shortcut icon" href="{{ asset('public/frontend/images/favicon.png') }}" type="image/png">


    <!--====== Font Awesome ======-->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">

    <!--====== Owl Carousel css ======-->
    <link rel="stylesheet" href="{{ asset('public/frontend/css/owl.carousel.min.css') }}">

    <!--====== Magnific Popup css ======-->
    <link rel="stylesheet" href="{{ asset('public/frontend/css/magnific-popup.css') }}">

    <!--====== Slick css ======-->
    <link rel="stylesheet" href="{{ asset('public/frontend/css/slick.css') }}">

    <!--====== Nice Select css ======-->
    <link rel="stylesheet" href="{{ asset('public/frontend/css/nice-select.css') }}">

    <!--====== Nice Number css ======-->
    <link rel="stylesheet" href="{{ asset('public/frontend/css/jquery.nice-number.min.css') }}">

    <!--====== Fontawesome css ======-->
    <link rel="stylesheet" href="{{ asset('public/frontend/css/font-awesome.min.css') }}">

    <!--====== Aanimate css ======-->
    <link rel="stylesheet" href="{{ asset('public/frontend/css/animate.css') }}">

    <!--====== Bootstrap css ======-->
    <link rel="stylesheet" href="{{ asset('public/frontend/css/bootstrap.min.css') }}">

    <!--====== Default css ======-->
    <link rel="stylesheet" href="{{ asset('public/frontend/css/default.css') }}">

    <!--====== Style css ======-->
    <link rel="stylesheet" href="{{ asset('public/frontend/css/style.css') }}">

    <!--====== Responsive css ======-->
    <link rel="stylesheet" href="{{ asset('public/frontend/css/responsive.css') }}">

    <link rel="stylesheet" href="{{ asset('public/frontend/css/custom.css') }}">


</head>

<body>

<!--====== PREALOADER PART START ======-->

<div class="preloader">
    <div class="thecube">
        <div class="cube c1"></div>
        <div class="cube c2"></div>
        <div class="cube c4"></div>
        <div class="cube c3"></div>
    </div>
</div>

<!--====== PREALOADER PART START ======-->

<!--====== HEADER PART START ======-->

<header id="header-part" class="header-2">
    <!--===== NAVBAR START =====-->
    <div class="navigation">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <nav class="navbar navbar-expand-lg">

                        <a class="navbar-brand" href="#">
                            <img src="{{ asset('public/frontend/images/logo-light.png') }}" alt="Logo">
                        </a> <!-- Logo -->

                        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>

                        <div class="collapse navbar-collapse sub-menu-bar" id="navbarSupportedContent">
                            <ul class="navbar-nav m-auto">
                                <li class="nav-item">
                                    <a class="active" href="#">Home</a>
                                    <!--
                                    <ul class="sub-menu">
                                        <li class="li"><a href="index.html">Home 01</a></li>
                                    </ul>
                                    -->
                                </li>
                                <li class="nav-item">
                                    <a href="#">About Us</a>
                                </li>

                                <li class="nav-item">
                                    <a href="#">Products</a>
                                </li>

                                <li class="nav-item">
                                    <a href="#">Contact Us</a>
                                </li>
                            </ul>
                        </div>

                        <div class="cart-search">
                            <ul class="navbar-nav text-right top-nav-icons">
                                <li class="nav-item">
                                    <a href="#">
                                        <i class="fa fa-phone"></i> (+88 - 123456789)
                                    </a>
                                </li>

                                <li class="nav-item">
                                    <a href="#">
                                        <i class="fa fa-globe"></i>
                                    </a>
                                    <ul class="sub-menu">
                                        <li class="li">
                                            <a href="#">Home 01</a>
                                        </li>
                                    </ul>
                                </li>
                                <li>
                                    <a href="#">
                                        <i class="fa fa fa-user-circle-o"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        Cart &nbsp;<i class="fa fa-shopping-basket"></i><span>0</span>
                                    </a>
                                </li>
                                <li>
                                    <a id="search" href="#">
                                        <i class="fa fa-search"></i>
                                    </a>
                                    <div class="search-box">
                                        <input type="search" placeholder="Search...">
                                        <button type="button">
                                            <i class="fa fa-search"></i>
                                        </button>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </nav>
                </div>
            </div>
        </div>
    </div>
    <!--===== NAVBAR ENDS =====-->
</header>

<!--====== HEADER PART ENDS ======-->

@yield('content')


<!--====== FOOTER PART START ======-->

<footer id="footer-part" class="footer-2 pt-20">
    <div class="container ">
        <div class="footer pt-20 pb-45">
            <div class="row">
                <div class="col-lg-3 col-md-6">
                    <div class="footer-about pt-30">
                        <a href="#"><img src="{{ asset('public/frontend/images/logo-light-footer.png') }}" alt="logo"></a>
                        <p>Donec vel ligula ornare, finibus ex at, vive risus. Aenean velit ex. </p>
                        <!--
                        <span><i class="fa fa-globe"></i>www.fresh@water.com</span>
                        -->
                    </div>
                </div>
                <div class="col-lg-2 col-md-3 col-sm-6">
                    <div class="footer-title pt-30">
                        <h5>Information</h5>
                    </div>
                    <div class="footer-info">
                        <ul>
                            <li><a href="#">Contact Us</a></li>
                            <li><a href="#">About Us</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-2 col-md-3 col-sm-6">
                    <div class="footer-title pt-30">
                        <h5>Our Services</h5>
                    </div>
                    <div class="footer-info">
                        <ul>
                            <li><a href="#">My Account</a></li>
                            <li><a href="#">Order History</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-2 col-md-6 col-sm-6">
                    <div class="footer-title pt-30">
                        <h5>Social Media</h5>
                    </div>
                    <ul class="social-icons">
                        <li class="oceanwp-twitter">
                            <a href="#" aria-label="Twitter" target="_blank" rel="noopener noreferrer">
                                <i class="fa fa-twitter" aria-hidden="true"></i>
                            </a>
                        </li>
                        <li class="facebook">
                            <a href="#" aria-label="Facebook" target="_blank" rel="noopener noreferrer">
                                <i class="fa fa-facebook" aria-hidden="true"></i>
                            </a>
                        </li>
                        <li class="linkedin">
                            <a href="#" aria-label="Linkedin" target="_blank" rel="noopener noreferrer">
                                <i class="fa fa-linkedin" aria-hidden="true"></i>
                            </a>

                        </li>
                    </ul>
                </div>
                <div class="col-lg-3 col-md-6 col-sm-6">
                    <div class="footer-title pt-30">
                        <h5>Get In Tuch</h5>
                    </div>
                    <div class="footer-address">
                        <ul>
                            <!--
                            <li>
                                <div class="icon map-i">
                                    <i class="fa fa-map-marker"></i>
                                </div>
                                <div class="address">
                                    <h5>Fresh Water</h5>
                                    <p>45 Grand Central Terminal <br> New York.</p>
                                </div>
                            </li>
                            -->
                            <li>
                                <div class="icon">
                                    <i class="fa fa-volume-control-phone"></i>
                                </div>
                                <div class="address">
                                    <p>+00123654789</p>
                                </div>
                            </li>
                            <li>
                                <div class="icon">
                                    <i class="fa fa-envelope-o"></i>
                                </div>
                                <div class="address">
                                    <p>freshwater@gmail.com</p>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="copyright pt-15 pb-15">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="text-center text-sm-left">
                        <p>&copy; All Rights Reserved Water 2020.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>

</footer>

<!--====== FOOTER PART ENDS ======-->

<!--====== BACK TOP TOP PART START ======-->

<a href="#" class="back-to-top">
    <img src="{{ asset('public/frontend/images/back-to-top.png') }}" alt="Icon">
</a>

<!--====== BACK TOP TOP PART ENDS ======-->

<!--====== jquery js ======-->
<script src="{{ asset('public/frontend/js/vendor/modernizr-3.6.0.min.js') }}"></script>
<script src="{{ asset('public/frontend/js/vendor/jquery-1.12.4.min.js') }}"></script>

<!--====== Bootstrap js ======-->
<script src="{{ asset('public/frontend/js/popper.min.js') }}"></script>
<script src="{{ asset('public/frontend/js/bootstrap.min.js') }}"></script>

<!--====== Owl Carousel js ======-->
<script src="{{ asset('public/frontend/js/owl.carousel.min.js') }}"></script>

<!--====== Magnific Popup js ======-->
<script src="{{ asset('public/frontend/js/jquery.magnific-popup.min.js') }}"></script>

<!--====== Slick js ======-->
<script src="{{ asset('public/frontend/js/slick.min.js') }}"></script>

<!--====== Nice Number js ======-->
<script src="{{ asset('public/frontend/js/jquery.nice-number.min.js') }}"></script>

<!--====== Nice Select js ======-->
<script src="{{ asset('public/frontend/js/jquery.nice-select.min.js') }}"></script>

<!--====== Validator js ======-->
<script src="{{ asset('public/frontend/js/validator.min.js') }}"></script>

<!--====== Ajax Contact js ======-->
<script src="{{ asset('public/frontend/js/ajax-contact.js') }}"></script>

<!--====== Main js ======-->
<script src="{{ asset('public/frontend/js/main.js') }}"></script>

<!--====== Google Map js ======-->
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDC3Ip9iVC0nIxC6V14CKLQ1HZNF_65qEQ"></script>
<script src="{{ asset('public/frontend/js/map-script.js') }}"></script>

</body>

</html>
