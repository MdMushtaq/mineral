@extends('layouts.admin-app')
@section('content')

<div class="container">
    <div class="page-header">
        <h1>Add Category Wise Offers &amp; Promotions</h1>
    </div>

    @if(Session::get('danger'))
    <div class="alert alert-danger">
        <p>{{ \Session::get('danger') }}</p>
    </div>
    @endif

    <div class="alert alert-danger">
        <p>You are already Promotion subcategory offers so it is not possible to offer promotino on the product .you have to use any one other Subcategory/Product offer if you want to add new offer you have to delete current offer</p>
    </div>
    <form action="{{ url('/admin/sub_category_wise_offers/formpost') }}" method="post" accept-charset="utf-8">
        {{ csrf_field() }}
        <div class="row">
            <div class="col-sm-4">
                <div class="form-group">
                    <label for="offer_name">Offer Name</label>
                    <input type="text" name="offer_name" value="" class="form-control">
                </div>
                <div class="form-group">
                    <label for="start_date">Enable On (UTC)</label>
                    <input type="text" name="start_date" value="" class="form_datetime form-control start_date" readonly="true">
                </div>
                <div class="form-group">
                    <label for="end_date">Disable On (UTC)</label>
                    <input type="text" name="end_date" value="" class="form_datetime form-control end_date" readonly="true">
                </div>
                <div class="form-group">
                    <label for="reduction_amount">Reduction Amount</label>
                    <div class="row">
                        <div class="col-md-6">
                            <select name="reduction_type" class="form-control">
                                <option value="percent">Percentage</option>
                            </select>
                        </div>
                        <div class="col-md-6">
                            <input type="text" name="reduction_amount" value="" class="form-control">
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label>Status</label>
                    <div class="form-group">
                        <select name="enabled_1" class="form-control">
                            <option value="1">Enabled</option>
                            <option value="0">Disabled</option>
                        </select>
                    </div>
                </div>
            </div>

            <div class="col-md-6 col-md-offset-1 well pull-right">
                <div class="form-group">
                    <div class="row">
                        <div class="col-md-6">
                            <label>Select Category</label>
                            <select class="form-control primary_category" name="primary_category" id="primary_category" required>
                                <option value="">SELECT</option>
                                <?php
                                foreach($categories as $category)
                                {
                                    ?>
                                    <option value="<?= $category->id; ?>"><?= $category->cat_name; ?></option>
                                <?php
                                }
                                ?>
                            </select>
                        </div>
                        <div class="col-md-6">
                            <label>Select Sub Category</label>
                            <select class="form-control secondary_category" name="secondary_category" id="secondary_category" required>
                                <option value="">SELECT</option>

                            </select>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <a href="#" onclick="addsubcategory()" class="btn btn-primary" title="Add Sub Category">Add Sub Category</a>
                            </div>

                            <div class="col-md-12" id="selectedproducts">
                                <input type="hidden" name="numberofsubcategory" id="numberofsubcategory" value="">
                                <h3>Selected Subcategories</h3>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-10">
                <button type="submit" class="btn btn-primary">Save</button>
            </div>
            <div class="col-md-2"></div>
        </div>
    </form>

</div>
<script>
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $(document).ready(function(){
        $("#primary_category").change(function(){
            var $catId = this.value;

            $.ajax({
                type: "POST",
                url: "{{ url('admin/getsubcategories') }}",
                data: {categoryid:$catId},
                success: function(result)
                {
                    $('#secondary_category').html(result);
                }
            });

        });
    });

    var subcategorycount = 1;

    var selectedsubcategoryids = [];

    function addsubcategory()
    {
        var subcategory = $("#secondary_category option:selected");
        var subcategoryvalue = subcategory.val();

        var subcategoryHtml = subcategory.html();
//
//        var productfound = false;

        for(var a = 0; a < selectedsubcategoryids.length; a++)
        {
            if(subcategoryvalue == selectedsubcategoryids[a])
            {
                alert('Sub Category Added');
                return false;
            }
        }
////
        subcategoryHtml = subcategorycount+". "+subcategoryHtml;
////
////        selectedproducts
        var nodediv = document.createElement("div");
        nodediv.setAttribute("class","col-md-12");
        nodediv.setAttribute("id","subcategorydivid"+subcategoryvalue);
//
        var node = document.createElement("p");                 // Create a <li> node
        var textnode = document.createTextNode(subcategoryHtml);
        node.appendChild(textnode);
        node.setAttribute("style","display: inline");
//
        var node1 = document.createElement("input");                 // Create a <li> node
        node1.setAttribute("type","hidden");
        node1.setAttribute("name","subcategory"+subcategorycount);
        node1.setAttribute("value",subcategoryvalue);

        nodediv.appendChild(node);
        nodediv.appendChild(node1);
////
////
        var atag = document.createElement("a");
        atag.setAttribute("style","padding: 2px 8px;cursor: pointer; float: right; background-color: red");
        var buttonfunction = "removesubcategory("+subcategoryvalue+")";
        atag.setAttribute("onclick",buttonfunction);
////
////
        var itag =  document.createElement("i");
        itag.setAttribute("class","icon-times");
        atag.appendChild(itag);
        nodediv.appendChild(atag);
////
////
        document.getElementById("selectedproducts").appendChild(nodediv);
//
        document.getElementById("numberofsubcategory").value = subcategorycount;
//
        subcategorycount++;
//
        selectedsubcategoryids.push(subcategoryvalue);
    }

    function removesubcategory(id)
    {

        var subcategorydivid = "#subcategorydivid"+id;
        $(subcategorydivid).remove();

        var elementIndex = selectedsubcategoryids.indexOf(id);
        selectedsubcategoryids.splice(elementIndex, 1);

        subcategorycount--;

    }
</script>
@endsection