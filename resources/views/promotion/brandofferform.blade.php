@extends('layouts.admin-app')
@section('content')

<div class="container">
    <script type="text/javascript">
        function areyousure(){
            return confirm('Are you sure you want to delete this category?');
        }
    </script>
    <style type="text/css">
        .pagination {
            margin:0px;
        }
    </style>
    <div class="page-header">
        <h1>Add Brand Offers &amp; Promotions</h1>
    </div>
    <form action="{{ url('/admin/brand_offers/formpost') }}" method="post" accept-charset="utf-8">
        {{ csrf_field() }}
        <div class="row">
            <div class="col-sm-4">
                <div class="form-group">
                    <label for="offer_name">Offer Name</label>
                    <input type="text" name="offer_name" value="" class="form-control">
                </div>
                <div class="form-group">
                    <label for="start_date">Enable On (UTC)</label>
                    <input type="text" name="start_date" value="" class="form_datetime form-control start_date" readonly="true">
                </div>
                <div class="form-group">
                    <label for="end_date">Disable On (UTC)</label>
                    <input type="text" name="end_date" value="" class="form_datetime form-control end_date" readonly="true">
                </div>
                <div class="form-group">
                    <label for="reduction_amount">Reduction Amount</label>
                    <div class="row">
                        <div class="col-md-6">
                            <select name="reduction_type" class="form-control">
                                <option value="percent">Percentage</option>
                            </select>
                        </div>
                        <div class="col-md-6">
                            <input type="text" name="reduction_amount" value="" class="form-control">
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label>Status</label>
                    <div class="form-group">
                        <select name="enabled_1" class="form-control">
                            <option value="1">Enabled</option>
                            <option value="0">Disabled</option>
                        </select>
                    </div>
                </div>
            </div>

            <div class="col-md-6 col-md-offset-1 well pull-right">
                <div class="form-group">
                    <label>Brands</label>
                    <select class="form-control" name="brand_id">
                        <option value="">Select Brand</option>
                        @foreach ($brans_promo as $brands)
                         <option value="{{$brands->id}}">{{$brands->brands_name}}</option>
                        @endforeach
                       
                    </select>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-10">
                <button type="submit" class="btn btn-primary">Save</button>
            </div>
            <div class="col-md-2"></div>
        </div>
    </form>
    <hr>
    <footer></footer>
</div>
@endsection