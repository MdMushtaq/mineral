@extends('layouts.admin-app')
@section('content')
<div class="page-header">
	<h1>Coupons</h1>
</div>
<a class="btn btn-primary" style="float:right;" href="add-new-coupon.php">
    <i class="icon-plus"></i> Add New Coupon
</a>

<table class="table">
    <thead>
        <tr>
          <th>Code</th>
          <th>Usage</th>
          <th></th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>1000</td>
            <td>1 / 10</td>
            <td class="text-right">
                <div class="btn-group">
                    <a class="btn btn-default" href="#"><i class="icon-pencil"></i></a>
                    <a class="btn btn-danger" href="#" onclick=""><i class="icon-times "></i></a>
                </div>
            </td>
        </tr>
        <tr>
            <td>2000</td>
            <td>5 / 10</td>
            <td class="text-right">
                <div class="btn-group">
                    <a class="btn btn-default" href="#"><i class="icon-pencil"></i></a>
                    <a class="btn btn-danger" href="#" onclick=""><i class="icon-times "></i></a>
                </div>
            </td>
        </tr>
    </tbody>
</table>

@endsection