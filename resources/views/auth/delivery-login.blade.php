<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('public/app.name', 'TechnoSouq') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('public/js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">

    <!-- Styles -->
    <link href="{{ asset('public/css/app.css') }}" rel="stylesheet">
</head>
<body>

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card" style="margin-top: 50px;">

                <div class="card-header text-center" style="font-size:30px;">Delivery Login</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('delivery.login.submit') }}">
                        @csrf

                        <img src="{{asset('public/logo.png')}}"  class="img-responsive" alt="Logo" style="margin-left: auto;margin-right:auto;display:block;">

                        <div class="form-group row">
                            <label class="col-md-4 col-form-label text-md-right">User Name</label>

                            <div class="col-md-6">

                                <input type="text" class="form-control" name="username" value="" required>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-md-4 col-form-label text-md-right">Password</label>

                            <div class="col-md-6">

                                <input type="password" class="form-control" name="password" value="" required>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label  class="col-md-4 col-form-label text-md-right">Verify Mobile Code</label>

                            <div class="col-md-6">
                                <input type="text" class="form-control" name="verification_code" required>


                            </div>
                        </div>

                        {{-- <div class="form-group row">
                            <div class="col-md-6 offset-md-4">
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                                    <label class="form-check-label" for="remember">
                                        {{ __('Remember Me') }}
                                    </label>
                                </div>
                            </div>
                        </div> --}}

                        <div class="form-group row mb-0">
                            <div class="col-md-8 offset-md-4">
                                <button type="submit" class="btn btn-primary btn-lg" style="background-color: #375aa6;">
                                    {{ __('Login') }}
                                </button>
                                {{-- @if (Route::has('password.request'))
                                <a class="btn btn-link" href="{{ route('password.request') }}">
                                    {{ __('Forgot Your Password?') }}
                                </a>
                                @endif --}}
                            </div>
                        </div>
                        <div class="form-group row mb-0">
                            <div class="col-md-8 offset-md-4">
                                <a href="{{ url('/delivery/agents/register') }}">
                                    <button type="button" class="btn btn-primary btn-lg" style="background-color: #375aa6;">
                                        Register Now
                                    </button>
                                </a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>


</body>
</html>


