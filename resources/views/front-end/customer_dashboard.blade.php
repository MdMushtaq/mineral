@extends('layouts.front-app')
@section('content')
<div class="container">
	<div class="row">
		<div class="col-12">
			<div class="page-header">
				<h1 class="mt-3 mb-3">Dashboard</h1>
			</div>
		</div>	
	</div>
</div>		
<div class="container">
	<div class="row">
		<div class="col-lg-4 col-md-3 col-sm-6">
	    	<div class="tile tile-primary">
	            <div class="tile-heading">My Orders
				</div>
				@php
				$ordersCount = 0; 
				$ordersCount = \App\Orders::where(['user_id' => auth()->id()])->count();

				$wishlistCount = 0; 
				$wishlistCount = \App\Wishlist::where(['user_id' => auth()->id()])->count();
				@endphp
	            <div class="tile-body">
	                <i class="fa fa-get-pocket"></i>                    
	                <h2 class="pull-right">{{$ordersCount}}</h2>
	            </div>
	            <div class="tile-footer">
	                <a href="#">View more...</a>
	            </div>
	    	</div>
		</div>
		<div class="col-lg-4 col-md-3 col-sm-6">
	    	<div class="tile tile-primary">
	        	<div class="tile-heading">Wish list
	        	</div>
	            <div class="tile-body">
	                <i class="fa fa-heart-o"></i>
	                 <h2 class="pull-right">{{$wishlistCount}}</h2>
	            </div>
	            <div class="tile-footer">
	                <a href="#">View more...</a>
	            </div>
	    	</div>
		</div>
		{{-- <div class="col-lg-3 col-md-3 col-sm-6">
	    	<div class="tile tile-primary">
	        	<div class="tile-heading">Share
	        	</div>
	            <div class="tile-body">
	                <i class="fa fa-share-alt"></i>
	                <h2 class="pull-right">84</h2>
	            </div>
	            <div class="tile-footer">
	                <a href="#">View more...</a>
	            </div>
	    	</div>
		</div> --}}
		<div class="col-lg-4 col-md-3 col-sm-6">
	    	<div class="tile tile-primary">
	        	<div class="tile-heading">Profile
	        	</div>
	            <div class="tile-body">
	                <i class="fa fa-user"></i>
				<h2 class="pull-right">{{ Auth::user()->name }}</h2>
	            </div>
	            <div class="tile-footer">
	                <a href="#">View more...</a>
	            </div>
	    	</div>
		</div>
	</div>
</div>	

<div class="container">
	<div class="row">
		<div class="col-md-12">
			<h2 class="mt-3 mb-3">My Orders</h2>
			<table class="table table-striped">
			<thead>
				<tr>
				
					<th>Date</th>
					<th>Subtotal</th>
					<th>Vat</th>
					<th>Shipping Price</th>
					<th>Total</th>
					<th>Order Status</th>
					<th>payment</th>
					<th>Action</th>
				</tr>
			</thead>
			<tbody>
				
				
                @foreach ($orderitem as $ordershow)
                    <tr class="text-center">
						
						<td>{{$ordershow->created_at->format('d/m/Y')}}</td> 
						<td>{{$ordershow->subtotal}}</td> 
						<td>{{$ordershow->vat}}</td>
						<td>{{$ordershow->shipping_price == '' ? 0 : $ordershow->shipping_price}}</td> 
						<td>{{$ordershow->total}}</td>
						<td>{{$ordershow->status}}</td>
						<td>{{$ordershow->payment}}</td>
						<td class="text-right">
							<div class="btn-group">
								<a class="btn btn-default" href="{{route('cartitem.show',$ordershow->id)}}">
									<i class="fa fa-eye"></i>
								</a>
							</div>
						</td>
					</tr>
					
                @endforeach
			  
				
				</tbody>
			</table>
		</div>	
	</div>	
</div>
{{-- <div class="container">
	<div class="row">
		<div class="col-md-12" style="text-align:center;">
	    	<a class="btn btn-primary mt-3 mb-3" href="#">View All Sale</a>
		</div>
	</div>
</div> --}}
<div class="container">
	<div class="row">
		<div class="col-md-12">
			<h2 class="mt-3 mb-3">Wish List</h2>
			<table class="table table-striped">
				<thead>
					<tr>
						<th class="gc_cell_right">Product Image</th>
						<th class="gc_cell_right">Product name</th>
						<th class="gc_cell_right">Product Arbic name</th>
						<th class="gc_cell_right">Product Price</th>
						<th class="gc_cell_right">Action</th>
					
					</tr>
				</thead>
				<tbody>
					@foreach ($wishlist as $wishlistitem)
						@php $product = $wishlistitem->products; @endphp
					<tr class="text-center">
						@foreach (json_decode($product->image) as $key => $itemimg)

						@if($key == 0)
							<td >
								<img src="{{asset('public/images/products/'.$itemimg)}}" height="50" width="50" alt="">
							</td>
						@endif
							
						@endforeach
						

						<td >{{$product->prod_name}}</td>
						<td >{{$product->arabic_name}}</td>
						<td >{{$product->price}}</td>
						<td >
							<div class="btn-group">
							<a class="btn btn-default" href="{{route('products.list',$product->id)}}">
									<i class="fa fa-eye"></i>
								</a>
							</div>
						</td>
					</tr>
					@endforeach
				
				
				</tbody>
			</table>
		</div>
	</div>
</div>
{{-- <div class="row">
    <div class="col-md-12" style="text-align:center;">
        <a class="btn btn-primary mt-3 mb-3" href="#">View All Wish List</a>
    </div>
</div> --}}

{{-- <div class="container">
	<div class="row">
		<h2 class="mt-3 mb-3">Share</h2>
		<table class="table table-striped">
			<thead>
				<tr>
		            <th class="gc_cell_left">First Name</th>
		            <th>Last Name</th>
		            <th>Email</th>
		            <th class="gc_cell_right">Active </th>
		  		</tr>
			</thead>
			<tbody>
		        <tr class="text-center">
		        	<td class="gc_cell_left">Muhammad</td>
		        	<td>Abdullah</td>
		            <td>
		                <a href="mailto:sdfsdf@mali.com">Muhammadabdullah@mali.com</a>
		            </td>
		        	<td>Yes</td>
		    	</tr>
		       
		    </tbody>
		</table>
	</div>
</div> --}}
{{-- <div class="row">
    <div class="col-md-12" style="text-align:center;">
        <a class="btn btn-primary mt-3 mb-3" href="#">View All Share</a>
    </div>
</div>     --}}
	
<br>
@endsection