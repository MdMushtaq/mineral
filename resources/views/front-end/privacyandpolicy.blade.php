@extends('layouts.front-app')

@section('content')
<style>
    .c1 h1
    {
        color: #39509c;
        font-family: sans-serif;
    }
    .c1 p
    {
        color: black;
        font-family: sans-serif;
    }
    h2
    {
        color: #39509c;
        font-family: sans-serif;
        font-size: 24px;
    }
    label
    {
        color: black;
    }
    .c1 ul li
    {
        color: black;
        font-family: sans-serif;
    }
</style>


    <div class="container">
        <div class="c1" style="background: #fff;">
            <h1>{{ trans('privacyandpolicy.Privacy and Confidentiality') }}</h1>
            <h2>{{ trans('privacyandpolicy.What Personal Information About Customers Does Technosouq Collect?') }}</h2>
            <p>{{ trans('privacyandpolicy.We collect your personal information in order to provide and continually improve our products and services.') }}</p>
            <p>{{ trans('privacyandpolicy.Here are the types of personal information we collect:') }}</p>
            <ul>
                <li><b>{{ trans('privacyandpolicy.Information You Give Us:') }} </b>{{ trans('privacyandpolicy.We receive and store any information you provide in relation to Technosouq Services. Click here to see examples of what we collect. You can choose not to provide certain information, but then you might not be able to take advantage of many of our Technosouq Services.') }}</li>
                <li><b>{{ trans('privacyandpolicy.Automatic Information:') }} </b>{{ trans('privacyandpolicy.We automatically collect and store certain types of information about your use of Technosouq Services, including information about your interaction with content and services available through Technosouq Services. Like many websites, we use "cookies" and other unique identifiers, and we obtain certain types of information when your web browser or device accesses Technosouq Services and other content served by or on behalf of Technosouq on other websites. Click here to see examples of what we collect.') }}</li>
                <li><b>{{ trans('privacyandpolicy.Information from Other Sources:') }}</b> {{ trans('privacyandpolicy.We might receive information about you from other sources, such as updated delivery and address information from our carriers, which we use to correct our records and deliver your next purchase more easily. Click here to see additional examples of the information we receive.') }}</li>
            </ul>

            <h2>{{ trans('privacyandpolicy.For What Purposes Does Technosouq Use Your Personal Information?') }}</h2>
            <p>{{ trans('privacyandpolicy.We use your personal information to operate, provide, develop, and improve the products and services that we offer our customers. These purposes include:') }}</p>
            <ul>
                <li><b>{{ trans('privacyandpolicy.Purchase and delivery of products and services.') }} </b>{{ trans('privacyandpolicy.We use your personal information to take and handle orders, deliver products and services, process payments, and communicate with you about orders, products and services, and promotional offers.') }}</li>
                <li><b>{{ trans('privacyandpolicy.Provide, troubleshoot, and improve Technosouq Services.') }}  </b> {{ trans('privacyandpolicy.We use your personal information to provide functionality, analyze performance, fix errors, and improve the usability and effectiveness of the Technosouq Services.') }}</li>
                <li><b>{{ trans('privacyandpolicy.Recommendations and personalization.') }} </b> {{ trans('privacyandpolicy.We use your personal information to recommend features, products, and services that might be of interest to you, identify your preferences, and personalize your experience with Technosouq Services.') }}</li>
                <li><b>{{ trans('privacyandpolicy.Recommendations and personalization.') }} </b> {{ trans('privacyandpolicy.When you use our voice, image and camera services, we use your voice input, images, videos, and other personal information to respond to your requests, provide the requested service to you, and improve our services. For more information about Alexa voice services .') }}</li>
                <li><b>{{ trans('privacyandpolicy.Comply with legal obligations.') }} </b> {{ trans('privacyandpolicy.In certain cases, we collect and use your personal information to comply with laws. For instance, we collect from sellers information regarding place of establishment and bank account information for identity verification and other purposes.') }}</li>
            </ul>
        </div>
    </div>
@endsection