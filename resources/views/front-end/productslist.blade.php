@extends('layouts.front-app')
@section('content')
@if (\Session::has('success'))
<div class="alert alert-success">
<h3>{{ trans('productlist.Successfully Added to Your Cart') }} </h3>
</div>
@endif


<section class="pt-2 pb-2" style="background-color:#EFEFEF">
	<div class="container-fluid p-0">
		<div class="row justify-content-center align-items-center">
			<div class="col-md-3">
				<h3 class="title ml-5" style="font-size:18px;">{{ trans('productlist.PRODUCT CATEEGORIES') }}</h3>
			</div>
			<div class="col-md-9">
				<nav class="nav d-block" style="margin-top:10px;">
			        <ol class="breadcrumb no-bg pl-0 pr-0">
			            <li class="breadcrumb-item">
			            	<a href="#">
			            		<i class="fa fa-home" aria-hidden="true"></i> {{ trans('productlist.Home') }}
			            	</a>
			            </li>
			            <li class="breadcrumb-item">
                            @php $cat =App\Categories::where('id',$list->categories_id)->first(); @endphp
                            <a href="{{route('category.list',$cat->id)}}"><i class="fa fa-angle-double-right"></i>
                                @if(\Session::get('locale') == 'ar')
                                {{ $cat->cat_arabic_name }}
                                @else
                                {{ $cat->cat_name }}
                                @endif
                            </a>
			            </li>
			            <li class="breadcrumb-item active">
                            @php $subcat =App\subcategories::where('id',$list->sub_category_id)->first(); @endphp
			            	<a href="{{route('subcategory.list',$subcat->id)}}">
			            		<i class="fa fa-angle-double-right"></i>
                                @if(\Session::get('locale') == 'ar')
                                {{$subcat->sub_cat_arabic_name}}
                                @else
                                {{$subcat->sub_cat_name}}
                                @endif
			            	</a>
			            </li>
			            <li class="breadcrumb-item active">
			            	<a href="#">
			            		<i class="fa fa-angle-double-right"></i>
                                @if(\Session::get('locale') == 'ar')
                                {{$list->arabic_name}}
                                @else
                                {{$list->prod_name}}
                                @endif
			            	</a>
			            </li>
			        </ol>
				</nav>
			</div>
		</div>
	</div>
</section>
<section id="magnific" class="mb-5">
	<div class="container-fluid p-0">
		<div class="row">
            <div class="col-md-1"></div>
			<div class="col-md-4">
                <div class="container">
                    <div class="xzoom-container">
                        @foreach (json_decode($list->arabic_image) as $key => $products)
                        @if($key == 0)
                        <img class="xzoom img-fluid"  id="xzoom-magnific" style="top: 10px;position: relative;border: 1px solid #efefef;"  src="{{asset('public/images/products/'.$products)}}" xoriginal="{{asset('public/images/products/'.$products)}}" />
                        @endif
                        @endforeach

                        <div class="xzoom-thumbs">
                            @foreach (json_decode($list->arabic_image) as $key => $products)
                            <a href="{{asset('public/images/products/'.$products)}}">
                                <img class="xzoom-gallery" width="60" src="{{asset('public/images/products/'.$products)}}" xpreview="{{asset('public/images/products/'.$products)}}">
                            </a>
                            @endforeach
                            @if($list->videolink != '')
                            <a href="#myModal" data-toggle="modal">
                                <img class="xzoom-gallery-video" width="60" src="{{asset('public/frontend/img/products/charger-thumb-6.jpg')}}">
                            </a>
                            @endif
                        </div>
                    </div>    
                </div>  
            </div>
              <div class="col-md-7">
                <h3 class="title d-block bg-primary ml-0 mr-0 mt pt-3 pb-3 pl-2" style="color:#fff;">
                    @if(\Session::get('locale') == 'ar')
                    {{$list->arabic_name}}
                    @else
                    {{$list->prod_name}}
                    @endif
                </h3>
                
                @if($list->discounted_price)
                    
                    <h6 class="title ml-0 mr-0 mt-3 mb-3 pl-2 d-block" style="color:#D4704C;border-bottom:7px solid #D4704C;padding-bottom:10px;">{{ trans('productlist.SR:') }} {{ number_format($list->discounted_price,2)}}</h6>
                    <span style="color:black;" >{{ trans('welcome.SR') }} {{number_format($list->price)}}</span>
                @else
                    <h6 class="title ml-0 mr-0 mt-3 mb-3 pl-2 d-block" style="color:#D4704C;border-bottom:7px solid #D4704C;padding-bottom:10px;">{{ trans('productlist.SR:') }} {{ number_format($list->price,2)}}</h6>
                @endif 
                <div class="table-responsive pl-2">
                  <table class="table table-sm table-borderless mb-0">
                        <tbody>
                          <tr>
                                <th class="pl-0 w-25" scope="row">
                                    <strong>{{ trans('productlist.Vat') }} {{config('cart.tax')}} %</strong>
                                </th>
                                <td>{{ trans('productlist.SR:') }} {{$list->vat}}</td>
                          </tr>
                          <tr>
                            <th class="pl-0 w-25" scope="row">
                                <strong>{{ trans('productlist.Total Price') }}</strong>
                            </th>
                            <td>{{ trans('productlist.SR:') }} {{number_format($list->total)}}</td>
                          </tr>
                        </tbody>
                  </table>
                    <form action="{{route('cart.added',$list->id)}}" method="POST">
                        @csrf
                        <div class="row mt-3 mb-3">
                            <div class="col-sm-3">
                                <div class="input-group">
                                    <span class="input-group-btn minus bg-primary">
                                        -
                                    </span>
                                    <input type="number" class="form-control input-number count" name="qty" value="1">
                                    <span class="input-group-btn plus bg-primary">
                                        +
                                    </span>
                                </div>
                            </div> 
                    
                            <div class="col-sm-8">	
                                <ul class="social">
                                    <li>
                                        <button id="btnFA" type="submit" class="btn btn-primary btn-xs">
                                            <i class="fa fa-shopping-cart"></i>
                                            {{ trans('productlist.ADD TO CART') }}
                                        </button>
                                    </li>
                                    <li>
                                        <a href="{{route('wishlist.add',$list->id)}}" data-tip="Add to Wishlist">
                                            <i class="fa fa-heart-o"></i>
                                        </a>
                                    </li>
                                    <li>
                                        <a data-toggle="dropdown" data-tip="Add to Cart">
                                            <i class="fa fa-share-alt"></i>
                                        </a>
                                        <ul class="dropdown-menu">
                                            <li>
                                                <a class="btn btn-whatsapp" data-placement="left" style="background-color: green;">
                                                    <i class="fa fa-whatsapp" style="color: white"></i>
                                                </a>
                                                <a onclick="addproducturl('{{route('products.list', $list->id)}}')" data-original-title="Email" rel="tooltip" class="btn btn-mail" data-placement="left" data-toggle="modal" data-target="#exampleModal">
                                                    <i class="fa fa-envelope"></i>
                                                </a>
                                            </li>
                                        </ul>
                                    </li>
                                </ul>
                            </div> 
                        </div> 
                        <div class="product-color">
                            <span>{{ trans('productlist.Color') }}</span>
                            <div class="color-choose">
                                @foreach($list->productQuantity as $productattribute)
                                    <input data-image="{{ strtolower($productattribute->colourname)}}" type="radio" id="{{ strtolower($productattribute->colourname)}}" name="product_color" value="{{ json_encode(['id' => $productattribute->id, 'name' => strtolower($productattribute->colourname)]) }}" checked>
                                    <label for="{{ strtolower($productattribute->colourname)}}"><span style="background: {{ strtolower($productattribute->colourname)}}"></span></label>
                                @endforeach
                               
                            </div>
                        </div>     
                    </form>
                </div>
                <h6 class="title d-block ml-0 mr-0 pl-2 mt-3">{{ trans('productlist.SPECIFICATION') }}</h6>
                <p class="pt-1 pl-2">
                    @if(\Session::get('locale') == 'ar')
                    {!! nl2br(e($list->arabic_specification)) !!}
                    @else
                    {!! nl2br(e($list->specification)) !!}
                    @endif
                </p>
            </div>
		</div>
	</div>	
</section>  

<section class="">
	<div class="container p-0">
		<div class="row">
			<div class="col-md-12">
				<div class="classic-tabs pt-1">
					<ul class="nav tabs-primary" id="advancedTab" role="tablist">
					    <li class="nav-item">
					    	<a class="nav-link active show" id="description-tab" data-toggle="tab" href="#description" role="tab" aria-controls="description" aria-selected="true">{{ trans('productlist.Description') }}</a>
					    </li>

					    <li class="nav-item">
					    	<a class="nav-link" id="info-tab" data-toggle="tab" href="#info" role="tab" aria-controls="info" aria-selected="false">{{ trans('productlist.Additional Information') }}</a>
					    </li>
					</ul>

					<div class="tab-content" id="advancedTabContent">
				    	<div class="tab-pane fade show active" id="description" role="tabpanel" aria-labelledby="description-tab">
				      		
                        <p class="pt-1">
                            @if(\Session::get('locale') == 'ar')
                            {!! nl2br(e($list->arabic_description)) !!}
                            @else
                            {!! nl2br(e($list->description)) !!}
                            @endif
                            </p>
				    	</div>

					    <div class="tab-pane fade" id="info" role="tabpanel" aria-labelledby="info-tab">
					    	
						    <table class="table table-striped table-bordered mt-3">
						        <thead>
							        <tr>
							        	<th scope="row" class="w-150 dark-grey-text h6">{{ trans('productlist.Weight') }}</th>
							            <td><em>0.3 {{ trans('productlist.kg') }}</em></td>
							       	</tr>
						        </thead>
						        <tbody>
						        	<tr>
						            	<th scope="row" class="w-150 dark-grey-text h6">{{ trans('productlist.Dimensions') }}</th>
						            	<td><em>50 × 60 {{ trans('productlist.cm') }}</em></td>
						          	</tr>
						        </tbody>
						    </table>
					    </div>
					</div>
				</div>
			</div>	
		</div>
	</div>
</section>

<div class="container-fluid mt-5 mb-5 top-special">
    <h3 class="title">{{ trans('productlist.Recommended') }}</h3> <span>
        <a href="#" class="viewmore">{{ trans('productlist.View More') }} <i class="fa fa-plus"></i></a>
    </span>
</div>


<section class="mb-5">
	<div class="container-fluid cat-product">
		<div class="row">
            @php $getproducts = App\products::where(['categories_id' => $list->categories_id,'status'=> 1 ])->limit(5)->get(); @endphp
            @foreach ($getproducts as $productlist)
                @if ($list->id != $productlist->id)
                    <div class="col-lg-5-cols">
                        <div class="product-block grid-v1">
                            <div class="hovereffect">
                                @foreach (json_decode($productlist->image) as $key => $products)
                                    <a href="{{route('products.list',$productlist->id)}}">
                                    @if($key == 0)
                                    <img class="img-responsive w-100" height="{{$productlist->img_height}}" src="{{asset('public/images/products/'.$products)}}" alt="{{ $productlist->prod_name}}">
                                    @endif

                                    @if($key == 1) 
                                    <img class="img-responsive w-100 hover" src="{{asset('public/images/products/'.$products)}}" alt="{{ $productlist->prod_name}}">
                                    @endif
                                    </a>
                                @endforeach
                                <div class="metas clearfix">
                                    <div class="title-wrapper">
                                        <h3 class="name">
                                            <a href="#" class="card-title">{{$productlist->prod_name}}</a>
                                        </h3>
                                        <span class="price">
                                            <span class="woocommerce-Price-amount amount">
                                                <bdi>SAR {{$productlist->price}}</bdi>
                                            </span>
                                        </span>
                                    </div>
                                </div>
                                <div class="overlay"> 
                                    <div class="social-icons">
                                        <a href="#" class="card-hover">
                                            <button id="btnFA" class="btn btn-primary btn-xs">
                                                <i class="fa fa-shopping-cart"></i>
                                                ADD TO CART
                                            </button>
                                        </a>
                                        <a href="#" data-tip="Add to Wishlist">
                                            <i class="fa fa-heart-o"></i>
                                        </a>
                                        <a href="" data-tip="Add to Cart">
                                            <i class="fa fa-share-alt"></i>
                                        </a>
                                    </div>

                                </div>
                            </div>
                        </div>        
                    </div>
                @endif
          @endforeach
		</div>	
	</div>
</section>

<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Send Email</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="{{ url('/sendproductemail') }}" method="POST">
                @csrf
                <div class="modal-body">
                    <div class="form-group">
                        <input type="hidden" id="producturl" name="producturl">
                        <label>Enter Email</label>
                        <input type="email" name="email" class="form-control" required>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-brimary">Send</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal" onclick="removeproducturl()">Close</button>
                </div>
            </form>
        </div>
    </div>
</div>

 <!-- Modal HTML -->
 <div id="myModal" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">{{$list->prod_name}}</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            </div>
            <div class="modal-body">
                <iframe id="productVideo" width="100%" height="400" src="{{$list->videolink}}" frameborder="0" allowfullscreen></iframe>
            </div>
        </div>
    </div>
</div>
<script>
    function addproducturl(productUrl)
    {
        $('#producturl').val(productUrl);
    }

    function removeproducturl()
    {
        $('#producturl').val("");
    }
</script>
@endsection