@extends('layouts.front-app')
@section('content')
<div class="container">
	@php
	$cartCount = Cart::count();
	@endphp

	@if($cartCount != 0)
    <div class="py-5 text-center">
        <h2>{{ trans('checkout-form.Checkout') }}</h2>
        <p class="lead">{{ trans('checkout-form.Need Some Informtion') }}</p>
    </div>
    <div class="stepwizard col-md-offset-3">
    	<div class="stepwizard-row setup-panel">
      		<div class="stepwizard-step">
        		<a href="#step-1" type="button" class="btn in-active-step btn-circle">1</a>
        		<p>{{ trans('checkout-form.Account') }}</p>
      		</div>
      		<div class="stepwizard-step">
        		<a href="#step-2" type="button" class="btn btn-default btn-circle btn-primary" disabled="disabled">2</a>
        		<p>{{ trans('checkout-form.Address') }}</p>
      		</div>
      		<div class="stepwizard-step">
        		<a href="#step-3" type="button" class="btn btn-default btn-circle in-active-step" disabled="disabled">3</a>
        		<p>{{ trans('checkout-form.Payment') }}</p>
      		</div>
    	</div>
  	</div>
    <div class="row">
        <div class="col-md-4 order-md-2 mt-4 mb-4 border-gray p-0">
            <div class="payment">
            	<h4 class="mt-3 ml-4">{{ trans('checkout-form.Cart - item') }}(s)</h4>
            </div>	
			
			
			@foreach ($cart as $item)
				@php $product = $item->product; @endphp
				<div class="col-md-12 mt-4 mb-4 border-bottom-img">
					<div class="row product-container">
						<div class="col-md-4 col-sm-4 col-xs-4">
							<div class="product-img">
                                @foreach (json_decode($product->image) as $key => $products)

                                @if($key == 0)
                                <img height="100" width="100" src="{{asset('public/images/products/'.$products)}}" class="img-thumbnail rounded" >
                                @endif
                                @endforeach
                              
							</div>
						</div>
						<div class="col-md-8 col-sm-8 col-xs-8">
							@php $options = []; @endphp
							@foreach ($item->attributes as $attributeKey => $option)
								@php $options[] = ucfirst($attributeKey == 'color' ? $option->name : $option); @endphp
							@endforeach
								<p class="product-text">
									@if(\Session::get('locale') == 'ar')
									{{$product->arabic_name }}
									@else
									{{$product->prod_name}}
									@endif
								</p>
								<p>{{$item->qty}} x {{$item->price}}</p>
								<p>{{ implode(', ', $options) }}</p>
							</div>
					</div>	
				</div>
			@endforeach

    	
    		<div class="payment">
            	<h4 class="mt-3 ml-4">{{ trans('checkout-form.Total') }}</h4>
            </div>

    		<div class="col-md-12 mt-4 mb-4 border-bottom-img">
    			<div class="row product-container">
	        		<div class="col-md-12 col-sm-12 col-xs-12">
	            		<table class="table">
							@php 
							$subTotal = Cart::subtotal();
							$vat = number_format((config('cart.tax') * $subTotal) / 100, 2);
							$shipping = number_format($city->Shipping->shippingrate, 2);
							$total = number_format($subTotal + $vat + $shipping,2);
							@endphp
                			<tbody>
                				<tr>
                					<td><strong> {{ trans('checkout-form.Subtotal:') }}</strong></td>
                					<td class="text-right">{{ trans('checkout-form.SR') }} {{number_format($subTotal, 2)}}</td>
								</tr>
								<tr>
									<td><strong>{{ trans('checkout-form.VAT') }} {{config('cart.tax')}}%:</strong></td>
									<td class="text-right">{{ trans('checkout-form.SR') }} {{$vat}}</td>
								</tr>
							
								<tr>
								<td><strong>Shipping</strong></td>
									<td class="text-right" id="shipping">
										{{ $city->name != 'Other' ? trans('checkout-form.SR').' '.$shipping : 'Shipping with Aramax'}}
									</td>
								</tr>
								<tr>
									<td><strong>{{ trans('checkout-form.Total:') }}</strong></td>
									<td class="text-right" id="total">{{ trans('checkout-form.SR') }} {{ $total }}</td>
								</tr>
								<tr></tr>
                			</tbody>
            			</table>
	        		</div>
	        	</div>	
    		</div>
        </div>

        <div class="col-md-8 order-md-1 mt-4 mb-4">
        	<div class="payment">
            	<h4 class="mt-3 ml-4">{{ trans('checkout-form.Payment & Shipping Address') }}</h4>
            </div>	
                <div class="row mt-3">
                    <div class="col-md-12 mb-6">
                        <label for="firstName">{{ trans('checkout-form.Name') }}</label>
					<input type="text" class="form-control" id="firstName" value="{{$customerinfo->name}}" placeholder="First name" readonly>
                        
                    </div>
                </div>
                <div class="mb-3">
                    <label for="email">{{ trans('checkout-form.Email') }} </label>
                    <input type="email" class="form-control" value="{{$customerinfo->email}}" id="email" placeholder="Email" readonly>
                </div>
                <div class="mb-3">
                    <label for="mobile-number">{{ trans('checkout-form.Mobile Number') }}</label>
                    <input type="tel" class="form-control" value="{{$customerinfo->phone}}" id="mobile-number" placeholder="Mobile number" readonly>
                    
                </div>	
				<div class="mb-3">
                    <label for="country">{{ trans('checkout-form.Country') }}</label>
                    <input type="text" class="form-control" id="country" value="{{$customerinfo->country}}" readonly>
                    
				</div>
				<div class="mb-3">
                    <label for="city">{{ trans('checkout-form.City') }}</label>
                    <input type="text" class="form-control" id="city" value="{{$city->name}}" readonly>
                    
                </div>

                <div class="mb-3">
                    <label for="address">{{ trans('checkout-form.Address') }}</label>
                    <input type="text" class="form-control" id="address" value="{{$customerinfo->address}}" readonly>
                    
                </div>
                <div class="custom-control custom-checkbox mb-3">
                    <input type="checkbox" class="custom-control-input" id="ship-to-another">
                    <label class="custom-control-label" for="ship-to-another">{{ trans('checkout-form.Shipping to another address') }}</label>
				</div>

				<form action="{{route('checkout-form.post')}}" method="POST" >
					@csrf

					<div id="changeShipInputs">
						<div class="mb-3">
							<label for="person-name">{{ trans('checkout-form.Name') }}</label>
							<input type="text" class="form-control" name="shipping_name" id="person-name" placeholder="Name" >
						
						</div>

						<div class="mb-3">
							<label for="person-mob-no">{{ trans('checkout-form.Mobile Number') }}</label>
							<input type="tel" class="form-control" id="person-mob-no" name="shipping_phone" placeholder="Mobile Number" >
						
						</div>
						<div class="mb-3">
							@php $city = App\City::all(); @endphp
							<label for="shipp_city">City</label>
							<select name="shipp_city" id="cities" onchange="myCityFunction();" class="form-control">
								@foreach ($city as $cities)
									<option value="" style="display: none">Select city</option>
									<option value="{{$cities->id}}">{{$cities->name}}</option>
								@endforeach
							</select>
						</div> 
						<div class="mb-3">
							<label for="person-mob-no">{{ trans('checkout-form.Address') }}</label>
							<input type="text" class="form-control" id="another-ship-address" name="shipping_address" placeholder="Another ship address" > 
						</div> 
					</div>    
					<button class="btn btn-primary btn-lg btn-block" type="submit">{{ trans('checkout-form.Save Address') }}</button>
					<div class="custom-control custom-checkbox mt-3 mb-3">
						<input type="checkbox" class="custom-control-input" required id="agree-terms">
						<label class="custom-control-label" for="agree-terms">
							<a href="#" target="_blank">{{ trans('checkout-form.I have read and agree to the Terms & Conditions') }}</a>
						</label>
					</div>
				</form>
        </div>
    </div>
    @else
        
		<div class="text-center">
			<p>{{ trans('checkout-form.There are no items in this cart') }}</p>
			<a href="{{ url('/') }}" type="button" class=" btn btn-success">{{ trans('checkout-form.Continue Shoping') }}</a>
		</div>
	@endif
</div>

<script>
	function myCityFunction() {
		let city_id = $('#cities').val()
		if (city_id) {
			$.ajaxSetup({
				headers: {
					'X-CSRF-TOKEN': '{{csrf_token()}}'
				}
			});
			$.post('{{route('city.change')}}', {
				city_id
			}, function ({shipping, total}) {
				$('#shipping').html(shipping)
				$('#total').html(total)
			})
		}
	}
</script>
@endsection