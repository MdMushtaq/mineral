@extends('layouts.front-app')

@section('content')
<style>
    .c1 h1
    {
        color: #39509c;
        font-family: sans-serif;
    }
    .c1 p
    {
        color: black;
        font-family: sans-serif;
    }
    h2
    {
        color: #39509c;
        font-family: sans-serif;
    }
    label
    {
        color: black;
    }
</style>
<section class="pb-5" >
<div class="container">
    <div class="c1">
        <h1>{{ trans('contact.CONTACT US') }}</h1>
        <p>{{ trans('contact.Feel free to reach us at anytime with your inquires, concerns or feedback.') }}</p>
        <p>{{ trans('contact.Telephone: +966-12-6694622') }}</p>
        <p>{{ trans('contact.Fax: +966-12-6673075') }}</p>
        <p>{{ trans('contact.P.O Box: 1338, Jeddah 21431 Saudi Arabia') }}</p>
    </div>
    <div class="row">
        <div class="col-md-6">
            <h2>{{ trans('contact.Get In Touch With Us') }}</h2>
            <form action="{{ url('') }}" method="post">
                @csrf
                <div class="row">
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="name">{{ trans('contact.Name') }}</label>
                                    <input type="text" name="name" class="form-control" required>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="name">{{ trans('contact.Email') }}</label>
                                    <input type="email" name="name" class="form-control" required>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="name">{{ trans('contact.Mobile Contact Number') }}</label>
                                    <input type="text" name="mobilenumber" class="form-control" required>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="name">{{ trans('contact.Order Number If you have an order') }}</label>
                                    <input type="text" name="ordernumber" class="form-control" required>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="name">{{ trans('contact.Enquiry') }}</label>
                                    <input type="text" name="enquiry" class="form-control" required>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="form-group">
                                    <button type="submit" class="btn btn-primary">{{ trans('contact.Save') }}</button>
                                </div>
                            </div>
                        </div>
                    </div>

            </form>

        </div>
    </div>
</div>


</section>
<script>
    function myMap() {
        var mapProp= {
            center:new google.maps.LatLng(51.508742,-0.120850),
            zoom:5,
        };
        var map = new google.maps.Map(document.getElementById("googleMap"),mapProp);
    }
</script>

<script src="https://maps.googleapis.com/maps/api/js?key=YOUR_KEY&callback=myMap"></script>
@endsection