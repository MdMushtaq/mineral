@extends('layouts.front-app')
@section('content')
<!--====== SLIDER PART START ======-->

<section id="slider-part" class="bg_cover slider-part-2" style="background-image: url(public/frontend/images/bg-3.jpg)">
    <div class="content-slied">
        <div class="single-slider d-flex align-items-center">
            <div class="container">
                <div class="row">
                    <div class="col-xl-6 col-lg-7 col-md-11">
                        <div class="slider-content">
                            <h2 data-animation="fadeInUp" data-delay="1s">Always want safe and good water for healthy life</h2>
                            <p data-animation="fadeInUp" data-delay="1.5s">Nunc molestie mi nunc, nec accumsan libero dignissim sit amet. Fusce sit amet tincidunt metus. Nunc eu risus suscipit massa dapibu.</p>
                            <a data-animation="fadeInUp" data-delay="2s" href="#">Learn More</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="single-slider d-flex align-items-center">
            <div class="container">
                <div class="row">
                    <div class="col-xl-6 col-lg-7 col-md-11">
                        <div class="slider-content">
                            <h2 data-animation="fadeInUp" data-delay="1s">Always want safe and good water for healthy life</h2>
                            <p data-animation="fadeInUp" data-delay="1.5s">Nunc molestie mi nunc, nec accumsan libero dignissim sit amet. Fusce sit amet tincidunt metus. Nunc eu risus suscipit massa dapibu.</p>
                            <a data-animation="fadeInUp" data-delay="2s" href="#">Learn More</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="single-slider d-flex align-items-center">
            <div class="container">
                <div class="row">
                    <div class="col-xl-6 col-lg-7 col-md-11">
                        <div class="slider-content">
                            <h2 data-animation="fadeInUp" data-delay="1s">Always want safe and good water for healthy life</h2>
                            <p data-animation="fadeInUp" data-delay="1.5s">Nunc molestie mi nunc, nec accumsan libero dignissim sit amet. Fusce sit amet tincidunt metus. Nunc eu risus suscipit massa dapibu.</p>
                            <a data-animation="fadeInUp" data-delay="2s" href="#">Learn More</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<!--====== SLIDER PART ENDS ======-->


<!--====== ABOUT US PART START ======-->

<section class="pt-70 pb-70" data-v-06a301af="" id="about-us">
    <div class="container" data-v-06a301af="">
        <div class="row justify-content-center" data-v-06a301af="">
            <div class="col-12" data-v-06a301af="">
                <div class="section-title text-center type3" data-v-06a301af="">
                        <span class="schoolbell" data-v-06a301af="">Wellcome To Mineralo
                        </span>
                    <h2 data-v-06a301af="">Most Trusted Company</h2>
                </div>
            </div>
        </div>
        <div class="row" data-v-06a301af="">
            <div class="col-md-6 col-lg-3" data-v-06a301af="">
                <div class="single-feature-inner text-center" data-v-06a301af="">
                    <div class="feature-icon" data-v-06a301af="">
                        <img src="{{ asset('public/frontend/images/icons/water-bottle.svg') }}" alt="" class="svg" data-v-06a301af="">
                    </div>
                    <h4 data-v-06a301af="">Soft Drinking Water</h4>
                    <p data-v-06a301af="">Sed quia magni eos qui ratione volup tatem.
                    </p>
                    <a href="#" data-v-06a301af="">
                        <i class="fa fa-arrow-circle-o-right" data-v-06a301af="">
                        </i>More
                    </a>
                </div>
            </div>
            <div class="col-md-6 col-lg-3" data-v-06a301af="">
                <div class="single-feature-inner text-center" data-v-06a301af="">
                    <div class="feature-icon" data-v-06a301af="">
                        <img src="{{ asset('public/frontend/images/icons/glass-of-water.svg') }}" alt="" class="svg" data-v-06a301af="">
                    </div>
                    <h4 data-v-06a301af="">Cholorine Free Water</h4>
                    <p data-v-06a301af="">Sed quia magni eos qui ratione volup tatem.
                    </p>
                    <a href="#" data-v-06a301af="">
                        <i class="fa fa-arrow-circle-o-right" data-v-06a301af="">
                        </i>More
                    </a>
                </div>
            </div>
            <div class="col-md-6 col-lg-3" data-v-06a301af="">
                <div class="single-feature-inner text-center" data-v-06a301af="">
                    <div class="feature-icon" data-v-06a301af="">
                        <img src="{{ asset('public/frontend/images/icons/laboratory(1).svg') }}" alt="" class="svg" data-v-06a301af="">
                    </div>
                    <h4 data-v-06a301af="">Pure Mineralized Water</h4>
                    <p data-v-06a301af="">Sed quia magni eos qui ratione volup tatem.
                    </p>
                    <a href="#" data-v-06a301af="">
                        <i class="fa fa-arrow-circle-o-right" data-v-06a301af=""></i>More
                    </a>
                </div>
            </div>
            <div class="col-md-6 col-lg-3" data-v-06a301af="">
                <div class="single-feature-inner text-center" data-v-06a301af="">
                    <div class="feature-icon" data-v-06a301af="">
                        <img src="{{ asset('public/frontend/images/icons/laboratory.svg') }}" alt="" class="svg" data-v-06a301af="">
                    </div>
                    <h4 data-v-06a301af="">Total Home Solution</h4>
                    <p data-v-06a301af="">Sed quia magni eos qui ratione volup tatem.</p>
                    <a href="#" data-v-06a301af="">
                        <i class="fa fa-arrow-circle-o-right" data-v-06a301af=""></i>More
                    </a>
                </div>
            </div>
        </div>
    </div>
</section>

<!--====== ABOUT US PART ENDS ======-->


<!--====== PRODUCTS PART START ======-->

<section id="products-part" class="pt-30 pb-30">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-8">
                <div class="custom-title text-center">
                    <h2>Our Products</h2>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-4 col-md-6 col-sm-6">
                <div class="singel-products mt-30">
                    <div class="products-image">
                        <img src="{{ asset('public/frontend/images/product/p-1.jpg') }}" alt="Products">
                        <div class="new-sele">
                            <a href="#">New</a>
                        </div>
                    </div>
                    <div class="products-contant">
                        <h6 class="products-title"><a href="#">Mineral water big bottle</a></h6>
                        <div class="price-rating d-flex justify-content-between">
                            <div class="price">
                                <span class="regular-price">$259</span>
                                <span class="discount-price">$215</span>
                            </div>
                            <div class="rating">
                                <ul>
                                    <li><i class="fa fa-star"></i></li>
                                    <li><i class="fa fa-star"></i></li>
                                    <li><i class="fa fa-star"></i></li>
                                    <li><i class="fa fa-star"></i></li>
                                    <li><i class="fa fa-star"></i></li>
                                </ul>
                            </div>
                        </div>
                        <p class="text">Mauris sed massa eu nulla mollis vehicula quis id tortor</p>
                        <div class="products-cart">
                            <a class="cart-add" href="#"><i class="fa fa-shopping-cart"></i> Add to cart</a>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-lg-4 col-md-6 col-sm-6">
                <div class="singel-products mt-30">
                    <div class="products-image">
                        <img src="{{ asset('public/frontend/images/product/p-2.jpg') }}" alt="Products">
                    </div>
                    <div class="products-contant">
                        <h6 class="products-title"><a href="#">Mineral water big bottle</a></h6>
                        <div class="price-rating d-flex justify-content-between">
                            <div class="price">
                                <span class="regular-price">$259</span>
                                <span class="discount-price">$215</span>
                            </div>
                            <div class="rating">
                                <ul>
                                    <li><i class="fa fa-star"></i></li>
                                    <li><i class="fa fa-star"></i></li>
                                    <li><i class="fa fa-star"></i></li>
                                    <li><i class="fa fa-star"></i></li>
                                    <li><i class="fa fa-star"></i></li>
                                </ul>
                            </div>
                        </div>
                        <p class="text">Mauris sed massa eu nulla mollis vehicula quis id tortor</p>
                        <div class="products-cart">
                            <a class="cart-add" href="#"><i class="fa fa-shopping-cart"></i> Add to cart</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-6 col-sm-6">
                <div class="singel-products mt-30">
                    <div class="products-image">
                        <img src="{{ asset('public/frontend/images/product/p-3.jpg') }}" alt="Products">
                        <div class="new-sele">
                            <a href="#">Sale</a>
                        </div>
                    </div>
                    <div class="products-contant">
                        <h6 class="products-title"><a href="#">Mineral water big bottle</a></h6>
                        <div class="price-rating d-flex justify-content-between">
                            <div class="price">
                                <span class="regular-price">$259</span>
                                <span class="discount-price">$215</span>
                            </div>
                            <div class="rating">
                                <ul>
                                    <li><i class="fa fa-star"></i></li>
                                    <li><i class="fa fa-star"></i></li>
                                    <li><i class="fa fa-star"></i></li>
                                    <li><i class="fa fa-star"></i></li>
                                    <li><i class="fa fa-star"></i></li>
                                </ul>
                            </div>
                        </div>
                        <p class="text">Mauris sed massa eu nulla mollis vehicula quis id tortor</p>
                        <div class="products-cart">
                            <a class="cart-add" href="#"><i class="fa fa-shopping-cart"></i> Add to cart</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<!--====== PRODUCTS PART ENDS ======-->

<!--====== SERVICES PART START ======-->

<section id="services-part" class="services-part-2 pt-30 pb-30">
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-lg-8">
                <div class="custom-title text-center">
                    <h2>Why Choose Us ?</h2>
                </div>
            </div>
        </div>
        <div class="row align-items-center">
            <div class="col-lg-4">
                <div class="singel-services mt-45 pb-50">
                    <div class="services-icon">
                        <img src="{{ asset('public/frontend/images/choose-us/icon-1.png') }}" alt="Icon">
                    </div>
                    <div class="services-cont pt-25 pl-70">
                        <h4>Aliquam congue fermentum</h4>
                        <p>Nam ut pharetra enim, in tincidunt orci. Ut sed neque dolor. Nullam auctor placerat ipsum. In finibus tortor pulvinar pulvinar laoreet. Quisque id nibh non lectus dictum dapibus quis ac urna.</p>
                        <a href="#">Read More <span><i class="fa fa-long-arrow-right"></i></span></a>
                    </div>
                </div>

                <div class="singel-services">
                    <div class="services-icon">
                        <img src="{{ asset('public/frontend/images/choose-us/icon-3.png') }}" alt="Icon">
                    </div>
                    <div class="services-cont pt-25 pl-70">
                        <h4>Aliquam congue fermentum</h4>
                        <p>Nam ut pharetra enim, in tincidunt orci. Ut sed neque dolor. Nullam auctor placerat ipsum. In finibus tortor pulvinar pulvinar laoreet. Quisque id nibh non lectus dictum dapibus quis ac urna.</p>
                        <a href="#">Read More <span><i class="fa fa-long-arrow-right"></i></span></a>
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="singel-services mt-50 text-center">
                    <img src="{{ asset('public/frontend/images/choose-us/services-1.jpg') }}" alt="Image">
                </div>
            </div>
            <div class="col-lg-4">
                <div class="singel-services right mt-45 text-right pb-50">
                    <div class="services-icon">
                        <img src="{{ asset('public/frontend/images/choose-us/icon-2.png') }}" alt="Icon">
                    </div>
                    <div class="services-cont pt-25 pr-70">
                        <h4>Aliquam congue fermentum</h4>
                        <p>Nam ut pharetra enim, in tincidunt orci. Ut sed neque dolor. Nullam auctor placerat ipsum. In finibus tortor pulvinar pulvinar laoreet. Quisque id nibh non lectus dictum dapibus quis ac urna.</p>
                        <a href="#">Read More <span><i class="fa fa-long-arrow-right"></i></span></a>
                    </div>
                </div>

                <div class="singel-services right text-right ">
                    <div class="services-icon">
                        <img src="{{ asset('public/frontend/images/choose-us/icon-4.png') }}" alt="Icon">
                    </div>
                    <div class="services-cont pt-25 pr-70">
                        <h4>Aliquam congue fermentum</h4>
                        <p>Nam ut pharetra enim, in tincidunt orci. Ut sed neque dolor. Nullam auctor placerat ipsum. In finibus tortor pulvinar pulvinar laoreet. Quisque id nibh non lectus dictum dapibus quis ac urna.</p>
                        <a href="#">Read More <span><i class="fa fa-long-arrow-right"></i></span></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<!--====== SERVICES PART ENDS ======-->

<!--====== DELIVERY PART START ======-->

<section id="delivery-part" class="delivery-part-2 bg_cover pt-95 pb-100" data-overlay="8" style="background-image: url(public/frontend/images/bg-2.jpg)">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-lg-7 offset-xl-1">
                <div class="delivery-text text-center pb-30">
                    <h2>Water Delivery 20 k.m.  Free Service</h2>
                    <p>Nunc molestie mi nunc, nec accumsan libero dignissim sit amet. Fusce sit amet tincidunt metus. Nunc eu risus suscipit massa dapibu.</p>
                    <a href="#">Read More</a>
                </div>
            </div>
        </div>
    </div>
    <div class="delivery-image d-none d-lg-flex align-items-end">
        <img src="{{ asset('public/frontend/images/delivery-van.png') }}" alt="Iamge">
    </div>
</section>

<!--====== DELIVERY PART ENDS ======-->


<!--====== SALOGAN PART START ======-->

<section id="sologan-part" class="services-part-2 pt-90 pb-90">
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-lg-12"></div>
        </div>
    </div>
</section>

<!--====== SALOGAN PART ENDS ======-->
@endsection