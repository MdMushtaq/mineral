@extends('layouts.front-app')

@section('content')
<style>
    .c1 h1
    {
        color: #39509c;
        font-family: sans-serif;
    }
    .c1 p
    {
        color: black;
        font-family: sans-serif;
    }
    h2
    {
        color: #39509c;
        font-family: sans-serif;
    }
    label
    {
        color: black;
    }
</style>
<section class="pb-5" >
    <div class="container">
        <div class="c1" style="background: #fff;">
            <h1>{{ trans('careers.Job Portal') }}</h1>
            <p>{{ trans('careers.Achieving success by respecting and empowering personal potential.') }}</p>
            <p>{{ trans('careers.Our passion and ingenuity fuels inspired design. We are always looking for talented, dedicated and passionate teammates who want to carve their own path exploring technological innovation in engineering. Visit our careers page to find out more!') }}</p>
            <br>
            <b><p>{{ trans('careers.There is no job available right now. Kindly upload your CV we will contact you asap.') }}</p></b>
        </div>

        @if ($message = Session::get('success'))
        <div class="alert alert-success alert-block">
            <button type="button" class="close" data-dismiss="alert">×</button>
            <strong>{{ $message }}</strong>

        </div>
        @endif
        @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        @endif
        <form method="POST" action="{{ url('careers') }}" enctype="multipart/form-data">
            @csrf
            <br>
            <div class="form-group input-group">
                <input class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ old('name') }}"  placeholder="{{ trans('careers.Your Name') }}" type="text">

            </div>

            <div class="form-group input-group">
                <input class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" placeholder="{{ trans('careers.Your Email') }}" type="email">
            </div>

            <div class="form-group input-group">
                <input class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="phonenumber" value="{{ old('email') }}" placeholder="{{ trans('careers.Phone Number') }}" type="text">
            </div>

            <div class="form-group input-group">
                <input class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="jobtitle" value="{{ old('email') }}" placeholder="{{ trans('careers.Job Title') }}" type="text">
            </div>

            <div class="form-group input-group">

                <input type="file" name="document">
            </div>

            <div class="form-group">
                <button type="submit" class="btn btn-primary btn-block"> {{ trans('careers.Register') }}  </button>
                <br>
                <button  class="btn btn-success btn-block"> {{ trans('careers.Back') }}  </button>
            </div>
           
        </form>

    </div>


</section>
@endsection