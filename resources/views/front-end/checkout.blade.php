@extends('layouts.front-app')
@section('content')

{{-- @php 
$cartCount = 0;
if (auth()->check()) {
    $cartCount = \App\Cart::where(['user_id' => auth()->id(), 'status' => 0])->get();
    $productclist = 
}
@endphp --}}

@if (\Session::has('danger'))
<div class="alert alert-danger">
<h3>{{ \Session::get('danger') }} </h3>
</div>
@endif


<div class="px-4 px-lg-0">
	<div class="pb-5">
        @php 
        $cartCount = Cart::count();
        @endphp
        
		@if($cartCount != 0)
		<div class="container py-5 text-center">
			<h2>{{ trans('checkout.Your Cart') }}</h2>
		</div>
		<div class="container-fluid">
			<div class="row">
				<div class="col-lg-12 pl-5 pr-5 bg-white rounded">
					<div class="table-responsive">
						<table class="table cart-table" cellspacing="0">
							<thead>
								<tr>
									<th class="product-remove">&nbsp;</th>
									<th class="product-thumbnail">{{ trans('checkout.Image') }}</th>
									<th class="product-name">{{ trans('checkout.Product') }}</th>
									<th class="product-color">{{ trans('checkout.Color') }}</th>
									<th class="product-price">{{ trans('checkout.Price') }}</th>
									<th class="product-quantity">{{ trans('checkout.Quantity') }}</th>
									
									<th class="product-subtotal">{{ trans('checkout.Total') }}</th>
								</tr>
							</thead>
							<tbody>
                               
								@foreach ($cart as $item)
								@php $product = $item->product; @endphp

									<tr>
										<td class="product-remove">
											<a href="{{route('remove.cart',$item->rowId)}}" class="remove">
												<i class="fa fa-times"></i>
											</a>						
										</td>
										
										<td class="product-thumbnail">
                                            @foreach (json_decode($product->image) as $key => $products)
												@if($key == 0)
												<a href="#">
													<img width="100" width="100" src="{{asset('public/images/products/'.$products)}}" class="img-fluid rounded shadow-sm" alt="" >
												</a>
												@endif
                                            @endforeach
                                           
										</td>
										
										<td class="product-name">
										<a href="#">
                                            @if(\Session::get('locale') == 'ar')
                                            {{$product->arabic_name}}
                                            @else
                                            {{$product->prod_name}}
                                            @endif
                                        </a>
										</td>
										@php $options = []; @endphp
										@foreach ($item->attributes as $attributeKey => $option)
											@php $options[] = ucfirst($attributeKey == 'color' ? $option->name : $option); @endphp
										@endforeach
										
										<td style="font-weight: bold;color:#000;">
											{{ implode(', ', $options) }}
										</td>

										<td class="product-price">
											<span class="amount">
												<bdi>
													<span class="currencySymbol">{{ trans('checkout.SR') }}</span>
                                                    {{$item->price}}
												</bdi>
											</span>
										</td>
										<form action="{{route('quantity.update',$item->rowId)}}" method="post">
											@csrf
											<td class="product-quantity" style="text-align: -webkit-center;">
												<div class="number-input md-number-input">
													<button onclick="this.parentNode.querySelector('input[type=number]').stepDown()" class="minus"></button>
													<input class="quantity" min="1" name="quantity" value="{{$item->qty}}" type="number">
													<button onclick="this.parentNode.querySelector('input[type=number]').stepUp()" class="plus"></button>
												</div>
											</td>
										</form>
										<td class="product-subtotal">
											<span class="price-amount amount">
												<bdi>
													<span class="currencySymbol">{{ trans('checkout.SR') }}</span> {{$item->total}}
												</bdi>
											</span>						
										</td>
									</tr>
								@endforeach
								
							</tbody>
						</table>
					</div>
				</div>
			</div>
			@php 
				$subTotal = Cart::subtotal();
				$vat = number_format((config('cart.tax') * $subTotal) / 100, 2);
				$total = number_format($subTotal + $vat, 2 );
				
			@endphp
			<div class="row bg-white">
                <div class="col-lg-6">
					<a href="{{ url('/') }}" type="button" style="    margin-top: 300px;" class="btn btn-dark  btn-primary">{{ trans('checkout.Continue Shoping') }}</a>
				</div>
                <div class="col-lg-6 rounded-pill">
					<div class="px-4 py-3 text-uppercase font-weight-bold" style="font-size:30px;">{{ trans('checkout.Cart totals') }}
					</div>
				  	<div class="p-4" style="border: 1px solid;">
					    <ul class="list-unstyled mb-4">
					    	<li class="d-flex justify-content-between py-3 border-bottom">
								<strong class="text-muted">{{ trans('checkout.Order Subtotal') }} </strong>
								
							<strong>{{ trans('checkout.SR') }} {{number_format($subTotal, 2)}}</strong>
					    	</li>
					      
					      	<li class="d-flex justify-content-between py-3 border-bottom">
							  <strong class="text-muted">{{ trans('checkout.VAT') }} {{config('cart.tax')}}%</strong>
					      		<strong>{{ trans('checkout.SR') }} {{$vat}}</strong>
							  </li>
							<li class="d-flex justify-content-between py-3 border-bottom">
								<strong class="text-muted">{{ trans('checkout.Total') }}</strong>
								
							<h5 class="font-weight-bold"> {{ $total}}</h5>
							</li>
					    </ul>
				    	<a href="{{route('checkout.form')}}" class="btn btn-dark rounded py-2 btn-block btn-primary">{{ trans('checkout.Procceed to checkout') }}</a>
				  </div>
				</div>
            </div>
	
        </div>
        @else
        
        <div class="text-center">
			<p>{{ trans('checkout.There are no items in this cart') }}</p>
        	<a href="{{ url('/') }}" type="button" class="btn btn-dark  btn-primary">{{ trans('checkout.Continue Shoping') }}</a>
        </div>
        @endif
       
  	</div>
</div>

@endsection