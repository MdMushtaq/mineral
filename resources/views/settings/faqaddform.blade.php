@extends('layouts.admin-app')
@section('content')

@if(count($errors))
<div class="alert alert-danger">
    <strong>Whoops!</strong> There were some problems with your input.
    <br/>
    <ul>
        @foreach($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
    </ul>
</div>
@endif
@if ($message = Session::get('success'))
<div class="alert alert-success alert-block">

    <button type="button" class="close" data-dismiss="alert">×</button>

    <strong>{{ $message }}</strong>

</div>
@endif

<div class="container">
    <div class="page-header"><h1>Faq Add</h1></div>
    <br>

    <form action="{{ url('/admin/faq/addpost') }}" enctype="multipart/form-data" method="post" accept-charset="utf-8">
        @csrf

        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label for="name">Category</label>
                    <select name="faqcategoryid" class="form-control" required>
                        <option value="">--Select Faq Category--</option>
                        @foreach($faqcategories as $faqcategory)
                        <option value="{{ $faqcategory->id }}">
                            {{ $faqcategory->category_name }}
                        </option>
                        @endforeach
                    </select>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="name">Question</label>
                            <textarea rows="2" class="form-control" name="question" required></textarea>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="name">Answer</label>
                            <textarea rows="5" class="form-control" name="answer" required></textarea>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="name">Arabic Question</label>
                            <textarea rows="2" class="form-control" name="arabicquestion" required dir="rtl"></textarea>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="name">Arabic Answer</label>
                            <textarea rows="5" class="form-control" name="arabicanswer" required dir="rtl"></textarea>
                        </div>
                    </div>

                </div>
            </div>

            <div class="row">
                <div class="col-md-10">
                    <button type="submit" class="btn btn-primary">Save</button>
                </div>

            </div>
    </form>
</div>

@endsection
