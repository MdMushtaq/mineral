@extends('layouts.admin-app')
@section('content')

@if(count($errors))
<div class="alert alert-danger">
    <strong>Whoops!</strong> There were some problems with your input.
    <br/>
    <ul>
        @foreach($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
    </ul>
</div>
@endif
@if ($message = Session::get('success'))
<div class="alert alert-success alert-block">

    <button type="button" class="close" data-dismiss="alert">×</button>

    <strong>{{ $message }}</strong>

</div>
@endif

<div class="container">
    <div class="page-header"><h1>Edit Faq Category</h1></div>

    <form action="{{ url('/admin/faq/category/editpost') }}" enctype="multipart/form-data" method="post" accept-charset="utf-8">
        @csrf
        <input type="hidden" name="faqcategoryid" value="{{ $faqcategory->id }}">
        <div class="row">
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="name">Category Name</label>
                            <input type="text" class="form-control" name="categoryname" value="{{ $faqcategory->category_name }}">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="name">Arabic Category Name</label>
                            <input type="text" class="form-control" name="arabiccategoryname" dir="rtl" value="{{ $faqcategory->arabiccategoryname }}">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-10">
                <button type="submit" class="btn btn-primary">Save</button>
            </div>

        </div>
    </form>
</div>

@endsection
