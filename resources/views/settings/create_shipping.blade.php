@extends('layouts.admin-app')
@section('content')
<div class="page-header">
    <h1>Add Shipping Country Wies</h1>
</div>

<form action="{{route('store.shipping')}}" enctype="multipart/form-data" method="post" accept-charset="utf-8">
	@csrf
	<div class="row">
    	<div class="col-md-12">
        	<div class="row">
            	<div class="col-md-6">
               		<div class="form-group">
                		<label for="name">Country Name</label>
                		<input type="text" name="name" value="" class="form-control">
              		</div>
            	</div>
	            <div class="col-md-6">
	            	<div class="form-group">
	               		<label for="arabic_name">Country Arabic Name</label>
	               		<input type="text" name="arabic_name" value="" class="form-control arabic-input" lang="ar" dir="rtl">
	            	</div>
	            </div>
        	</div>
			<div class="row">
			    <div class="col-md-6">
			        <div class="form-group">
                        <label>Status</label>
			        	<select name="status" class="form-control">
							<option value="1">Enabled</option>
							<option value="0">Disabled</option>
						</select>
			        </div>
                </div>
                <div class="col-md-3">
	            	<div class="form-group">
	               		<label for="shipping_rate">Shipping Rate</label>
	               		<input type="number" name="shipping_rate" value="" class="form-control">
	            	</div>
	            </div>
    		</div>
        	<br>
        	
		</div>
	</div>
	<div class="row">
	    <div class="col-md-10">
	        <button type="submit" class="btn btn-primary">Save</button>
	    </div>
	    <div class="col-md-2"></div>
	</div>
</form>
@endsection