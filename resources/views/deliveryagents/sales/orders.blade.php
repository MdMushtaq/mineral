@extends('layouts.delivery-app')
@section('content')
<div class="page-header">
	<h1>{{ $headingTitle }}</h1>
</div>

<form action="#" method="post" accept-charset="utf-8">
    <div class="well">
        <div class="row">
            <div class="col-sm-4">
                <div class="form-group">
                    <label class="control-label" for="input-order-id">Order ID</label>
                    <input type="text" name="filter_order_id" value="" placeholder="Order ID" id="input-order-id" class="form-control">
                </div>
                <div class="form-group">
                    <label class="control-label" for="input-customer">Customer</label>
                    <input type="text" name="filter_customer" value="" placeholder="Customer" id="input-customer" class="form-control" autocomplete="off"><ul class="dropdown-menu"></ul>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="form-group">
                    <label class="control-label" for="input-order-status">Order Status</label>
                    <select name="filter_order_status" id="input-order-status" class="form-control">
                        <option value=""></option>
                        <option value="#">Confirmed By Admin</option>
                        <option value="#">Confirmed by Account</option>
                    </select>
                </div>
                <div class="form-group">
                    <label class="control-label" for="input-total">Total</label>
                    <input type="text" name="filter_total" value="" placeholder="Total" id="input-total" class="form-control">
                </div>
            </div>
            <div class="col-sm-4">
                <div class="form-group">
                    <label class="control-label" for="input-date-added">Date Added</label>
                    <div class="input-group date">
                        <input type="text" name="" value="" placeholder="Date Added" data-date-format="YYYY-MM-DD" id="input-date-added" class="form-control datepicker picker__input" readonly="" aria-haspopup="true" aria-expanded="false" aria-readonly="false" aria-owns="input-date-added_root input-date-added_hidden">
                        <input type="hidden" name="filter_date_added" id="input-date-added_hidden">
                            <span class="input-group-btn">
                                <button type="button" class="btn btn-default">
                                    <i class="fa fa-calendar"></i>
                                </button>
                            </span>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label" for="input-date-modified">Date Modified</label>
                    <div class="input-group date">
                        <input type="text" name="" value="" placeholder="Date Modified" data-date-format="YYYY-MM-DD" id="input-date-modified" class="form-control datepicker picker__input" readonly="" aria-haspopup="true" aria-expanded="false" aria-readonly="false" aria-owns="input-date-modified_root input-date-modified_hidden">
                        <input type="hidden" name="filter_date_modified" id="input-date-modified_hidden">
                        <span class="input-group-btn">
                            <button type="button" class="btn btn-default">
                                <i class="fa fa-calendar"></i>
                            </button>
                        </span>
                    </div>
                </div>
                <a class="btn btn-primary pull-right space" href="#">Reset</a>
                <button type="submit" id="button-filter" class="btn btn-primary pull-right">
                    <i class="fa fa-search"></i> Filter
                </button>
            </div>
        </div>
    </div>
    <table class="table table-striped">
        <thead>
            <tr>
                <th><a href="#">Order Date</a></th>
                <th><a href="#">Customer Name</a></th>
                <th><a href="#">Order Status</a> </th>
                <th><a href="#">Payment</a> </th>
                <th><a href="#">Status</a></th>
                <th><a href="#">Agent Approved</a></th>
                <th><a href="#">Total</a></th>
                <th>VAt</th>
                <th>Grant total</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
        
        @foreach($orders as $order)
            <tr>
                <td>{{ $order->created_at->format('d/m/Y') }}</td>
                <td>{{$order->user->name }}</td>
                <td style="max-width:200px;">{{$order->status}}
                    {{-- <select name="order_status" disabled class="form-control" id="order_status">
                        <option value="Processing" {{$order->status == 'Processing' ? 'selected' : ''}}>Processing</option>
                        <option value="Shipped" {{$order->status == 'Shipped' ? 'selected' : ''}}>Shipped</option>
                        <option value="Cancled" {{$order->status == 'Cancled' ? 'selected' : ''}}>Cancled</option>
                        <option value="Pending" {{$order->status == 'Pending' ? 'selected' : ''}}>Pending</option>
                        <option value="Delevered" {{$order->status == 'Delevered' ? 'selected' : ''}}>Delevered</option>
                        <option value="Dispatched" {{$order->status == 'Dispatched' ? 'selected' : ''}}>Dispatched</option>

                    </select> --}}
                </td>
                <td style="max-width:200px;">{{$order->payment}}</td>
                <td style="max-width:200px;">
                    <select name="order_approved_status" disabled class="form-control" id="order_approved_status">
                        <option value="" {{$order->approved_status == '' ? 'selected' : ''}}>Not Approved</option>

                        <option value="Confirm_byAdmin" {{$order->approved_status == 'Confirm_byAdmin' ? 'selected' : ''}}>Confirm By Admin</option>
                        <option value="Confirm_byAjent" {{$order->approved_status == 'Confirm_byAjent' ? 'selected' : ''}}>Confirm By Ajents</option>
                    
                    </select>
                </td>
                <td class="text-right">
                    @if($order->agent_id)
                    <center><span>Approved</span></center>
                    @else
                    <div class="btn-group">
                        <a class="btn btn-default" href="{{route('order.agentapprove',$order->id)}}">Approve
                            <i class="icon-check"></i>
                        </a>
                    </div>
                    @endif
                </td>
                <td>
                    <div class="MainTableNotes total7222">SR {{ $order->vat }}</div>
                </td>

                <td>
                    <div class="MainTableNotes total7222">SR <?= $order->total; ?></div>
                </td>
                <td>
                    <div id="total7222">SR <?= $order->subtotal; ?></div>
                </td>
                <td class="text-right">
                    <div class="btn-group">
                        <a class="btn btn-default" href="{{route('orderitem.show',$order->id)}}">
                                <i class="icon-eye"></i>
                        </a>
                    </div>
                </td>
            </tr>
        @endforeach
    </tbody>
</table>

</form>
@endsection