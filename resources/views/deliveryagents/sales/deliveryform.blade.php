@extends('layouts.admin-app')
@section('content')

 @if(count($errors))
 	<div class="alert alert-danger">
        <strong>Whoops!</strong> There were some problems with your input.
        <br/>
        <ul>
            @foreach($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif 

    <div class="container">

        <div class="page-header"><h1>Delivery Form</h1></div>
        <form action="{{ route('delivery.create') }}" method="post" accept-charset="utf-8">
            @csrf
            <div class="row">
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-6">

                            <div class="form-group">
                                <label for="name">Name</label>
                                <input type="text" name="name" class="form-control" required>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="name">Mobile Number</label>
                                <input type="text" name="mobile" required class="form-control" >
                               
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <label>Verification Code</label>
                            <div class="form-group">
                                <input type="text" name="verification_code" required class="form-control" >
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-10">
                    <button type="submit" class="btn btn-primary">Save</button>
                </div>
               
            </div>


        </form>
    </div>

@endsection
