@extends('layouts.admin-app')
@section('content')

    <style type="text/css">
        .option-form
        {
            display:none;
            margin-top:10px;
        }
        .optionValuesForm
        {
            background-color:#fff;
            padding:6px 3px 6px 6px;
            -webkit-border-radius: 3px;
            -moz-border-radius: 3px;
            border-radius: 3px;
            margin-bottom:5px;
            border:1px solid #ddd;
        }

        .optionValuesForm input
        {
            margin: 0px;
        }
        .optionValuesForm a
        {
            margin-top: 3px;
        }
    </style>


    <div class="page-header">
        <h1>Product Edit Form</h1>
    </div>


@if ($message = Session::get('error'))
<div class="alert alert-danger">
    <strong>Error!</strong> {{ $message }}
</div>
@endif

{{-- @if(count($errors))
<div class="alert alert-danger">
    <strong>Whoops!</strong> There were some problems with your input.
    <br/>
    <ul>
        @foreach($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
    </ul>
</div>
@endif --}}
@if(\Session::has('img-error'))
<div class="alert alert-danger">
    <p>{{ \Session::get('img-error') }}</p>
</div>
<br />
@endif

<br>

    <form action="{{ url('admin/products/editpost',$product->id) }}" enctype='multipart/form-data' method="post" accept-charset="utf-8">
        {{ csrf_field() }}
        <input type="hidden" name="product_id" value="<?= $product->id; ?>">
        <div class="row">
            <div class="col-md-9">
                <div class="tabbable">
                    <ul class="nav nav-tabs">
                        <li class="active">
                            <a href="#product_info" data-toggle="tab" aria-expanded="true">PRODUCT</a>
                        </li>
                        <li class="">
                            <a href="#product_categories" data-toggle="tab" aria-expanded="false">Categories</a>
                        </li>
                    </ul>
                </div>
                <div class="tab-content">
                    <div class="tab-pane active" id="product_info">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Name</label>
                                    <input type="text" name="name" value="<?= $product->prod_name; ?>" placeholder="Name" id="name" class="form-control" maxlength="60">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Arabic Name</label>
                                    <input type="text" name="arabic_name" value="<?= $product->arabic_name; ?>" placeholder="اسم" id="arabic_name" class="form-control arabic-input" lang="ar" dir="rtl" maxlength="60">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>DESCRIPTION </label>
                                    <textarea name="description" cols="40" rows="10" class="form-control" id="description"><?= $product->description; ?></textarea>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Arabic Description</label>
                                    <textarea name="arabic_description" cols="40" rows="10" class="form-control arabic-input" lang="ar" dir="rtl" id="arabic_description"><?= $product->arabic_description; ?></textarea>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>MODEL</label>
                                    <input type="text" name="model" value="<?= $product->model; ?>" placeholder="Enter Model" class="form-control">
                                </div>
                            </div>

                        </div>
                        <fieldset>
                            <legend>Add Images</legend>
                            <form></form>
                            
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-6">
                                        <label><strong>Image</strong></label>
                                        <div class="input-group">
                                            <input type="file" name="product_img[]" class="form-control show-for-sr" id="upload_imgs" accept=".jpg, .jpg, .png">
                                            <br>
                                            <div class="quote-imgs-thumbs quote-imgs-thumbs--hidden" id="img_preview" style="height: 50%;" aria-live="polite"></div>
                                            
                                            @if($product->image != '')
                                                @foreach (json_decode($product->image) as $key => $products)
                                                <div>
                                                    <div style="width: 25%; float: left;">
                                                        <input type="radio" name="main_image" value="{{$key+1}}" {{$key== 0 ? 'checked' : ''}}> Main image
                                                    </div>
                                                    <img style="width: 65%; float: left;" src="{{asset('public/images/products/'.$products)}}">
                                                    @if (count(json_decode($product->image)) > 1)
                                                        <form action="{{route('img.delete',$product->id)}}" method="post">
                                                            @csrf
                                                            <input type="hidden" name="remove_img" value="{{$key}}">
                                                            <button type="submit" style="width: 10%;float:left;" class="btn btn-danger"><i class="icon-times"></i></button>
                                                        </form>
                                                    @endif
                                                </div>                                           
                                                @endforeach
                                            
                                            @endif
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <label><strong>Arabic Image</strong></label>
                                        <div class="input-group">
                                            <input type="file" name="arb_product_img[]" class="form-control show-for-sr" id="arb_upload_imgs" accept=".jpg, .jpg, .png">
                                            <br>
                                            <div class="quote-imgs-thumbs arb-quote-imgs-thumbs--hidden" id="arb_img_preview" style="height: 50%;"  aria-live="polite"></div>
                                           
                                            @if($product->arabic_image != '')
                                             
                                                @foreach (json_decode($product->arabic_image) as $arb_products)
                                                    <img src="{{asset('public/images/products/'.$arb_products)}}" style="margin-bottom: 10px;margin-right: 5px;" width="100" height="100" alt="">
                                                
                                                    @endforeach 
                                            
                                            @endif
                                        </div>
                                        
                                    </div>
                                </div>
                                <hr>
<!--                                <div class="row">-->
<!--                                    <div class="col-md-12">-->
<!--                                        <label><strong>Upload Video Link</strong></label>-->
<!--                                        <input type="text" name="videolink" value="{{$product->videolink}}" class="form-control">-->
<!--                                    </div>-->
<!--                                </div>-->
                            </div>
                        </fieldset>



                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Specification </label>
                                    <textarea name="specification" cols="40" rows="10" class="form-control" id="specification"><?= $product->specification; ?></textarea>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Arabic Specification</label>
                                    <textarea name="arabic_specification" cols="40" rows="10" class="form-control arabic-input" lang="ar" dir="rtl" id="arabic_specification"><?= $product->arabic_specification; ?></textarea>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Additional Information </label>
                                    <textarea name="additionalinformation" class="form-control" id="additionalinformation">{{ $product->additionalinformation }}</textarea>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Arabic Additional Information</label>
                                    <textarea name="arabic_additionalinformation" class="form-control arabic-input" lang="ar" dir="rtl" id="arabic_additionalinformation">{{ $product->arabic_additionalinformation }}</textarea>
                                </div>
                            </div>
                        </div>
    
                        <fieldset>
                            <legend>SEO Information</legend>
                            <div style="padding-top:10px;">

                                <div class="form-group">
                                    <label for="slug">URL Keyword </label>
                                    <input type="text" name="slug" value="<?= $product->url_keyword; ?>" class="form-control">
                                </div>

                                <div class="form-group">
                                    <label for="seo_title">Title Tag </label>
                                    <input type="text" name="seo_title" value="<?= $product->title_tag; ?>" class="form-control">
                                </div>

                                <div class="form-group">
                                    <label for="meta">Meta Tags</label>
                                    <textarea name="meta" cols="40" rows="10" class="form-control"><?= $product->meta_tag; ?></textarea>
                                    <span class="help-block">
                                        ex. &lt;meta name="description" content="We sell products that help you" /&gt;
                                    </span>
                                </div>
                            </div>
                        </fieldset>
                    </div>

                    <div class="tab-pane" id="product_categories">
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-6">
                                    <label>Select MainCategory</label>
                                    <select class="form-control maincategory" name="maincategory_id" id="maincategory" >
                                        <option value="">SELECT</option>
                                        @foreach ($maincategory as $maincategortitem)
                                        <option value="{{$maincategortitem->id}}" {{ $product->maincategory_id == $maincategortitem->id ? 'selected' : ''}}>{{$maincategortitem->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-6">
                                    <label>Select Category</label>
                                    <select class="form-control primary_category" name="primary_category" id="primary_category" required>
                                        @foreach($categories as $category)
                                        {
                    
                                        <option value="{{$category->id}}" {{$product->categories_id == $category->id ? 'selected' : ''}}>{{$category->cat_name}}</option>
                    
                                        }
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-md-6">
                                    <label>Select Sub Category</label>
                                    <select class="form-control secondary_category" name="secondary_category" id="secondary_category" required>
                                        <?php
                                        $subcategories = App\subcategories::where('categories_id',$product->categories_id)->get();
                                        foreach($subcategories as $subcategory)
                                        {
                                            ?>
                                            <option value="<?= $subcategory->id; ?>" <?php if($subcategory->id == $product->sub_category_id) { echo "selected"; } ?>><?php echo $subcategory->sub_cat_name; ?></option>
                                        <?php
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <br>
                        </div>
                    </div>
                    <div class="tab-pane" id="product_related"></div>
                    <div class="tab-pane" id="product_photos"></div>
                </div>
                <div class="row">
                    <div class="col-md-10">
                        <button type="submit" name="submit" id="submitButton" class="btn btn-primary">Save</button>
                    </div>
                    <div class="col-md-2"></div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <label for="language">Shipping </label>
                    <select name="shippable" class="form-control">
                        <?php
                        if($product->shipping == 1)
                        {
                        ?>
                        <option value="<?= $product->shipping; ?>" selected>Shippable</option>
                        <?php
                        }
                        else if($product->shipping == 0)
                        {
                        ?>
                        <option value="<?= $product->shipping; ?>" selected>Not Shippable</option>
                        <?php
                        }
                        ?>
                        <option value="1">Shippable</option>
                        <option value="0">Not Shippable</option>
                    </select>
                </div>
                <div class="form-group">
                    <label>Brands</label>
                    <select class="form-control" name="brand" id="brand">

                         
                         
                        @if($product->brands == '')

                            <option value="">Select</option>
                        
                        @endif
                       
                        @foreach ($brandshow as $editbrands)

                        <option value="{{$editbrands->id}}" {{$editbrands->id == $product->brands ? 'selected' : ''}}>{{$editbrands->brands_name}}</option>

                        @endforeach
                        
                    </select>
                </div>
                <div class="form-group" style="display: none;">
                    <label for="language">Tax </label>
                    <select name="taxable" class="form-control">
                        <option value="1">Taxable</option>
                    </select>
                </div>

                <div class="form-group">
                    <label for="sku">SKU</label>
                    <input type="text" name="sku" value="<?= $product->sku; ?>" id="sku" class="form-control">
                </div>

                <div class="form-group">
                    <label for="weight">Weight </label>
                    <input type="text" name="weight" value="<?= $product->weight; ?>" id="weight" class="form-control">
                </div>
                <fieldset>
                    <legend>
                        {{-- Retail --}}
                        <div class="checkbox " style="font-size:16px; margin-top:5px;">
                            Retail
                            <label>
                                <input type="radio" name="status" value="1" <?php if($product->status == 1){ echo 'checked'; }  ?>>
                                Enabled
                            </label>
                            <label class="pull-right">
                                <input type="radio" name="status" value="0" <?php if($product->status == 0){ echo 'checked'; }  ?>>
                                Disabled
                            </label>
                            
                        </div>
                    </legend>
                    
                   
                    <div class="row">
                        <div class="col-md-6">
                            <label for="price">Sale Price</label>
                            <input type="text" name="price_1" value="<?= $product->price; ?>" id="price_1" class="form-control">
                        </div>
                        <div class="col-md-6">
                            <label for="saleprice">Cost Price</label>
                            <input type="text" name="saleprice_1" value="<?= $product->sale_price; ?>" id="saleprice_1" class="form-control">
                        </div>
                    </div>
                </fieldset>


                <div class="row">
                    <div class="col-md-12">
                        <label for="create_date">Created date</label>
                        <input type="text" name="create_date" value="<?= $product->created_at; ?>" id="create_date" class="form_datetime form-control start_date">
                    </div>
                   
                </div>
                <br>

                <div class="row">
                    <div class="col-md-12">
                        <label >
                            <input type="checkbox" name="newarrival"  {{ $product->newarrival == 'on' ? 'checked' : ''}}>
                            New Arrival
                        </label>
                        <br>
                        <label >
                            <input type="checkbox" name="hotproducts" {{ $product->hotproducts == 'on' ? 'checked' : ''}}>
                            Hot Products
                        </label>
                        <br>
                        <label >
                            <input type="checkbox" name="featuredproducts"  {{ $product->featuredproducts == 'on' ? 'checked' : ''}} >
                            Featured Products
                        </label>
                    </div>
                </div>
            </div>
        </div>
    </form>

    <script>
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $(document).ready(function(){
            $("#primary_category").change(function(){
                var $catId = this.value;
                $.ajax({
                    type: "POST",
                    url: "{{ url('admin/getsubcategories') }}",
                    data: {categoryid:$catId},
                    success: function(result)
                    {
                        $('#secondary_category').html(result);
                    }
                });
    
            });
        });
    </script>
    

<script>

      var imgUpload = document.getElementById('upload_imgs')
      , imgPreview = document.getElementById('img_preview')
      , totalFiles
      , previewTitle
      , previewTitleText
      , img;
    
    imgUpload.addEventListener('change', previewImgs, false);
    
    function previewImgs(event) {
      totalFiles = imgUpload.files.length;
      
      let str = ''
      if(!!totalFiles) {
          str += '<p style="font-weight: bold;">'+totalFiles+' Total Images Selected</p>';
      }
      let count = {{count(json_decode($product->image))}}
      for(var i = 0; i < totalFiles; i++) {
        let imageUrl = URL.createObjectURL(event.target.files[i]);
        str += '<div><div style="width: 25%; float: left;"><input type="radio" name="main_image" value="'+(count+1)+'"> Main image</div><img style="width: 75%; float: left;" src="'+imageUrl+'" class="img-preview-thumb"></div>';
        count++;
       }
      $(imgPreview).html(str);
    }

    </script>
<script>
    var room = 1;
    var count = 1;
    function education_fields() {

        room++;

        var objTo = document.getElementById('education_fields')

        var lastproductcolour = document.getElementById('productcolour'+count).value;
        var lastquantity = document.getElementById('quantity'+count).value;
        var lastsku = document.getElementById('productsku'+count).value;

        if(lastproductcolour && lastquantity && lastsku)
        {
            count++;

            var divtest = document.createElement("div");
            divtest.setAttribute("class", "form-group removeclass"+room);
            var rdiv = 'removeclass'+room;
            divtest.innerHTML = '<div class="col-sm-3 nopadding">' +
                '<div class="form-group"> ' +
                '<input type="hidden" name="productattributeidd[]" value=""><input type="text" class="form-control" id="productcolour'+count+'" name="productcolour[]" value="" placeholder="Product Colour" required>' +
                '</div>' +
                '</div>' +
                '<div class="col-sm-3 nopadding">' +
                '<div class="form-group"> ' +
                '<input type="text" class="form-control" id="quantity'+count+'" name="quantity[]" value="" placeholder="Quantity" required>' +
                '</div>' +
                '</div>' +
                '<div class="col-sm-3 nopadding">'+
                '<div class="form-group">'+
                '<input type="text" class="form-control" id="productsku'+count+'" name="productsku[]" value="" placeholder="SKU" >'+
                '</div>'+
                '</div>'+
                '<div class="col-sm-3 nopadding">' +
                '<div class="form-group">' +
                '<div class="input-group"> ' +
                '<div class="input-group-btn"> ' +
                '<button class="btn btn-danger" type="button" onclick="remove_education_fields('+ room +');"> <span class="glyphicon glyphicon-minus" aria-hidden="true"></span> </button></div></div></div></div><div class="clear"></div>';

            objTo.appendChild(divtest)
        }
        else
        {
            alert('Please fill field to add more');
        }




    }
    function remove_education_fields(rid) {
        $('.removeclass'+rid).remove();
        --count;
    }
</script>


<script>

    var arbimgUpload = document.getElementById('arb_upload_imgs')
      , arbimgPreview = document.getElementById('arb_img_preview')
    //   , arbimgUploadForm = document.getElementById('img-upload-form')
      , totalFiles
      , previewTitle
      , previewTitleText
      , img;
    
    arbimgUpload.addEventListener('change', previewImgs, false);
    
    function previewImgs(event) {
      totalFiles = arbimgUpload.files.length;
      
      if(!!totalFiles) {
        arbimgPreview.classList.remove('arb-quote-imgs-thumbs--hidden');
        previewTitle = document.createElement('p');
        previewTitle.style.fontWeight = 'bold';
        previewTitleText = document.createTextNode(totalFiles + ' Total Images Selected');
        previewTitle.appendChild(previewTitleText);
        arbimgPreview.appendChild(previewTitle);
      }
      
      for(var i = 0; i < totalFiles; i++) {
        img = document.createElement('img');
        img.src = URL.createObjectURL(event.target.files[i]);
        img.classList.add('img-preview-thumb');
        arbimgPreview.appendChild(img);
      }
    }
</script>

@endsection


