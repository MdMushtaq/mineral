@extends('layouts.admin-app')
@section('content')

 @if(count($errors))
 	<div class="alert alert-danger">
        <strong>Whoops!</strong> There were some problems with your input.
        <br/>
        <ul>
            @foreach($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif 

    <div class="container">

        <div class="page-header"><h1>Category Form</h1></div><script type="text/javascript">
            function areyousure()
            {
                return confirm('Are you sure you want to delete this category?');
            }
        </script>
        <form action="{{ route('add.categories') }}" enctype="multipart/form-data" method="post" accept-charset="utf-8">
            @csrf
            
            <div class="row">
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-6">

                            <div class="form-group">
                                <label for="name">Category Name</label>
                                <input type="text" name="name" value="" class="form-control" required>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="name">Category Arabic Name</label>
                                <input type="text" name="arabic_name" value="" required class="form-control" lang="ar" dir="rtl">
                                <!--             -->
                            </div>
                        </div>


                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="image">Image </label>
                                <div class="input-append">
                                    <input type="file" name="cat_image[]" multiple class="form-control">
                                </div>

                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="image">Arabic Image </label>
                                <div class="input-append">
                                    <input type="file" name="cat_arabic_image[]" multiple class="form-control">
                                </div>

                            </div>
                        </div>

                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Main Categories</label>
                                <select class="form-control" name="main_categories" required>
                                    <option value="">--Select Main Category--</option>

                                    @foreach ($maincategory as $mainitem)
                                    <option value="{{$mainitem->id}}">{{$mainitem->name}}</option>
                                    @endforeach

                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">

                            <label>Status</label>
                            <div class="form-group">
                                <select name="status" class="form-control">
                                    <option value="1">Enabled</option>
                                    <option value="0">Disabled</option>
                                </select>
                            </div>

                        </div>

                    </div>

                    <br>

                    <fieldset>
                        <legend>SEO INFORMATION</legend>
                        <div class="row">


                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="seo_title">SEO Title </label>
                                    <input type="text" name="seo_title" value="" class="form-control">
                                </div>
                            </div>



                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Meta Data</label>
                                    <textarea name="meta" cols="40" rows="3" class="form-control"></textarea>
                                    <span class="help-block">ex. &lt;meta name="description" content="We sell products that help you" /&gt;</span>
                                </div>
                            </div>
                        </div>
                        <div class="row">

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="slug">Slug </label>
                                    <input type="text" name="slug" value="" class="form-control">
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="sequence">Sequence </label>
                                    <input type="text" name="sequence" value="" class="form-control">
                                </div>

                            </div>

                        </div>
                    </fieldset>

                </div>
            

                <!-- </div> -->
            </div>

            <div class="row">
                <div class="col-md-10">
                    <button type="submit" class="btn btn-primary">Save</button>
                </div>
                <div class="col-md-2">

                </div>
            </div>


        </form>




        <script type="text/javascript">
            $('form').submit(function() {
                $('.btn .btn-primary').attr('disabled', true).addClass('disabled');
            });
        </script>    <hr>
        <footer></footer>
    </div>

@endsection
