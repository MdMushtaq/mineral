@extends('layouts.admin-app')
@section('content')

<div class="container">

    <!--          <h1>Good</h1>-->
    <!--          <h1>Yes</h1>-->


    <div class="page-header"><h1>Sub Category Form</h1></div>
     
    <form action="{{ url('/admin/subcategories/formeditpost') }}" enctype="multipart/form-data" method="post" accept-charset="utf-8">
        {{ csrf_field() }}
        <input type="hidden" name="subcategoryid" value="<?= $subcategory->id; ?>">
        <div class="row">
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="name">
                                Subcategory Name                    </label>
                            <input type="text" name="name" value="<?= $subcategory->sub_cat_name; ?>" class="form-control">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="name">
                                Subcategory Arabic Name                    </label>
                            <input type="text" name="arabic_name" value="<?= $subcategory->sub_cat_arabic_name; ?>" class="form-control" lang="ar" dir="rtl">
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Main Categories</label>
                            <select class="form-control" name="main_categories" required id="maincategory">
                                <option value="">--Select Main Category--</option>
                                @foreach ($maincategory as $mainitem)
                                <option value="{{$mainitem->id}}" {{ $mainitem->id == $subcategory->maincategory_id ? 'selected' : ''}}>{{$mainitem->name}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-md-4">

                        <div class="form-group">
                            <label>
                                Categories                        </label>
                            <select class="form-control" name="categories_id" required id="primary_category">
                                @foreach ($categories as $catogriesitem)
                                <option value="{{$catogriesitem->id}}" {{$catogriesitem->id == $subcategory->categories_id ? 'selected' : ''}}>{{ $catogriesitem->cat_name}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="sequence">
                                Sequence                            </label>
                            <input type="text" name="sequence" value="<?= $subcategory->sequence; ?>" class="form-control">
                        </div>
                    </div>
                </div>

                <br>
                <fieldset>
                    <legend>
                        SEO INFORMATION                </legend>
                    <div class="row">

                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="slug">
                                    Slug                            </label>
                                <input type="text" name="slug" value="<?= $subcategory->slug; ?>" class="form-control">
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label>
                                    Meta Data                            </label>
                                <textarea name="meta" cols="40" rows="3" class="form-control"><?= $subcategory->meta_data; ?></textarea>
                                    <span class="help-block">
                                ex. &lt;meta name="description" content="We sell products that help you" /&gt;
                                    </span>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="seo_title">
                                    SEO Title                            </label>
                                <input type="text" name="seo_title" value="<?= $subcategory->seo_title; ?>" class="form-control">
                            </div>
                        </div>

                        <div class="col-md-6">
                            <label>
                                Status                        </label>
                            <div class="form-group">
                                <select name="enabled_1" class="form-control">
                                    <option value="1" {{$subcategory->status == 1 ? 'selected' : ''}}>Enabled</option>
                                    <option value="0" {{$subcategory->status == 0 ? 'selected' : ''}}>Disabled</option>
                                </select>
                            </div>
                        </div>
                    </div></fieldset>
            </div>
      
        </div>
        <div class="row">
            <div class="col-md-10">
                <button type="submit" class="btn btn-primary">
                    Save        </button>
            </div>
            <div class="col-md-2">
            </div>
        </div>


    </form>
    <script type="text/javascript">
        $('form').submit(function() {
            $('.btn .btn-primary').attr('disabled', true).addClass('disabled');
        });
    </script>    <hr>
    <footer></footer>
</div>

@endsection
