@extends('layouts.admin-app')
@section('content')

@foreach ($errors->all() as $error)
<li>{{ $error }}</li>
@endforeach

    <div class="container">

        <div class="page-header"><h1>Category Form</h1></div><script type="text/javascript">
            function areyousure()
            {
                return confirm('Are you sure you want to delete this category?');
            }
        </script>
        <form action="{{route('update.categories',$categories->id)}}" enctype="multipart/form-data" method="post" accept-charset="utf-8">
            @csrf
            
            <div class="row">
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-6">

                            <div class="form-group">
                                <label for="name">Category Name</label>
                            <input type="text" name="name" value="{{$categories->cat_name}}" class="form-control">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="name">Category Arabic Name</label>
                                <input type="text" name="arabic_name" value="{{$categories->cat_arabic_name}}" class="form-control" lang="ar" dir="rtl">
                                <!--             -->
                            </div>
                        </div>


                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group" {{ $errors->has('cat_image') ? 'has-error' : '' }}>
                                <label for="image">Image </label>
                                <div class="input-append">
                                    <input type="file" name="cat_image[]" multiple class="form-control">

                                    @if($categories->cat_img != '')

                                        @foreach (json_decode($categories->cat_img) as  $category)

                                            <img name="up_cat_img"  src="{{asset('public/images/categories/'.$category)}}" width="600" height="200" alt="">

                                        @endforeach

                                    @endif
                                    <span class="text-danger">{{ $errors->first('cat_image') }}</span>
                                </div>

                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="image"> </label>
                                <div class="input-append">
                                    <input type="file" name="cat_arabic_image[]" multiple class="form-control">

                                    @if($categories->cat_arabic_img != '')

                                        @foreach (json_decode($categories->cat_arabic_img) as  $categoryarb)

                                            <img name="up_cat_img"  src="{{asset('public/images/categories/'.$categoryarb)}}" width="600" height="200" alt="">

                                        @endforeach 

                                    @endif

                                </div>

                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Main Categories</label>
                                <select class="form-control" name="main_categories" required>
                                    <option value="">--Select Main Category--</option>
                                    @foreach ($maincategory as $mainitem)
                                    <option value="{{$mainitem->id}}" {{ $mainitem->id == $categories->miancategory_id ? 'selected' : ''}}>{{$mainitem->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">

                            <label>Status</label>
                            <div class="form-group">
                                <select name="status" class="form-control">
                                    <option value="1" {{$categories->cat_status == '1' ? 'selected' : ''}} >Enabled</option>
                                    <option value="0" {{$categories->cat_status == '0' ? 'selected' : ''}}>Disabled</option>
                                </select>
                            </div>

                        </div>

                    </div>

                    <br>

                    <fieldset>
                        <legend>SEO INFORMATION</legend>
                        <div class="row">


                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="seo_title">SEO Title </label>
                                    <input type="text" name="seo_title" value="{{$categories->cat_seo_title}}" class="form-control">
                                </div>
                            </div>



                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Meta Data</label>
                                    <textarea name="meta" cols="40" rows="3" class="form-control">{{$categories->cat_meta_data}}</textarea>
                                    <span class="help-block">ex. &lt;meta name="description" content="We sell products that help you" /&gt;</span>
                                </div>
                            </div>
                        </div>
                        <div class="row">

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="slug">Slug </label>
                                    <input type="text" name="slug" value="{{$categories->cat_slug }}" class="form-control">
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="sequence">Sequence </label>
                                    <input type="text" name="sequence" value="{{$categories->cat_sequence}}" class="form-control">
                                </div>

                            </div>

                        </div>
                    </fieldset>

                </div>
            

                <!-- </div> -->
            </div>

            <div class="row">
                <div class="col-md-10">
                    <button type="submit" class="btn btn-primary">Save</button>
                </div>
                <div class="col-md-2">

                </div>
            </div>


        </form>




        <script type="text/javascript">
            $('form').submit(function() {
                $('.btn .btn-primary').attr('disabled', true).addClass('disabled');
            });
        </script>    <hr>
        <footer></footer>
    </div>

@endsection
