@extends('layouts.admin-app')
@section('content')

@if (\Session::has('success'))
<div class="alert alert-success">
   <p>{{ \Session::get('success') }}</p>
</div>
<br />

@elseif(\Session::has('danger'))
<div class="alert alert-danger">
   <p>{{ \Session::get('danger') }}</p>
</div>
<br />
@endif 

    <div class="container">
      
        <div class="page-header"><h1>Sub Category</h1></div>
        <div class="row">
            <div class="col-md-12">
                <div class="row">

                    <div class="col-md-4"></div>
                   

                    <div class="col-md-8">
                        <form action="#" class="form-inline form-group" style="float:right" method="post" accept-charset="utf-8">
                            <div class="form-group">

                            </div>
                            <div class="form-group">
                                <input type="text"  id="myInput" onkeyup="myFunction()" class="form-control" name="term" value="" placeholder="">
                            </div>
<!--                            <button class="btn btn-default" name="submit" value="search">Search</button>-->
                            <a class="btn btn-default" href="#">Reset</a>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <div class="text-right form-group">
            <a class="btn btn-primary" style="font-weight:normal;" href="{{ url('admin/subcategories/form') }}">
                <i class="icon-plus"></i> Add New Sub Category
            </a>

            <a class="btn btn-danger" style="font-weight:normal;" href="{{ route('trash.subcategories') }}">
                <i class="icon-times"></i> Trash Sub Category
            </a>

        </div>
        <table class="table table-striped" id="myTable">
            <thead>
            <tr>
                <th><a href="#">MainCategory Name</a></th>
                <th><a href="#">MainCategory Arabic Name</a></th>
                <th><a href="#">Category Name <i class="icon-sort-alt-down"></i></a></th>
                <th><a href="#">Category Arabic Name</a></th>
                <th><a href="#">Subcategory Name</a></th>
                <th><a href="#">Subcategory Arabic Name</a></th>
                <th style="width:16%">

                </th>
            </tr>
            </thead>

            <tbody>

            
            @foreach($subcategories as $subcat)
                @php $maincategory = App\MainCategory::where('id',$subcat->maincategory_id)->first(); @endphp
                @php $catgetname = App\Categories::where('id',$subcat->categories_id)->first(); @endphp
            <tr>
               
                <td>{{$maincategory->name}}</td>
                <td>{{$maincategory->arabic_name}}</td>
                <td>{{$catgetname->cat_name}}</td>
                <td>{{$catgetname->cat_arabic_name}}</td>
                <td>{{$subcat->sub_cat_name}}</td>
                <td>{{$subcat->sub_cat_arabic_name}}</td>
               
                <td class="text-right">
                    <div class="btn-group">
                        <a class="btn btn-default" href="{{ url('admin/subcategories/edit',$subcat->id) }}" alt="Edit"><i class="icon-pencil"></i></a>
                       
                        @if( $subcat->status == '0')
                        <a class="btn btn-danger" href="{{ route('subcategories.delete',$subcat->id) }}" onclick="return confirm('are you sure?')" alt="delete"><i class="icon-times"></i></a>
                        @endif
                    </div>
                </td>
            </tr>
            @endforeach
            </tbody>
        </table>

    </div>
<script>
    function myFunction() {
        var input, filter, table, tr, td, i, txtValue;
        input = document.getElementById("myInput");
        filter = input.value.toUpperCase();
        table = document.getElementById("myTable");
        tr = table.getElementsByTagName("tr");
        for (i = 0; i < tr.length; i++) {
            td = tr[i].getElementsByTagName("td")[2];
            if (td) {
                txtValue = td.textContent || td.innerText;
                if (txtValue.toUpperCase().indexOf(filter) > -1) {
                    tr[i].style.display = "";
                } else {
                    tr[i].style.display = "none";
                }
            }
        }
    }
</script>
@endsection
