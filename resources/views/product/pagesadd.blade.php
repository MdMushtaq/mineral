@extends('layouts.admin-app')
@section('content')

<div class="container">
    <script type="text/javascript">
        function areyousure()
        {
            return confirm('Are you sure you want to delete this category?');
        }
    </script>
    <style type="text/css">
        .pagination {
            margin:0px;
        }
    </style>
    <div class="page-header">
        <h1>Add New Page</h1>
    </div>
    <form action="{{ url('admin/pages/addpost') }}" method="post" accept-charset="utf-8">
        {{ csrf_field() }}
        <div class="row">
            <div class="col-md-3">
                <div class="form-group">
                    <label for="title">Title</label>
                    <input type="text" name="title" value="" class="form-control">
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <label for="Arabic title">Arabic Title</label>
                    <input type="text" name="arabic_title" value="" class="form-control">
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <label for="menu_title">Menu Title</label>
                    <input type="text" name="menu_title" value="" class="form-control">
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <label for="arabic_menu_title">Arabic Menu Title</label>
                    <input type="text" name="arabic_menu_title" value="" class="form-control">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <div class="form-group">
                        <label for="content">Content</label>
                        <div class="redactor-box"><ul class="redactor-toolbar toolbar-fixed-box" id="redactor-toolbar-0" style="position: absolute; width: 555px; top: 219px; left: 0px; visibility: hidden;"><li><a href="#" class="re-icon re-html" rel="html" tabindex="-1"></a></li><li><a href="#" class="re-icon re-formatting" rel="formatting" tabindex="-1"></a></li><li><a href="#" class="re-icon re-bold" rel="bold" tabindex="-1"></a></li><li><a href="#" class="re-icon re-italic" rel="italic" tabindex="-1"></a></li><li><a href="#" class="re-icon re-deleted" rel="deleted" tabindex="-1"></a></li><li><a href="#" class="re-icon re-unorderedlist" rel="unorderedlist" tabindex="-1"></a></li><li><a href="#" class="re-icon re-orderedlist" rel="orderedlist" tabindex="-1"></a></li><li><a href="#" class="re-icon re-outdent" rel="outdent" tabindex="-1"></a></li><li><a href="#" class="re-icon re-indent" rel="indent" tabindex="-1"></a></li><li><a href="#" class="re-icon re-link" rel="link" tabindex="-1"></a></li><li><a href="#" class="re-icon re-alignment" rel="alignment" tabindex="-1"></a></li><li><a href="#" class="re-icon re-horizontalrule" rel="horizontalrule" tabindex="-1"></a></li></ul><div class="redactor-editor redactor-linebreaks" contenteditable="true" dir="ltr" style="min-height: 200px;"></div><textarea name="content" cols="40" rows="10" class="form-control redactor" dir="ltr" style="display: none;"></textarea></div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <div class="form-group">
                        <label for="arabic_content">Arabic Content</label>
                        <div class="redactor-box"><ul class="redactor-toolbar toolbar-fixed-box" id="redactor-toolbar-1" style="position: absolute; width: 555px; top: 219px; left: 0px; visibility: hidden;"><li><a href="#" class="re-icon re-html" rel="html" tabindex="-1"></a></li><li><a href="#" class="re-icon re-formatting" rel="formatting" tabindex="-1"></a></li><li><a href="#" class="re-icon re-bold" rel="bold" tabindex="-1"></a></li><li><a href="#" class="re-icon re-italic" rel="italic" tabindex="-1"></a></li><li><a href="#" class="re-icon re-deleted" rel="deleted" tabindex="-1"></a></li><li><a href="#" class="re-icon re-unorderedlist" rel="unorderedlist" tabindex="-1"></a></li><li><a href="#" class="re-icon re-orderedlist" rel="orderedlist" tabindex="-1"></a></li><li><a href="#" class="re-icon re-outdent" rel="outdent" tabindex="-1"></a></li><li><a href="#" class="re-icon re-indent" rel="indent" tabindex="-1"></a></li><li><a href="#" class="re-icon re-link" rel="link" tabindex="-1"></a></li><li><a href="#" class="re-icon re-alignment" rel="alignment" tabindex="-1"></a></li><li><a href="#" class="re-icon re-horizontalrule" rel="horizontalrule" tabindex="-1"></a></li></ul><div class="redactor-editor redactor-linebreaks" contenteditable="true" dir="ltr" style="min-height: 200px;"></div><textarea name="arabic_content" cols="40" rows="10" class="form-control redactor" dir="ltr" style="display: none;"></textarea></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-3">
                <div class="form-group">
                    <label for="seo_title">SEO Title</label>
                    <input type="text" name="seo_title" value="" class="form-control">
                </div>
                <div class="form-group">
                    <label for="slug">Slug</label>
                    <input type="text" name="slug" value="" class="form-control">
                </div>

                <div class="form-group">
                    <select name="parent_id" class="hidden">
                        <option value="-1">Hidden</option>
                        <option value="0" selected="selected">Top Level</option>
                        <option value="6"> CAREERS</option>
                    </select>
                </div>

                <div class="form-group">
                    <label for="sequence">Sequence</label>
                    <input type="text" name="sequence" value="0" class="form-control">
                </div>
            </div>
            <div class="col-md-9">
                <div class="form-group">
                    <label>Meta Data</label>
                    <textarea name="meta" cols="40" rows="3" class="form-control"></textarea>
                    <span id="helpBlock" class="help-block">ex. &lt;meta name="description" content="We sell products that help you" /&gt;</span>
                </div>


            </div>
        </div>
        <div class="form-actions">
            <div class="row">
                <div class="col-md-10">
                    <button type="submit" class="btn btn-primary">Save</button>
                </div>
                <div class="col-md-2"></div>
            </div>
        </div>
    </form>

   
</div>

@endsection
