@extends('layouts.admin-app')
@section('content')

    <div class="container">

        <div class="page-header"><h1>Sub Category Form</h1></div>
        <form action="{{ url('/admin/subcategories/form') }}" enctype="multipart/form-data" method="post" accept-charset="utf-8">
            {{ csrf_field() }}
            <div class="row">
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="name">Subcategory Name</label>
                                <input type="text" name="name" value=""  required class="form-control">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="name">Subcategory Arabic Name</label>
                                <input type="text" name="arabic_name" value="" required class="form-control" lang="ar" dir="rtl">
                            </div>
                        </div>
                    </div>
                   
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Main Categories</label>
                                <select class="form-control" name="maincategory_id" id="maincategory" required>
                                    <option value="">--Select MainCategory--</option>
    
                                    @foreach ($maincategory as $maincatitem)
    
                                    <option value="{{$maincatitem->id}}">{{$maincatitem->name}}</option>
                                    @endforeach
    
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Categories</label>
                                <select class="form-control" id="primary_category" name="categories_id" required>
                                    <option value="">--Select Category--</option>
                                </select>
                            </div>
                        </div>
                    </div>

                  
                    <div class="row">
                        
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="sequence">
                                    Sequence                            </label>
                                <input type="text" name="sequence" value="" class="form-control">
                            </div>
                        </div>
                    </div>
                    <br>
                    <fieldset>
                        <legend>
                            SEO INFORMATION                </legend>
                        <div class="row">

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="slug">
                                        Slug                            </label>
                                    <input type="text" name="slug" value="" class="form-control">
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>
                                        Meta Data                            </label>
                                    <textarea name="meta" cols="40" rows="3" class="form-control"></textarea>
                                    <span class="help-block">
                                ex. &lt;meta name="description" content="We sell products that help you" /&gt;
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="seo_title">
                                        SEO Title                            </label>
                                    <input type="text" name="seo_title" value="" class="form-control">
                                </div>
                            </div>

                            <div class="col-md-6">
                                <label>
                                    Status                        </label>
                                <div class="form-group">
                                    <select name="enabled_1" class="form-control">
                                        <option value="1">Enabled</option>
                                        <option value="0">Disabled</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </fieldset>
                </div>
            </div>
            <div class="row">
                <div class="col-md-10">
                    <button type="submit" class="btn btn-primary">
                        Save        </button>
                </div>
                <div class="col-md-2">
                </div>
            </div>


        </form>
        <script type="text/javascript">
            $('form').submit(function() {
                $('.btn .btn-primary').attr('disabled', true).addClass('disabled');
            });
        </script>
    </div>

    <script>
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $(document).ready(function(){
            $("#maincategory").change(function(){
                var $maincatId = this.value;
                $.ajax({
                    type: "POST",
                    url: "{{ url('admin/getcategories') }}",
                    data: {maincategoryid:$maincatId},
                    success: function(result)
                    {
                        $('#primary_category').html(result);
                    }
                });
    
            });
        });
    </script>
@endsection
