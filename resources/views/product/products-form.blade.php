@extends('layouts.admin-app')

@section('content')

<style type="text/css">
	.option-form {
        display:none;
        margin-top:10px;
  	}
    .optionValuesForm{
    	background-color:#fff;
        padding:6px 3px 6px 6px;
        -webkit-border-radius: 3px;
        -moz-border-radius: 3px;
        border-radius: 3px;
        margin-bottom:5px;
        border:1px solid #ddd;
    }

    .optionValuesForm input {
        margin:0px;
    }
    .optionValuesForm a {
        margin-top:3px;
    }
   
</style>

@if(count($errors))
<div class="alert alert-danger">
   <strong>Whoops!</strong> There were some problems with your input.
   <br/>
   <ul>
       @foreach($errors->all() as $error)
           <li>{{ $error }}</li>
       @endforeach
   </ul>
</div>
@endif

@if (\Session::has('success'))
<div class="alert alert-success">
    <p>{{ \Session::get('success') }}</p>
</div>
<br />

@elseif(\Session::has('danger'))
<div class="alert alert-danger">
    <p>{{ \Session::get('danger') }}</p>
</div>
<br />
@endif 


<div class="page-header">
	<h1>Product Form</h1>
</div>

<br>

<form action="{{ url('admin/products/addformpost') }}" method="post" accept-charset="utf-8" enctype='multipart/form-data'>
    {{ csrf_field() }}
    <div class="row">
        <div class="col-md-9">
            <div class="tabbable">
                <ul class="nav nav-tabs">
                    <li class="active">
                    	<a href="#product_info" data-toggle="tab" aria-expanded="true">PRODUCT</a>
                    </li>
                    <li class="">
                    	<a href="#product_categories" data-toggle="tab" aria-expanded="false">Categories</a>
                    </li>
                </ul>
            </div>
            <div class="tab-content">
                <div class="tab-pane active" id="product_info">
                    <div class="row">
                        <div class="col-md-6">
                        <div class="form-group">
                        <label>Name</label>
                        <input type="text" name="name" value="{{old('name')}}" placeholder="Name" id="name" class="form-control" required maxlength="60">
                        </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Arabic Name</label>
                                <input type="text" name="arabic_name" value="{{old('arabic_name')}}" placeholder="اسم" required id="arabic_name" class="form-control arabic-input" lang="ar" dir="rtl" maxlength="60">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>DESCRIPTION </label>
                                <textarea name="description" class="form-control" id="description">{{old('description')}}</textarea>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Arabic Description</label>
                                <textarea name="arabic_description" class="form-control arabic-input" lang="ar" dir="rtl" id="arabic_description">{{old('arabic_description')}}</textarea>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                      <div class="col-md-12">
                            <div class="form-group">
                                <label>MODEL</label>
                                <input type="text" name="model" value="{{old('model')}}" placeholder="Enter Model" class="form-control">
                            </div>
                        </div>

                    </div>
                    <fieldset>
                        <legend>Add Images</legend>
                        <div class="form-group">
                        	<div class="row">
	                            <div class="col-md-6">
	                                <label><strong>Image</strong></label>
	                                <div class="input-group">
                                        <input type="file" name="product_img[]" required multiple class="form-control show-for-sr" id="upload_imgs" accept=".jpg, .jpg, .png">
                                        <br>
                                        <div class="quote-imgs-thumbs quote-imgs-thumbs--hidden" id="img_preview" style="height: 50%;"  aria-live="polite">
                                            
                                        </div>
								    </div>
                                </div>
                                <div class="col-md-6">
	                                <label><strong>Arabic Image</strong></label>
	                                <div class="input-group">
                                        <input type="file" name="arb_product_img[]" required multiple class="form-control show-for-sr" id="arb_upload_imgs" accept=".jpg, .jpg, .png">
                                        <br>
                                        <div class="quote-imgs-thumbs arb-quote-imgs-thumbs--hidden" id="arb_img_preview" style="height: 50%;"  aria-live="polite"></div>
								    </div>
                                </div>
                            </div>
                            <hr>
<!--                            <div class="row">-->
<!--                                <div class="col-md-12">-->
<!--                                    <label><strong>Upload Video Link</strong></label>-->
<!--                                    <input type="text" name="videolink" value="{{old('videolink')}}" class="form-control">-->
<!--                                </div>-->
<!--                            </div>-->
                        </div>
                    </fieldset>



                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Specification </label>
                                <textarea name="specification" class="form-control" id="specification">{{old('specification')}}</textarea>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Arabic Specification</label>
                                <textarea name="arabic_specification" class="form-control arabic-input" lang="ar" dir="rtl" id="arabic_specification">{{old('arabic_specification')}}</textarea>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Additional Information </label>
                                <textarea name="additionalinformation" class="form-control" id="additionalinformation">{{old('additionalinformation')}}</textarea>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Arabic Additional Information</label>
                                <textarea name="arabic_additionalinformation" class="form-control arabic-input" lang="ar" dir="rtl" id="arabic_additionalinformation">{{old('arabic_additionalinformation')}}</textarea>
                            </div>
                        </div>
                    </div>
                    <fieldset>
                        <legend>SEO Information</legend>
                        <div style="padding-top:10px;">

                            <div class="form-group">
                                <label for="slug">URL Keyword </label>
                                <input type="text" name="slug" value="" class="form-control">
                            </div>

                            <div class="form-group">
                                <label for="seo_title">Title Tag </label>
                                <input type="text" name="seo_title" value="" class="form-control">
                            </div>

                            <div class="form-group">
                                <label for="meta">Meta Tags</label>
                                <textarea name="meta" cols="40" rows="10" class="form-control"></textarea>
                                <span class="help-block">ex. &lt;meta name="description" content="We sell products that help you" /&gt;</span>
                            </div>
                        </div>
                    </fieldset>
                </div>

                <div class="tab-pane" id="product_categories">
                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-6">
                                <label>Select MainCategory</label>
                                <select class="form-control maincategory" name="maincategory_id" id="maincategory" >
                                    <option value="">SELECT</option>

                                    @foreach ($maincategory as $maincategortitem)
                                    <option value="{{$maincategortitem->id}}">{{$maincategortitem->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                       <br>
                	</div>
                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-6">
                                <label>Select Category</label>
                                <select class="form-control primary_category" name="primary_category" id="primary_category" >
                                    <option value="">SELECT</option>

                                </select>
                            </div>
                            <div class="col-md-6">
                                <label>Select Sub Category</label>
                                <select class="form-control secondary_category" name="secondary_category" id="secondary_category" >
                                    <option value="">SELECT</option>

                                </select>
                            </div>
                        </div>
                        <br>
                    </div>
            	</div>
                <div class="tab-pane" id="product_related"></div>
                <div class="tab-pane" id="product_photos"></div>
            </div>
            <div class="row">
                <div class="col-md-10">
                    <button type="submit" name="submit" id="submitButton" class="btn btn-primary">Save</button>
                </div>
                <div class="col-md-2"></div>
            </div>
        </div>
        <div class="col-md-3">
            <div class="form-group">
                <label for="language">Shipping </label>
                <select name="shippable" class="form-control">
					<option value="1">Shippable</option>
					<option value="0">Not Shippable</option>
				</select>
            </div>
            <div class="form-group">
                <label>Brands</label>
                <select class="form-control" name="brand" id="brand">
                    <option value="">Select Brand</option>

                    @foreach ($brands as $getbrands)
                        <option value="{{old($getbrands->id)}}">{{$getbrands->brands_name}}</option>
                    @endforeach
                    
                </select>
           	</div>
            <div class="form-group">
                <label for="sku">SKU</label>
                <input type="text" name="sku" value="" id="sku" class="form-control">
            </div>

            <div class="form-group">
                <label for="weight">Weight </label>
                <input type="text" name="weight" value="" id="weight" class="form-control">
            </div>
			<fieldset>
                <legend>
                   	Retail
                    <div class="checkbox pull-right" style="font-size:16px; margin-top:5px;">
                        <label>
                            <input type="radio" name="status" value="1" checked>
                            Enabled
                        </label>
                        <label class="pull-right">
                            <input type="radio" name="status" value="0">
                            Disabled
                        </label>
                    </div>
                </legend>
                <div class="row">
                    <div class="col-md-6">
                        <label for="price">Sale Price</label>
                        <input type="text" required name="price_1" value="" id="price_1" class="form-control">
                    </div>
                    <div class="col-md-6">
                        <label for="saleprice">Cost Price</label>
                        <input type="text"  name="saleprice_1" value="" id="saleprice_1" class="form-control">
                    </div>
                </div>
            </fieldset>
            <div class="row">
                <div class="col-md-12">
                    <label >
                        <input type="checkbox" name="newarrival" >
                       New Arrival
                    </label>
                    <br>
                    <label >
                        <input type="checkbox" name="hotproducts" >
                        Hot Products
                    </label>
                    <br>
                    <label >
                        <input type="checkbox" name="featuredproducts" >
                        Featured Products
                    </label>
                </div>
            </div>
        </div>
    </div>
</form>
<script>
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $(document).ready(function(){
        $("#primary_category").change(function(){
            var $catId = this.value;

            $.ajax({
                type: "POST",
                url: "{{ url('admin/getsubcategories') }}",
                data: {categoryid:$catId},
                success: function(result)
                {
                    $('#secondary_category').html(result);
                }
            });
        });
    });
</script>

<script>

    var imgUpload = document.getElementById('upload_imgs')
      , imgPreview = document.getElementById('img_preview')
      , totalFiles
      , previewTitle
      , previewTitleText
      , img;
    
    imgUpload.addEventListener('change', previewImgs, false);
    
    function previewImgs(event) {
      totalFiles = imgUpload.files.length;
      console.log(totalFiles);
      
      let str = ''
      if(!!totalFiles) {
          str += '<p style="font-weight: bold;">'+totalFiles+' Total Images Selected</p>';
      }
      
      for(var i = 0; i < totalFiles; i++) {
        let imageUrl = URL.createObjectURL(event.target.files[i]);
        let isChecked = (i == 0) ? 'checked' : '';
        str += '<div><div style="width: 25%; float: left;"><input type="radio" name="main_image" value="'+(i+1)+'" '+isChecked+'> Main image</div><img style="width: 75%; float: left;" src="'+imageUrl+'" class="img-preview-thumb"></div>';
      }
      $(imgPreview).html(str);
    }
</script>


<script>

    var arbimgUpload = document.getElementById('arb_upload_imgs')
      , arbimgPreview = document.getElementById('arb_img_preview')
    //   , arbimgUploadForm = document.getElementById('img-upload-form')
      , totalFiles
      , previewTitle
      , previewTitleText
      , img;
    
    arbimgUpload.addEventListener('change', previewImgs, false);
    
    function previewImgs(event) {
      totalFiles = arbimgUpload.files.length;

      
      
      if(!!totalFiles) {
        arbimgPreview.classList.remove('arb-quote-imgs-thumbs--hidden');
        previewTitle = document.createElement('p');
        previewTitle.style.fontWeight = 'bold';
        previewTitleText = document.createTextNode(totalFiles + ' Total Images Selected');
        previewTitle.appendChild(previewTitleText);
        arbimgPreview.appendChild(previewTitle);
      }
      
      
      for(var i = 0; i < totalFiles; i++) {
        img = document.createElement('img');
        img.src = URL.createObjectURL(event.target.files[i]);
        img.classList.add('img-preview-thumb');
        arbimgPreview.appendChild(img);
      }
    }
</script>

<script>
    var room = 1;
    var count = 1;
    function education_fields() {
        room++;
        var objTo = document.getElementById('education_fields');

        var lastproductcolour = document.getElementById('productcolour'+count).value;
        var lastquantity = document.getElementById('quantity'+count).value;
        var lastsku = document.getElementById('productsku'+count).value;

        if(lastproductcolour && lastquantity && lastsku)
        {
            count++;

            var divtest = document.createElement("div");
            divtest.setAttribute("class", "form-group removeclass"+room);
            var rdiv = 'removeclass'+room;
            divtest.innerHTML = '<div class="col-sm-3 nopadding">' +
                '<div class="form-group"> ' +
                '<input type="text" class="form-control" id="productcolour'+count+'" name="productcolour[]" value="" placeholder="Product Colour" required>' +
                '</div>' +
                '</div>' +
                '<div class="col-sm-3 nopadding">' +
                '<div class="form-group"> ' +
                '<input type="text" class="form-control" id="quantity'+count+'" name="quantity[]" value="" placeholder="Quantity" required>' +
                '</div>' +
                '</div>' +
                '</div>' +
                '<div class="col-sm-3 nopadding">'+
                '<div class="form-group">'+
                '<input type="text" class="form-control" id="productsku'+count+'" name="productsku[]" value="" placeholder="SKU" >'+
                '</div>'+
                '</div>'+
                '<div class="col-sm-3 nopadding">' +
                '<div class="form-group">' +
                '<div class="input-group"> ' +
                '<div class="input-group-btn"> ' +
                '<button class="btn btn-danger" type="button" onclick="remove_education_fields('+ room +');"> <span class="glyphicon glyphicon-minus" aria-hidden="true"></span> </button></div></div></div></div><div class="clear"></div>';

            objTo.appendChild(divtest)

        }
        else
        {
            alert('Please fill field to add more');
        }


    }
    function remove_education_fields(rid)
    {
        $('.removeclass'+rid).remove();
        --count;

    }
</script>

<script>
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $(document).ready(function(){
        $("#maincategory").change(function(){
            var $maincatId = this.value;

            $.ajax({
                type: "POST",
                url: "{{ url('admin/getcategories') }}",
                data: {maincategoryid:$maincatId},
                success: function(result)
                {
                    $('#primary_category').html(result);
                }
            });

        });

        $("#primary_category").change(function(){
            var $catId = this.value;

            $.ajax({
                type: "POST",
                url: "{{ url('admin/getsubcategories') }}",
                data: {categoryid:$catId},
                success: function(result)
                {
                    $('#secondary_category').html(result);
                }
            });

        });
    });
</script>

@endsection


