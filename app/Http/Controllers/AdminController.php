<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\User;
use App\Orders;

class AdminController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $data["customers"] = User::all();
        $data["totalcustomers"] = $data["customers"]->count();
        $orders = Orders::with('orderitems')->where('payment','Success')->orderBy('id', 'DESC')->get();
        return view('admin', $data,compact('orders'));
    }
}
