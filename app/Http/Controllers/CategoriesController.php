<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Categories;
use App\subcategories;
use App\Brands;
use App\products;
use App\banners;
use Session;
use App\Pages;
use Illuminate\Support\Facades\Storage;
use App\Links;
use App\Productattributes;
use App\MainCategory;

class CategoriesController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth:admin');
    }
    public function mainCategory()
    {
        $maincategory = MainCategory::all();
        return view('product.main-category',compact('maincategory'));
    }
    public function mainCategoryForm()
    {
        return view('product.main-categoryForm');
    }

    public function mainCategoryCreate(Request $request)
    {
        $maincategory = new MainCategory;
        $maincategory->name = $request->name;
        $maincategory->arabic_name  = $request->arabic_name;
        $maincategory->status  = $request->status;
        $maincategory->save();

        return redirect('/admin/main-categories')->with('success','Add Successfuly Main Category');
    }

    public function index()
    {
        //
        $categories = Categories::where('cat_status',1)->get();
        return view('product.categories', compact('categories'));
    }

    public function categoriesTrash()
    {
        //
        $categories = Categories::where('cat_status',0)->get();
        return view('product.categories', compact('categories'));
    }
    public function create()
    {
        // $this->authorize('Categories.Create');
        $maincategory = MainCategory::where('status',1)->get();
        return view('product.addcategory',compact('maincategory'));
    }
    public function store(Request $request)
    {
        // $this->authorize('Categories.Store');

        request()->validate([
            'cat_image' => 'required',
            'cat_arabic_image' => 'required'
        ],
        [
            'cat_image.required' => 'Imge should be required!',
            'cat_arabic_image.required' => 'Arabic Imge should be required!'
        ]);
        if($request->hasfile('cat_image'))
        {
            foreach($request->file('cat_image') as $image)
            {
                $name=$image->getClientOriginalName();
                $image->move(public_path().'/images/categories/', $name);  
                $data[] = $name;
            }
          
        }
        if($request->hasfile('cat_arabic_image'))
        {
          
           foreach($request->file('cat_arabic_image') as $image)
            {
                $name=$image->getClientOriginalName();
                $image->move(public_path().'/images/categories/', $name);  
                $data1[] = $name;
            }
          
        }
        $categories = new Categories();
        $categories->cat_name = $request->name;
        $categories->cat_arabic_name = $request->arabic_name;
        $categories->miancategory_id = $request->main_categories;
        $categories->cat_status = $request->status;
        $categories->cat_img = json_encode($data);
        $categories->cat_arabic_img = json_encode($data1);
        $categories->cat_seo_title = $request->seo_title;
        $categories->cat_meta_data = $request->meta;
        $categories->cat_slug = $request->slug;
        $categories->cat_sequence = $request->sequence;
        $categories->save();

        return redirect('/admin/categories')->with('success','Successfully created Categories');
    }
    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $categories = Categories::find($id);
        $maincategory = MainCategory::where('status',1)->get();
        return view('product.update_category', compact('categories','maincategory'));
    }

    public function update(Request $request, $id)
    {       
        $categories = Categories::find($id);
        $categories->cat_name = $request->name;
        $categories->cat_arabic_name = $request->arabic_name;
        $categories->cat_status = $request->status;
        $categories->miancategory_id = $request->main_categories;
        if($request->hasfile('cat_image')){
           
            foreach($request->file('cat_image') as $image)
            {
                $name=$image->getClientOriginalName();
                $image->move(public_path().'/images/categories/', $name);  
                $data[] = $name;  
                $categories->cat_img = json_encode($data);
            }
        }

            if($request->hasfile('cat_arabic_image')){
            
                foreach($request->file('cat_arabic_image') as $image)
                {
                    $name=$image->getClientOriginalName();
                    $image->move(public_path().'/images/categories/', $name);  
                    $data1[] = $name;  
                    $categories->cat_arabic_img = json_encode($data1);
                }
            }

        $categories->cat_seo_title = $request->seo_title;
        $categories->cat_meta_data = $request->meta;
        $categories->cat_slug = $request->slug;
        $categories->cat_sequence = $request->sequence;

        $categories->save();

        return redirect('/admin/categories')->with('success','Successfully updated Categories');
    }
    public function destroy($id)
    {
        //
    }

    public function delete($id)
    {
        $categories = Categories::find($id);

        if($categories->cat_status == 1)
        {
            Categories::where('id',$categories->id)->update(['cat_status' => '0']);
        }
        elseif($categories->cat_status == 0)
        {
           
            $categories->delete();
        }
     
        return redirect('admin/categories')->with('danger','Successfully deleted Categories');
    }

    public function subcategories()
    {
        $data["subcategories"] = subcategories::where('status','1')->get();
        return view('product.subcategories', $data);
    }


    function subcategoriesform()
    {
        $maincategory = MainCategory::all();
        $categories = Categories::get();
        return view('product.subcategoriesform', compact('maincategory','categories'));
    }

    public function trashsubcategories()
    {
        $data["subcategories"] = subcategories::where('status','0')->get();
        return view('product.subcategories', $data);
    }
    function subcategoriesformpost(Request $request)
    {

        $subCategories = new subcategories;

        $subCategories->sub_cat_name = $request->name;
        $subCategories->sub_cat_arabic_name = $request->arabic_name;
        $subCategories->maincategory_id = $request->maincategory_id;
        $subCategories->categories_id = $request->categories_id;
        $subCategories->sequence = $request->sequence;
        $subCategories->slug = $request->slug;
        $subCategories->meta_data = $request->meta;
        $subCategories->seo_title = $request->seo_title;
        $subCategories->status = $request->enabled_1;

        $subCategories->save();

        return redirect('admin/subcategories')->with('success','Successfully Created Sub Categories');
    }

    function subcategoriesformedit($subcategoryid)
    {
        $data["subcategory"] = subcategories::where('id', $subcategoryid)->first();
        $data["maincategory"] = MainCategory::where('status',1)->get();
        $data["categories"] = Categories::where('miancategory_id',$data["subcategory"]->maincategory_id)->get();

        return view("product.subcategoriesformedit", $data);
    }

    function subcategoriesformeditpost(Request $request)
    {
        $subcategoryid = $request->subcategoryid;

        $updateColumns = array(
            'sub_cat_name' => $request->name,
            'sub_cat_arabic_name' => $request->arabic_name,
            'maincategory_id' => $request->main_categories,
            'categories_id' => $request->categories_id,
            'sequence' => $request->sequence,
            'slug' => $request->slug,
            'meta_data' => $request->meta,
            'seo_title' => $request->seo_title,
            'status' => $request->enabled_1,
        );
        $subcategories = new subcategories;
        $subcategories::where('id', $subcategoryid)->update($updateColumns);
        return redirect('admin/subcategories')->with('success','Successfully updated Sub Categories');
    }

    public function subcategories_delet($id)
    {
        
        $subcategories = subcategories::find($id);
        $subcategories->delete();

        return back()->with('danger','Successfully deleted Sub Categories');
    }

    public function brands()
    {
        $brands = Brands::where('brands_status',1)->get();
        return view('product.brands', compact('brands'));
    }

    public function trashbrand()
    {
        $brands = Brands::where('brands_status',0)->get();
        return view('product.brands', compact('brands'));
    }

    public function addbrands()
    {
        return view('product.brands-form');
    }

    public function storebrands(Request $request)
    {
        request()->validate([
            'image' => 'required|',
            
        ],
        [
            'image.required' => 'Image feild required!',
            
        ]);
        if($request->hasfile('image'))
        {
           $file = $request->file('image');
           $brands_img=time().$file->getClientOriginalName();
           $file->move(public_path().'/images/brands/', $brands_img);
          
        }

        $brands = new Brands();
        $brands->brands_name = $request->name;
        $brands->brands_arb_name = $request->arabic_name;
        $brands->brands_img = $brands_img;
        $brands->brands_status = $request->status;
        $brands->brands_seo_title = $request->seo_title;
        $brands->brands_meta_data = $request->meta;
        $brands->brands_slug = $request->slug;
        $brands->brands_sequence = $request->sequence;
        $brands->save();

        return redirect('/admin/brands')->with('success','Successfully created Brands');

    }

    public function editbrand($id)
    {
        $brands = Brands::find($id);

        return view('product.brandsedit',compact('brands'));
    }

    function updatebrand(Request $request,$id)
    {
        $brands = Brands::find($id);
        $brands->brands_name = $request->name;
        $brands->brands_arb_name = $request->arabic_name;

        if($request->hasfile('image'))
        {
           $file = $request->file('image');
           $brands_img=time().$file->getClientOriginalName();
           $file->move(public_path().'/images/brands/', $brands_img);
           $brands->brands_img = $brands_img;
        }
        $brands->brands_status = $request->status;
        $brands->brands_seo_title = $request->seo_title;
        $brands->brands_meta_data = $request->meta;
        $brands->brands_slug = $request->slug;
        $brands->brands_sequence = $request->sequence;
        $brands->save();

        return redirect('/admin/brands')->with('success','Successfully Update Brands');
    }

    public function deletebrand($id)
    {
        $brands = Brands::find($id);
        $brands->delete();

        return back()->with('danger','Successfully Deleted Brands');

    }
  
    public function products()
    {
        $products = products::with('productQuantity')->where('status',1)->orderBy('created_at','desc')->get();
        return view('product.products',compact('products'));
    }

    public function Filterbycategoru(Request $request)
    {
        $catFilter = $request->category_filter;
        $subcatFilter = $request->subcategory_filter;

        if($catFilter == "newarrival")
        {
            $products = products::where('newarrival',"on")->get();
        }
        else if($catFilter == "hotproducts")
        {
            $products = products::where('hotproducts',"on")->get();
        }
        else if($catFilter == "featuredproducts")
        {
            $products = products::where('featuredproducts',"on")->get();
        }
        else if($catFilter)
        {
            $products = products::where('categories_id',$catFilter)->get();
        }
        else if($subcatFilter)
        {
            $products = products::where('sub_category_id',$subcatFilter)->get();
        }

        return view('product.products',compact('products'));
    }

    function producttrash()
    {
        $data["products"] = products::where('status',0)->get();
        return view('product.products', $data );
    }

    public function addproducts()
    {
        $maincategory = MainCategory::all();
        $brands = Brands::all();
     
        return view('product.products-form',compact('brands','maincategory'));
    }

    public function addproductspost(Request $request)
    {
        request()->validate([
            'maincategory_id' => 'required',
            'primary_category' => 'required',
            'secondary_category' => 'required',
            'product_img' => 'required',
            'arb_product_img' => 'required',
            'additionalinformation' => 'max:1000',
            'arabic_additionalinformation' => 'max:1000'
        ],
        [
            'maincategory_id' => 'MainCategories feild required!',
            'primary_category.required' => 'Categories feild required!',
            'secondary_category.required' => 'SubCategories feild required!',
            'product_img.required' => 'Product image required',
            'arb_product_img.required' => 'Product arbic image required'
        ]);

   
        if($request->hasfile('product_img')){
            $mainImage = $request->input('main_image') - 1;
            $uploadedImage = $request->file('product_img.'.$mainImage);
            $name=$uploadedImage->getClientOriginalName();
            $uploadedImage->move(public_path().'/images/products/', $name);  
            $data[] = $name;  
            foreach($request->file('product_img') as $imgKey => $image)
            {
                if ($mainImage == $imgKey) {
                    continue;
                }
                $name=$image->getClientOriginalName();
                $image->move(public_path().'/images/products/', $name);  
                $data[] = $name;  
            }
        }
       
        
        if($request->hasfile('arb_product_img')){
           
            foreach($request->file('arb_product_img') as $image)
            {
                $name=$image->getClientOriginalName();
                $image->move(public_path().'/images/products/', $name);  
                $data1[] = $name;  
            }
        }

        if(count($request->product_img)>4)
        {
            return back()->with("danger","More than 4 images are not allowed");
        }

        if(count($request->arb_product_img)>4)
        {
            return back()->with("danger","More than 4 images are not allowed");
        }
        $products = new products;

        $products->prod_name = $request->name;
        $products->arabic_name = $request->arabic_name;
        $products->description = $request->description;
        $products->arabic_description = $request->arabic_description;
        $products->model = $request->model;
        $products->image = json_encode($data);
        $products->arabic_image = json_encode($data1);
        $products->videolink = $request->videolink;
        $products->img_height = '310';
        $products->track_stock = $request->track_stock;;
        $products->available_quantity = $request->available_quantity;
        $products->url_keyword = $request->slug;
        $products->title_tag = $request->seo_title;
        $products->meta_tag = $request->meta;
        $products->specification = $request->specification;
        $products->arabic_specification = $request->arabic_specification;
        $products->additionalinformation = $request->additionalinformation;
        $products->arabic_additionalinformation = $request->arabic_additionalinformation;
        $products->newarrival = $request->newarrival;
        $products->hotproducts = $request->hotproducts;
        $products->featuredproducts = $request->featuredproducts;
        $products->shipping = $request->shippable;
        $products->brands = $request->brand;
        $products->sku = $request->sku;
        $products->weight = $request->weight;
        $products->status = $request->status;
        $products->price = $request->price_1;
        $products->sale_price = $request->saleprice_1;
        $products->all_offers = "";
        $products->maincategory_id = $request->maincategory_id;
        $products->categories_id = $request->primary_category;
        $products->sub_category_id = $request->secondary_category;
        $products->save();

//        $maxProductId = $products->id;
//        $productColor = $request->productcolour;
//        $productquantity = $request->quantity;
//        $productsku = $request->productsku;
//        $index = 0;
//        foreach($productColor as $producttcolor)
//        {
//            $productAttributes = new Productattributes;
//            $productAttributes->colourname = $producttcolor;
//            $productAttributes->quantity = $productquantity[$index];
//            $productAttributes->sku = $productsku[$index];
//            $productAttributes->productid = $maxProductId;
//            $productAttributes->save();
//            $index++;
//        }
        return redirect('admin/products')->with('success','Successfully Add Product');
    }

    function editproduct($id)
    {
        $product = products::find($id);
        $maincategory = MainCategory::all();
        $categories = Categories::all();
        $brandshow = Brands::all();
        $subcategories = subcategories::all();
        $data["productAttributes"] = Productattributes::where("productid", $id)->orderBy('id', 'desc')->get();
        return view("product.productedit", compact('product','brandshow','categories','subcategories','maincategory'), $data);
    }

    function editproductpost(Request $request ,$id)
    {
       
        $request->validate([
            'additionalinformation' => 'max:1000',
            'arabic_additionalinformation' => 'max:1000',
//            'videolink' => 'max:70'
        ]);


       

        $products = products::find($id);
        $products->prod_name = $request->name;
        $products->arabic_name = $request->arabic_name;
        $products->description = $request->description;
        $products->arabic_description = $request->arabic_description;
        $products->model = $request->model;
        

        $data = json_decode($products->image);
        $data1 = json_decode($products->arabic_image);
        $uploadImage = $request->hasfile('product_img') ? count($request->file('product_img')) : 0;
        if((count($data) + $uploadImage) > 4)
        {
            return back()->with("img-error","More than 4 images are not allowed");
        }
        if($request->hasfile('product_img')){
            foreach($request->file('product_img') as $imgKey => $image)
            {
                $name=$image->getClientOriginalName();
                $image->move(public_path().'/images/products/', $name);  
                $data[] = $name;
            }
        }
        $mainImage = $request->input('main_image') - 1;
        if ($mainImage !== 0) {
            $prevImg = $data[0];
            $mainImg = $data[$mainImage];
            $data[0] = $mainImg;
            $data[$mainImage] = $prevImg;
        }
        if($request->hasfile('arb_product_img')){
            foreach($request->file('arb_product_img') as $image)
            {
                $name=$image->getClientOriginalName();
                $image->move(public_path().'/images/products/', $name);  
                $data1[] = $name;  
            }
        }
      
        $products->videolink = $request->videolink;
        $products->img_height = '310';
        $products->track_stock = $request->track_stock;;
        $products->available_quantity = $request->available_quantity;
        $products->url_keyword = $request->slug;
        $products->title_tag = $request->seo_title;
        $products->meta_tag = $request->meta;
        $products->specification = $request->specification;
        $products->arabic_specification = $request->arabic_specification;
        $products->additionalinformation = $request->additionalinformation;
        $products->arabic_additionalinformation = $request->arabic_additionalinformation;
        $products->newarrival = $request->newarrival;
        $products->hotproducts = $request->hotproducts;
        $products->featuredproducts = $request->featuredproducts;
        $products->shipping = $request->shippable;
        $products->brands = $request->brand;
        $products->sku = $request->sku;
        $products->weight = $request->weight;
        $products->status = $request->status;
        $products->price = $request->price_1;
        $products->sale_price = $request->saleprice_1;
        $products->all_offers = "";
        $products->image = json_encode($data);
        $products->arabic_image = json_encode($data1);
        $products->maincategory_id = $request->maincategory_id;
        $products->categories_id = $request->primary_category;
        $products->sub_category_id = $request->secondary_category;
        $products->created_at = strtotime($request->create_date);
        $products->save();

//        $productColor = $request->productcolour;
//        $productquantity = $request->quantity;
//        $productsku = $request->productsku;
//        $productattributeids = $request->productattributeidd;
//
//        $index = 0;
//        $difference = 0;
//        if(gettype($productattributeids) == "array")
//        {
//            foreach($productattributeids as $productattributeid)
//            {
//                $productAttributes = new Productattributes;
//
//                if(empty($productattributeid))
//                {
//                    $productAttributes->colourname = $productColor[$index];
//                    $productAttributes->quantity = $productquantity[$index];
//                    $productAttributes->sku = $productsku[$index];
//                    $productAttributes->productid = $id;
//                    $productAttributes->save();
//                    $index++;
//                }
//                else
//                {
//                    $updateColumns = array(
//                        'colourname' => $productColor[$index],
//                        'quantity' => $productquantity[$index],
//                        'sku' => $productsku[$index],
//                    );
//
//                    $productAttributes::where('id', $productattributeid)->update($updateColumns);
//
//                    $index++;
//                }
//            }
//        }

        return redirect('admin/products');
    }

    public function imgDelete(Request $request,$id)
    {
        $product = products::find($id);
        $images  = json_decode($product->image);
        unset($images[$request->remove_img]);
        $product->image = json_encode(array_values($images));
        $product->save();
        return redirect()->back();
    }

    function deleteproductattribute(Request $request)
    {
        $productAttribute = new Productattributes;
        $productAttribute::where('id', $request->productattributeid)->where('productid', $request->productid)->delete();
        return redirect("admin/products/edit/".$request->productid);
    }

    function productdelete($id)
    {
        $products = products::find($id);
        $products->delete();
        return redirect('admin/products')->with('danger','Product Deleted');
    }

    function getcategories(Request $request)
    {
        $categories = Categories::where('miancategory_id', $request->maincategoryid)->get();

        echo "<option value=''>SELECT</option>";
        foreach($categories as $categoriesitem)
        {
            echo "<option value='$categoriesitem->id'>".$categoriesitem->cat_name."</option>";
        }
    }

    function getsubcategories(Request $request)
    {
         // $mainCatId = $request->categoryid;
         $data["subcategories"] = subcategories::where('categories_id', $request->categoryid)->get();

         echo "<option value=''>SELECT</option>";
         foreach($data["subcategories"] as $subcategory)
         {
             echo "<option value='$subcategory->id'>".$subcategory->sub_cat_name."</option>";
         }
    }

    function banners()
    {
        $data["banners"] = banners::all();
        return view('content.banner', $data);
    }

    function bannersadd()
    {
        return view('content.banneradd');
    }

    function bannersaddpost(Request $request)
    {
        $banner = new banners;
        $banner->name = $request->name;
        $banner->arabic_name = $request->arabic_name;
        $banner->save();

        return redirect('admin/banners');
    }

    function pagesadd()
    {
        return view('product.pagesadd');
    }

    function pagesaddpost(Request $request)
    {

        $pages = new Pages;
        $pages->title = $request->title;
        $pages->arabic_title = $request->arabic_title;
        $pages->menu_title = $request->menu_title;
        $pages->menu_arabic_title = $request->arabic_menu_title;
        $pages->content = $request->content;
        $pages->arabic_content = $request->arabic_content;
        $pages->seo_title = $request->seo_title;
        $pages->metadata = $request->meta;
        $pages->slug = $request->slug;
        $pages->sequence = $request->sequence;
        $pages->save();

        return redirect('admin/pages');

    }

    function pagesaddlink()
    {
        return view('product.pagesaddlink');
    }

    function pagesaddlinkpost(Request $request)
    {
        $links = new Links;
        $links->title = $request->title;
        $links->arabic_title = $request->arabic_title;
        $links->url = $request->url;
        $links->open_link_in_window = $request->new_window;
        $links->parent = $request->parent_id;
        $links->sequence = $request->sequence;
        $links->save();

        return redirect('admin/pages');
    }


    public function pages()
    {
        return view('product.pages');
    }

    public function information()
    {
        $data["pages"] = Pages::all();
        return view('product.information', $data);
    }
}
