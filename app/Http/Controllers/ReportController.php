<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\products;
use App\Productattributes;
use App\Popularproducts;
use App\Orderitem;
use App\Orders;
use Illuminate\Support\Facades\DB;

class ReportController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('auth:admin');
    }
    public function index()
    {
        $best_sell = products::with([
            'productQuantity' => function ($q)
            {
                $q->withCount([
                    'order_items as items_quantity' => function ($q2)
                    {
                        $q2->select(DB::raw("SUM(product_quantity) as itemsquantity"));
                    }
                ]);
            }
        ])->get();
        return view("report.products_report",compact('best_sell'));
    }
    public function SalesReport()
    {
        $orders = Orders::with('orderitems')->get();
        return view("report.sales_report",compact('orders'));
    }

    public function SalesReportPost(Request $request)
    {
        $betweenDates = "DATE(created_at) BETWEEN '".$request->best_sellers_start."' and '".$request->best_sellers_end."'";
        $orders = Orders::with('orderitems')->whereRaw($betweenDates)->get();

        return view("report.sales_report",compact('orders'));
    }

    // public function reportpost(Request $request)
    // {
    //     foreach($request->popularproducts as $popularproduct)
    //     {po
    //         $popularProudct = new Popularproducts;
    //         $popularProudct->productid = $popularproduct; po
    //         $popularProudct->status = 1;
    //         $popularProudct->save();
    //         $checkingProductRow = Popularproducts::where("productid", $popularproduct)->first();

    //         return redirect()->back();
    //     }
    // }

}
