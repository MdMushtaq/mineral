<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Categories;
use App\subcategories;
use App\products;
use App\Productattributes;
use Cart;
use Auth;
use Illuminate\Support\Facades\Mail;
use App\Mail\SendCareerEmail;
use App\Faq;
use App\Faqcategories;
use App\Subscriptions;
use App\Mail\SendProductUrlEmail;
use App\banners;
use App\subcategory_promotions;
use App\product_promotions;
use App\brand_promotions;


class WelcomeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $banners = banners::get();
        return view('front-end.welcome',compact('banners'));
    }

    public function newarrival()
    {
        $getproducts = products::where(['status' => 1 , 'newarrival' => 'on'])->orderBy('created_at','desc')->limit(15)->get();
        // dd($getproducts[0]->shipping);
        return view('front-end.allproductslist',compact('getproducts'));
    }

    public function hotproduct()
    {
        $getproducts = products::where(['status' => 1 , 'hotproducts' => 'on'])->orderBy('created_at','desc')->limit(15)->get();
        return view('front-end.allproductslist',compact('getproducts'));
    }

    public function featureproduct()
    {
        $getproducts = products::where(['status' => 1 , 'featuredproducts' => 'on'])->orderBy('created_at','desc')->limit(15)->get();
        return view('front-end.allproductslist',compact('getproducts'));
    }

    function Categorylist($id)
    {
        $getproducts = products::where(['categories_id' => $id , 'status' => 1])->orderBy('created_at','desc')->get();
        $categories = Categories::where('id',$id)->first();
        return view('front-end.categoryproduct',compact('getproducts','categories'));
    }

    function SubCategorylist($id)
    {
        $getproducts = products::where(['sub_category_id' => $id , 'status' => 1])->orderBy('created_at','desc')->get();
        $subcategories = subcategories::where('id',$id)->first();
        return view('front-end.subcategoryproduct',compact('getproducts','subcategories'));
    }


    function productslist(Request $request, $id)
    {
        $list = products::find($id);
        return view('front-end.productslist',compact('list'));
    }

    public function careers()
    {
        return view('front-end.careers');
    }

    public function careersPost(Request $request)
    {
        // Get the uploades file with name document
        $document = $request->file('document');

        // Required validation
        $request->validate([
            'document' => 'required'
        ]);

        // Check if uploaded file size was greater than
        // maximum allowed file size
        if ($document->getError() == 1)
        {
            $max_size = $document->getMaxFileSize() / 1024 / 1024;  // Get size in Mb
            $error = 'The document size must be less than ' . $max_size . 'Mb.';
            return redirect()->back()->with('flash_danger', $error);
        }

        $data = [
            'document' => $document,
            'name'=>$request->name,
            'email'=>$request->email,
            'phonenumber'=>$request->phonenumber,
            'jobtitle'=>$request->jobtitle,
        ];

        Mail::to('m.atif_khan@yahoo.com')->send(new SendCareerEmail($data));

        return back()->with('success','OK');

    }

    public function faq()
    {
        $faqs = Faq::all();
        $faqcategories = Faqcategories::all();
        return view('front-end.faq', compact('faqs','faqcategories'));
    }

    public function faqPost(Request $request)
    {
        $whereClause = "question like '%".$request->question."%'";
        if($request->faqcategoryid)
        {
            $faqs = Faq::where('faqcategoryid', $request->faqcategoryid)->get();
        }
        if($request->question)
        {
            $faqs = Faq::whereRaw($whereClause)->get();
        }
        $faqcategories = Faqcategories::all();
        return view('front-end.faq', compact('faqs','faqcategories'));
    }

    public function contact()
    {
        $getproducts = products::where(['status' => 1])->orderBy('created_at','desc')->limit(32)->get();
        return view('front-end.contact',compact('getproducts'));
    }

    public function termsandconditions()
    {
        return view('front-end.termsandconditions');
    }

    public function privacyandpolicy()
    {
        return view('front-end.privacyandpolicy');
    }

    public function about()
    {
        return view('front-end.about');
    }

    public function subscribe(Request $request)
    {
        $subscriptions = new Subscriptions;
        $subscriptions->email = $request->email;
        $subscriptions->save();
        return back()->with("successfulsubsciption",'Email subscribed for newsletter');
    }

    public function sendproductemail(Request $request)
    {
        $data = array(
            "producturl"=>$request->producturl
        );
        Mail::to($request->email)->send(new SendProductUrlEmail($data));
        return back()->with('success','Product Url has been sent');
    }

    public function cartadd(Request $request,$id) {
        $product = products::find($id);
        $options = [];
        if ($request->product_color) {
            $options['color'] = (object)json_decode($request->product_color);
        }
        $options['original_price'] = $product->price;
        $cart =  Cart::add([
            [
                'id' => $id,  
                'name' => $product->prod_name, 
                'qty' => $request->qty, 
                'price' => $product->discounted_price ?? $product->price,
                'options' => $options
            ] 
          ]);
        
        return redirect()->back()->with('success','Successfully Added to Your Cart');

    }
    public function checkoutcart()
    {
        $cart =  Cart::content();
        $products = products::whereIn('id', $cart->pluck('id')->toArray())->get();
        foreach ($cart as $item) {
            $options = $item->options->toArray();
            unset($options['original_price']);
            $product = $products->where('id', $item->id)->first();
            $item->product = $product;
            $item->total = $item->qty * $item->price;
            $item->attributes = $options;
        }
        return view('front-end.checkout',compact('cart','products'));
    }

    public function Qtyupdate(Request $request,$rowId)
    {
        Cart::update($rowId, $request->quantity);
        return redirect()->back();
        
    }

    public function removeCart($rowId)
    {
        Cart::remove($rowId);
        return back()->with('danger','Successfully Remove Item');
    }

    public function deals()
    {
        return view('front-end.deals');
    }

    public function help()
    {
        return view('front-end.help');
    }

    public function paymenttest()
    {
        return view('front-end.api-test');
    }

}
