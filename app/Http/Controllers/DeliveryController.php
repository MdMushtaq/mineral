<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Orders;

class DeliveryController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('checkdelivery');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $data["customers"] = User::all();
        $data["totalcustomers"] = $data["customers"]->count();
        $orders = Orders::with('orderitems')->where('payment','Success')->orderBy('id', 'DESC')->get();

        return view('delivery', $data,compact('orders'));
    }
}
