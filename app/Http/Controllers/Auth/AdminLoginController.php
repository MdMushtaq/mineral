<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use Illuminate\Support\Facades\Hash;
use App\Delivery;
use App\City;

class AdminLoginController extends Controller
{
    public function __construct()
    {
      $this->middleware('guest:admin', ['except' => ['logout']]);
    }

    public function showLoginForm(){
        return view ('auth.admin-login');
    }

    public function showDeliveryLoginForm(){
        return view ('auth.delivery-login');
    }

    public function showDeliveryLoginFormPost(Request $request)
    {
        // Validate the form data
//        $this->validate($request, [
//            'email'   => 'required|email',
//            'password' => 'required|min:6'
//        ]);

//        username password verification_code


        $password = Hash::make($request->password);

        $delivery = Delivery::where('name', $request->username)->where('verification_code', $request->verification_code)->where('status', 1)->first();

        if(getType($delivery) == "object")
        {
            if (Hash::check($request->password, $delivery->password))
            {
                // The passwords match...
                $request->session()->put('id', $delivery->id);
                $request->session()->put('name', $delivery->name);
                $request->session()->put('city_id', $delivery->city_id);
                return redirect('/delivery/agents');
            }
                // The passwords match...
        }
        else
        {
            return redirect()->back();        }
    }

    
    public function login(Request $request)
    {
      // Validate the form data
      $this->validate($request, [
        'email'   => 'required|email',
        'password' => 'required|min:6'
      ]);

      // Attempt to log the user in
      if (Auth::guard('admin')->attempt(['email' => $request->email, 'password' => $request->password], $request->remember)) {
        // if successful, then redirect to their intended location
        return redirect()->intended(route('admin.dashboard'));
      }

      // if unsuccessful, then redirect back to the login with the form data
      return redirect()->back()->withInput($request->only('email', 'remember'))->withErrors(['password'=>'Authentication Failed']);
    }

    public function showDeliveryRegisterForm()
    {
        $cities = City::all();
        return view ('auth.delivery-register',compact('cities'));
    }

    public function showDeliveryRegisterFormPost(Request $request)
    {
        $this->validate($request,[
            'password' => 'required|min:6'
        ]);

        $deliveryagent =  new Delivery;
        $deliveryagent->name = $request->name;
        $deliveryagent->mobile = $request->mobile;
        $deliveryagent->city_id = $request->city;
        $deliveryagent->password = Hash::make($request->password);
        $deliveryagent->status = 0;
        $deliveryagent->save();

        return back()->with('success','Registered successfully');

    }

    public function logout()
    {
      Auth::guard('admin')->logout();
      return redirect('/admin/login');
    }

    
}
