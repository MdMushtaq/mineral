<?php

namespace App\Http\Controllers\deliveryagents;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Orders;
use Illuminate\Support\Facades\Session;

class SalesController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('checkdelivery');
    }

    public function index(Request $request)
    {
        $headingTitle = "Order";
        $orders = Orders::with('orderitems')->where('approved_status','')->where('city_id',$request->session()->get('city_id'))->orderBy('id', 'DESC')->get();
        return view("deliveryagents.sales.orders", compact('orders','headingTitle'));
    }

    public function orderagentapproved(Request $request, $orderid)
    {
        $order = Orders::find($orderid);
        $order->agent_id = $request->session()->get('id');
        $order->save();

        return redirect('/delivery/agents/orders');

    }

}
