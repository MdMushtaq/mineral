<?php

namespace App\Http\Controllers;

use App\City;
use Illuminate\Http\Request;
use App\Orders;
use App\User;
use App\Orderitem;
use App\MainCategory;
use App\Delivery;

class SalesController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function index()
    {
        $headingTitle = "Order";
        $orders = Orders::with('orderitems')->where('approved_status','')->orderBy('id', 'DESC')->get();
        return view("sales.orders", compact('orders','headingTitle'));
    }

    public function confirmbyAdmin()
    {
        $headingTitle = "Confirm By Admin";
        $orders = Orders::with('orderitems')->where('approved_status','Confirm_byAdmin')->orderBy('id', 'DESC')->get();
        return view("sales.orders", compact('orders','headingTitle'));
    }

    public function Orderdelivered()
    {
        $headingTitle = "Delivered";
        $orders = Orders::with('orderitems')->where('status','Delevered')->orderBy('id', 'DESC')->get();
        return view("sales.orders", compact('orders','headingTitle'));
    }

    // public function confirmbyajent()
    // {
    //     $orders = Orders::where('approved_status','Confirm_byAjent');
    //     return view("sales.orders", compact('orders'));
    // }

    public function show($id)
    {
       
       $itemorder = Orders::with('orderitems')->where('id',$id)->orderBy('id', 'DESC')->get();
       return view("sales.orderitems", compact('itemorder'));
    }

    public function update(Request $request ,$id)
    {
        // dd($request);
        $order = Orders::find($id);
        $order->status =  $request->order_status;
        $order->approved_status = $request->order_approved_status;
        $order->aramex_ref = $request->aramex_ref;
        $order->aramex_track = $request->aramex_tracking;
        $order->shipping_date = $request->shipping_date;
        $order->save();
        return redirect('/admin/orders');
    }

    public function delivery()
    {
        $deliveries = Delivery::all();
        return view('sales.delivery',compact('deliveries'));
    }

    public function deliveryForm()
    {
        return view('sales.deliveryform');
    }

    public function deliveryFormPost(Request $request)
    {
        $delivery = new Delivery;
        $delivery->name = $request->name;
        $delivery->mobile = $request->mobile;
        $delivery->verification_code = $request->verification_code;
        $delivery->save();

        return redirect('/admin/orders/delivery');
    }

    function deliveryformedit($deliveryid)
    {
        $delivery = Delivery::where('id', $deliveryid)->first();
        $cities = City::all();
        return view("sales.deliveryformedit", compact('delivery','cities'));
    }

    function deliveryformeditpost(Request $request)
    {
        $delivery = Delivery::find($request->delivery_id);
        $delivery->name = $request->name;
        $delivery->id_code = $request->idcode;
        $delivery->mobile = $request->mobile;
        $delivery->city_id = $request->city;
        $delivery->verification_code = $request->verification_code;
        $delivery->status = $request->status;
        $delivery->save();

        return redirect('/admin/orders/delivery');

    }




}
