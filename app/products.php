<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class products extends Model
{
    //

    protected $table = 'products';

    public function order_item()
    {
       return $this->hasMany(Orderitem::class, 'product_id');
    }

    public function productQuantity()
    {
       return $this->hasMany(Productattributes::class, 'productid');
    }

    public function getDiscountedPriceAttribute()
    {
       if ($this->promotion_type == 'product' || $this->promotion_type == 'subcategory') {
          if ($this->promotion_type == 'product') {
            $discount = product_promotions::where('id' ,$this->all_offers)->where('disable_on_utc', '>', now())->first();
          } else {
            $discount = subcategory_promotions::where('id' ,$this->all_offers)->where('disable_on_utc', '>', now())->first();
          }
          if ($discount) {
            return ((100 - $discount->percentage) * $this->price) / 100;
          }
       }
       return null;
    }
  
   public function getVatAttribute() 
   {
      $price = $this->discounted_price ?? $this->price;
      return ($price * config('cart.tax')) / 100;
   }

   public function getMainImageAttribute()
   {
      $images = json_decode($this->image);
      if (isset($images[0])) {
         return asset('public/images/products/'.$images[0]);
      }
      return null;
   }

   public function getTotalAttribute()
   {
      $price = $this->discounted_price ?? $this->price;
      return $this->vat + $price;
   }

}   
   