<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class City extends Model
{
    protected $table = 'city';

    public function Shipping()
    {
        return $this->hasOne(Freeshippings::class, 'city_id');
    }
}


